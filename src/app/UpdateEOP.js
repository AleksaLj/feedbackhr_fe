import { Component } from './core/Component';
import config from './config';
import { EOPModel } from './models/EOPModel';

export class UpdateEOP extends Component {

    constructor(employeeId) {
        super();
        this.employeeId = employeeId;
    }

    getPerformanceFactorMark(mark) {
        var result = 0;
        switch (mark) {
            case 'NE':
                result = 1;
                break;
            case 'U':
                result = 2;
                break;
            case 'IN':
                result = 3;
                break;
            case 'MR':
                result = 4;
                break;
            case 'AR':
                result = 5;    
                break;
        }
        return result;
    }

    getCheckedMark(elementName) {
        var elements = document.getElementsByName(elementName);
        var mark = 0;
                for (var j = 0; j < elements.length; j++) {
                    if (elements[j].checked) {
                        var elementValue = elements[j].value;
                        mark = this.getPerformanceFactorMark(elementValue);
                    }
        }
        return mark;
    }

    getCheckedFollowUpReason(followUpElement, followUpComment, followUpResolved) {
        var obj = {
            Resolved: followUpResolved.checked,
            ShortComment: null,
            SalaryAfter: null
        };

        if (followUpElement.checked) {
            var salary = null;
            if (followUpElement.id == 's-eop') {
                salary = {
                    IsSatisfied: document.getElementById('satisfied-after-follow-up').checked,
                    SalaryChanged: document.getElementById('changed-after-follow-up').checked,
                    ChangedFor: document.getElementById('amount-after-follow-up').value,
                    DesiredAmount: document.getElementsByName('satisfied-amount-after-follow-up').value
                }
            }
                obj.Resolved = followUpResolved.checked;
                obj.ShortComment = followUpComment.value;
                obj.SalaryAfter = salary;
            
                        
            return obj;   
        }
        return obj;
    }
    
    getEopById(EopId) {
        const URL = config.url + config.getEopById + `${EopId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const EOP = JSON.parse(data);
                return Promise.resolve(EOP);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }

    updateEOP(EOP) {
        const URL = config.url + config.updateEop;
        fetch(URL, {
            method: 'POST',
            body: JSON.stringify(EOP),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                response.text();
                if (response.status == 200)
                    Notiflix.Notify.Success('sucessfully saved');
                else
                    Notiflix.Notify.Failure('unsucessfully saved');
            })
            .catch((error) => {
                console.log(error);
            })
    };

    onUpdateEvent(formId) {
        this.getEopById(formId)
            .then((EOP) => { 
                var eop = EOP;
                eop.client = document.getElementById('client-appraiser').value;
                
                //EOPReport
                eop.dateOfTalk = document.getElementById('talk-date-input-update-eop').value;
                eop.hrReporter = document.getElementById('hr-reporter-name-eop').value;

                eop.reportEOP = {
                    StartDate: EOP.reportEOP.startDate,
                    EndDate: EOP.reportEOP.endDate,
                    EmployeesImpression: document.getElementById('employee-impressions-eop').value,
                    NextSteps: document.getElementById('next-steps-eop').value,
                    PerformanceFeedback: document.getElementById('performance-feedback-eop').value,
                    FinalRecommendation: document.getElementById('final-recommendation-eop').value
                };

                // LineManager Marks
                var qLmEOPMark = this.getCheckedMark('qualityLmEOP');
                var pLmEOPMark = this.getCheckedMark('productivityLmEOP');
                var pkLmEOPMark = this.getCheckedMark('professional-knowledgeLmEOP');
                var rLmEOPMark = this.getCheckedMark('reliabilityLmEOP');
                var cLmEOPMark = this.getCheckedMark('communicationLmEOP');
                var tcLmEOPMark = this.getCheckedMark('teamworkcooperationLmEOP');
                var aLmEOPMark = this.getCheckedMark('autonomyLmEOP');
               
                //Line Manager EOPConcrete
                eop.lineManagerEOP = {
                    Quality: { Mark: qLmEOPMark, Remark: document.getElementById('RemarksQualityLmEOP').value },
                    Productivity: { Mark: pLmEOPMark, Remark: document.getElementById('RemarksProductivityLmEOP').value },
                    ProfessionalKnowledge: { Mark: pkLmEOPMark, Remark: document.getElementById('RemarksProfessionalKnowledgeLmEOP').value },
                    Reliability: { Mark: rLmEOPMark, Remark: document.getElementById('RemarksReliabilityLmEOP').value },
                    Communication: { Mark: cLmEOPMark, Remark: document.getElementById('RemarksCommunicationLmEOP').value },
                    TeamWorkAndCooperation: { Mark: tcLmEOPMark, Remark: document.getElementById('RemarksTeamworkCooperationLmEOP').value },
                    Autonomy: { Mark: aLmEOPMark, Remark: document.getElementById('RemarksAutonomyLmEOP').value },
                    OverallImpression: document.getElementById('OverallImpressionLmEOP').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsLmEOP').value
                }
                
                //Team Lead Marks
                var qTlEOPMark = this.getCheckedMark('qualityTlEop');
                var pTlEOPMark = this.getCheckedMark('productivityTlEop');
                var pkTlEOPMark = this.getCheckedMark('professional-knowledgeTlEop');
                var rTlEOPMark = this.getCheckedMark('reliabilityTlEop');
                var cTlEOPMark = this.getCheckedMark('communicationTlEop');
                var tcTlEOPMark = this.getCheckedMark('teamwork-cooperationTlEop');
                var aTlEOPMark = this.getCheckedMark('autonomyTlEop');
                
                //Team Lead EOPConcrete
                eop.teamLeadEOP = {
                    Quality: { Mark: qTlEOPMark, Remark: document.getElementById('RemarksQualityTlEOP').value },
                    Productivity: { Mark: pTlEOPMark, Remark: document.getElementById('RemarksProductivityTlEOP').value },
                    ProfessionalKnowledge: { Mark: pkTlEOPMark, Remark: document.getElementById('RemarksProfessionalKnowledgeTlEOP').value },
                    Reliability: { Mark: rTlEOPMark, Remark: document.getElementById('RemarksReliabilityTlEOP').value },
                    Communication: { Mark: cTlEOPMark, Remark: document.getElementById('RemarksCommunicationTlEOP').value },
                    TeamWorkAndCooperation: { Mark: tcTlEOPMark, Remark: document.getElementById('RemarksTeamworkCooperationTlEOP').value },
                    Autonomy: { Mark: aTlEOPMark, Remark: document.getElementById('RemarksAutonomyTlEOP').value },
                    OverallImpression: document.getElementById('OverallImpressionTlEOP').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsTlEOP').value
                }

                //Client Marks
                var qClEOPMark = this.getCheckedMark('qualityClEop');
                var pClEOPMark = this.getCheckedMark('productivityClEop');
                var pkClEOPMark = this.getCheckedMark('professional-knowledgeClEop');
                var rClEOPMark = this.getCheckedMark('reliabilityClEop');
                var cClEOPMark = this.getCheckedMark('communicationClEop');
                var tcClEOPMark = this.getCheckedMark('teamwork-cooperationClEop');
                var aClEOPMark = this.getCheckedMark('autonomyClEop');
                 
                //Client EOPConcrete
                eop.clientEOP = {
                    Quality: { Mark: qClEOPMark, Remark: document.getElementById('RemarksQualityClEOP').value },
                    Productivity: { Mark: pClEOPMark, Remark: document.getElementById('RemarksProductivityClEOP').value },
                    ProfessionalKnowledge: { Mark: pkClEOPMark, Remark: document.getElementById('RemarksProfessionalKnowledgeClEOP').value },
                    Reliability: { Mark: rClEOPMark, Remark: document.getElementById('RemarksReliabilityClEOP').value },
                    Communication: { Mark: cClEOPMark, Remark: document.getElementById('RemarksCommunicationClEOP').value },
                    TeamWorkAndCooperation: { Mark: tcClEOPMark, Remark: document.getElementById('RemarksTeamworkCooperationClEOP').value },
                    Autonomy: { Mark: aClEOPMark, Remark: document.getElementById('RemarksAutonomyClEOP').value },
                    OverallImpression: document.getElementById('OverallImpressionClEOP').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsClEOP').value
                }
                
                var patEop = this.getCheckedFollowUpReason(document.getElementById('pat-eop'), document.getElementById('pat-eop-shortComment'), document.getElementById('pat-eop-resolved'));
                var wwoEop = this.getCheckedFollowUpReason(document.getElementById('wwo-eop'), document.getElementById('wwo-eop-shortComment'), document.getElementById('wwo-eop-resolved'));
                var tacttpEop = this.getCheckedFollowUpReason(document.getElementById('tacttp-eop'), document.getElementById('tacttp-eop-shortComment'), document.getElementById('tacttp-eop-resolved'));
                var rwceuEop = this.getCheckedFollowUpReason(document.getElementById('rwceu-eop'), document.getElementById('rwceu-eop-shortComment'), document.getElementById('rwceu-eop-resolved'));
                var tcEop = this.getCheckedFollowUpReason(document.getElementById('tc-eop'), document.getElementById('tc-eop-shortComment'), document.getElementById('tc-eop-resolved'));
                var cwlmEop = this.getCheckedFollowUpReason(document.getElementById('cwlm-eop'), document.getElementById('cwlm-eop-shortComment'), document.getElementById('cwlm-eop-resolved'));
                var cwtlEop = this.getCheckedFollowUpReason(document.getElementById('cwtl-eop'), document.getElementById('cwtl-eop-shortComment'), document.getElementById('cwtl-eop-resolved'));
                var oswcamEop = this.getCheckedFollowUpReason(document.getElementById('oswcam-eop'), document.getElementById('oswcam-eop-shortComment'), document.getElementById('oswcam-eop-resolved'));
                var icacpEop = this.getCheckedFollowUpReason(document.getElementById('icacp-eop'), document.getElementById('icacp-eop-shortComment'), document.getElementById('icacp-eop-resolved'));
                var pweEop = this.getCheckedFollowUpReason(document.getElementById('pwe-eop'), document.getElementById('pwe-eop-shortComment'), document.getElementById('pwe-eop-resolved'));
                var cjsEop = this.getCheckedFollowUpReason(document.getElementById('cjs-eop'), document.getElementById('cjs-eop-shortComment'), document.getElementById('cjs-eop-resolved'));
                var tbaEop = this.getCheckedFollowUpReason(document.getElementById('tba-eop'), document.getElementById('tba-eop-shortComment'), document.getElementById('tba-eop-resolved'));
                var ofpacdEop = this.getCheckedFollowUpReason(document.getElementById('ofpacd-eop'), document.getElementById('ofpacd-eop-shortComment'), document.getElementById('ofpacd-eop-resolved'));
                var bEop = this.getCheckedFollowUpReason(document.getElementById('b-eop'), document.getElementById('b-eop-shortComment'), document.getElementById('b-eop-resolved'));
                var sEop = this.getCheckedFollowUpReason(document.getElementById('s-eop'), document.getElementById('s-eop-shortComment'), document.getElementById('s-eop-resolved'));
                                
                var followUpsToDo = [
                    patEop,
                    wwoEop,
                    tacttpEop,
                    rwceuEop,
                    tcEop,
                    cwlmEop,
                    cwtlEop,
                    oswcamEop,
                    icacpEop,
                    pweEop,
                    cjsEop,
                    tbaEop,
                    ofpacdEop,
                    bEop,
                    sEop];
                eop.followUpsToDo = followUpsToDo;

                var followUpOptions = [
                    { FollowUpOption: 1, Checked: document.getElementById('pat-eop').checked },
                    { FollowUpOption: 2, Checked: document.getElementById('wwo-eop').checked },
                    { FollowUpOption: 3, Checked: document.getElementById('tacttp-eop').checked },
                    { FollowUpOption: 4, Checked: document.getElementById('rwceu-eop').checked },
                    { FollowUpOption: 5, Checked: document.getElementById('tc-eop').checked },
                    { FollowUpOption: 6, Checked: document.getElementById('cwtl-eop').checked },
                    { FollowUpOption: 7, Checked: document.getElementById('cwlm-eop').checked },
                    { FollowUpOption: 8, Checked: document.getElementById('oswcam-eop').checked },
                    { FollowUpOption: 9, Checked: document.getElementById('icacp-eop').checked },
                    { FollowUpOption: 10, Checked: document.getElementById('pwe-eop').checked },
                    { FollowUpOption: 11, Checked: document.getElementById('cjs-eop').checked },
                    { FollowUpOption: 12, Checked: document.getElementById('tba-eop').checked },
                    { FollowUpOption: 13, Checked: document.getElementById('ofpacd-eop').checked },
                    { FollowUpOption: 14, Checked: document.getElementById('b-eop').checked },
                    { FollowUpOption: 15, Checked: document.getElementById('s-eop').checked }
                ];
                
                eop.followUpSection.followUpOptions = followUpOptions; 
                eop.followUpSection.hrNotes = document.getElementById('eop-hr-notes').value;
                
                if (document.getElementById('follow-up').checked) {                       
                    eop.followUpSection.talkDate = document.getElementById('eop-talk-date').value;
                }
            
                eop.followUpSection.hrReporter = document.getElementById('follow-up-hr-reporter').value;
                var changedForValue = null;
                if (document.getElementById('salary-change').checked) {                  
                    changedForValue = parseInt(document.getElementById('salary-change-amount').value);
                }

                var desiredAmount = null;
                if (!document.getElementById('satisfied').checked) {
                    desiredAmount = parseInt(document.getElementById('expected').value);
                }

                eop.salary = {
                    IsSatisfied: document.getElementById('satisfied').checked,
                    SalaryChanged: document.getElementById('salary-change').checked,                        
                    ChangedFor: changedForValue,
                    DesiredAmount: desiredAmount
                };

                //
                var desiredAmountAfter = null;
                if (document.getElementById('satisfied-after-follow-up').checked == false) {
                    desiredAmountAfter = parseInt(document.getElementById('satisfied-amount-after-follow-up').value);
                }                                                                                                                                                   

                var changedForValueAfter = null;
                if (document.getElementById('changed-after-follow-up').checked) {
                    changedForValueAfter = parseInt(document.getElementById('amount-after-follow-up').value);
                }
                eop.followUpsToDo[14].SalaryAfter = {                 
                    IsSatisfied: document.getElementById('satisfied-after-follow-up').checked,
                    SalaryChanged: document.getElementById('changed-after-follow-up').checked,
                    ChangedFor: changedForValueAfter,
                    DesiredAmount: desiredAmountAfter                    
                };
                
                this.updateEOP(eop);
            });
    };
}
 //done/is open for follow up ako nisu svi resolved

document.addEventListener('DOMContentLoaded', () => {
    const urlParams = new URLSearchParams(window.location.search);
    const employeeId = urlParams.get('id');
    window.app = new UpdateEOP(employeeId);
});