import Oidc from 'oidc-client';

import { Component } from './core/Component';
import config from './config';
import { StringHelper } from './utility/StringHelper';
import { doc } from 'prettier';
import { EOPModel } from './models/EOPModel';
import { PATModel } from './models/PATModel';
import { UpdateEOP } from './UpdateEOP';
import { UpdatePAT } from './UpdatePAT';
import * as api from './apiJs/Index';
import { FillEOPFormComponent } from './common/FillEOPFormComponent';
import { FillPATFormComponent } from './common/FillPATFormComponent';
import { FillAdHocForm } from './common/AdHocForm/FillAdHocFormComponent';
import { UpdateAdHocForm } from './services/adHoc/UpdateAdHocFormService';
import { FillExitForm } from './common/ExitForm/FillExitForm';
import { FbhrCommon } from './common/FbhrCommon';
import { promises } from 'fs-extra';

export class App extends Component {
    constructor() {
        super();

        Oidc.Log.logger = console;
        Oidc.Log.level = Oidc.Log.DEBUG;

        this.stringHelper = new StringHelper();
        this.formId = null;
        this._adHocForm = null;
        this.employeeId = null;
        this.headers = new Headers();

        this.setUserManager = this.setUserManager.bind(this);
        this.setUserManager();

        this.employees = this.getEmployees.bind(this);
        this.teams = this.getTeams.bind(this);
        this.units = this.getUnits.bind(this);
        this.locations = this.getLocations.bind(this);  
    }

    setUserManager() {
        const that = this;
        const userManagerConfig = {
            client_id: 'feedback_hr',
            redirect_uri: "https://" + window.location.host + '/callback.html',
            response_type: 'token id_token',
            scope: "openid profile feedbackhr_api timemanager_api",
            authority: "https://test-timemanager-oauth.enjoying.rs/",
            silent_redirect_uri: 'https://' + window.location.host + '/silent_renew.html',
            post_logout_redirect_uri: 'https://' + window.location.host + '/',
            automaticSilentRenew: true,
            filterProtocolClaims: true,
            loadUserInfo: true,
            monitorSession: true
        };
        const userManager = new Oidc.UserManager(userManagerConfig);
        userManager.getUser().then(function (user) {
            if (user) {
                console.log(user);
                that.headers.append('Content-Type', 'application/json; charset=utf-8');
                that.headers.append('application-name', 'feedback-hr');
                that.headers.append('application-version', 'x.xx.x');
                that.headers.append('Authorization', 'Bearer ' + user.access_token);

                that.getAllData().then((data) => {
                    that.getUnits(data.result);
                    that.getTeams(data.result);
                    that.getLocations(data.result);
                    that.getEmployees(data.result);
                    that.getForms();
                });
                that.getForms();
               
            } else {
                debugger;
                userManager.signinRedirect({
                    data: { path: window.location.pathname }
                });
            }
        });
    }
    
    getAllData() {
        const URL = config.urlTest;
        return fetch(URL, {
            headers: this.headers
        })
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                return Promise.resolve(data);
            })
            .catch((error) => {

            })
    }

    getForms() {
        const URL = config.url + config.getForms;
        
        fetch(URL, {
            headers: this.headers
        })
            .then((response) => {
                 response.text();
            })
            .then((data) => {
                this.forms = JSON.parse(data);
                this.showForms(this.forms);
            })
            .catch((error) => {
                
            })
    }

    getEmployees(data) {
        this.employees = data.employees;
        this.showEmployees(this.employees);
    }

    getEmployeeById(employeeId) {
        const URL = config.url + config.getEmployee + `${employeeId}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const emp = JSON.parse(data);
                return Promise.resolve(emp);
            })
            .catch((error) => {
            })
    }

    getEmployeeUnit(unitId) {        
        var index = 0;
        var unit;
        var entry;
                
        for (index = 0; index < this.units.length; index++) {
            entry = this.units[index];
            if (entry.id == unitId) {
                unit = entry;
                break;
            }
        }
        return unit;
    }

    getUnits(data) {        
        this.units = data.units;
        this.showUnits(this.units);
    }

    getTeams(data) {        
        this.teams = data.teams;
        this.showTeams(this.teams);
    }

    getLocations(data) {       
        this.locations = data.locations;
        this.showLocations(this.locations);
    }

    getEmployeeTeam(teamId) {        
        var index = 0;
        var team;
        var entry;
        for (index = 0; index < this.teams.length; ++index) {
            entry = this.teams[index];
            if (entry.id == teamId) {
                team = entry;
                break;
            }
        }
        return team;
    }

    getUnresolvedForms() {
        const URL = config.url + config.getUnresolvedForm;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const unresolved = JSON.parse(data);
                return Promise.resolve(unresolved);
            })
            .catch((error) => {

            })
    }

    showEmployees(employees) {
        employees.forEach((employee) => {
            const rowDiv = document.getElementById('rowDiv');

            const mainDiv = document.createElement('div');
            mainDiv.setAttribute('class', 'fb-employee col-12 col-sm-6 col-md-4 col-xl-3');
            rowDiv.appendChild(mainDiv);

            const employeeItemDiv = document.createElement('div');
            employeeItemDiv.setAttribute('class', 'fb-doc-item mb-3');
            mainDiv.appendChild(employeeItemDiv);

            const employeeProfileHref = document.createElement('a');
            employeeProfileHref.setAttribute('href', `profile.html?id=${employee.id}`);
            employeeProfileHref.setAttribute('class', 'div-link');
            employeeProfileHref.setAttribute('target', '_blank');
            employeeProfileHref.setAttribute('id', `${employee.id}`);

            const employeeRowDiv = document.createElement('div');
            employeeRowDiv.setAttribute('class', 'row');
            employeeItemDiv.appendChild(employeeProfileHref);
            employeeItemDiv.appendChild(employeeRowDiv);

            const rowContentDiv = document.createElement('div');
            rowContentDiv.setAttribute('class', 'col-12 d-flex justify-content-start align-items-center');
            employeeRowDiv.appendChild(rowContentDiv);

            const imgDiv = document.createElement('div');
            imgDiv.setAttribute('class', 'fb-profile');
            const elImg = document.createElement('img');
            elImg.setAttribute('src', config.imageUrl + `${employee.imageUrl}`);

            ///////////

            elImg.addEventListener("error", (e) => {
                elImg.setAttribute('alt', 'Name, Last Name');
            })

            /////////////////////
            imgDiv.appendChild(elImg);
            rowContentDiv.appendChild(imgDiv);

            const empMainDiv = document.createElement('div');
            empMainDiv.setAttribute('class', 'pl-2');
            rowContentDiv.appendChild(empMainDiv);

            const empNameDiv = document.createElement('div');
            empNameDiv.setAttribute('class', 'fb-employee-name');
            empNameDiv.textContent = `${employee.firstName} ${employee.lastName}`;
            empMainDiv.appendChild(empNameDiv);

            const empDataDiv = document.createElement('div');
            empDataDiv.setAttribute('class', 'fb-employee-data');
            empMainDiv.appendChild(empDataDiv);

            var unit = this.getEmployeeUnit(employee.unitId);
            const unitSpan = document.createElement('span');
            unitSpan.setAttribute('class', 'fb-employee-unit d-inline-block text-nowrap text-truncate');
            unitSpan.textContent = employee.unitId == 0 ? 'undefined unit' : unit.name.split('-')[0];
            empDataDiv.appendChild(unitSpan);

            var team = this.getEmployeeTeam(employee.teamId);
            const teamSpan = document.createElement('span');
            teamSpan.setAttribute('class', 'fb-employee-team d-inline-block text-nowrap text-truncate');
            teamSpan.textContent = employee.teamId == 0 ? 'undefined team' : team.name;
            empDataDiv.appendChild(teamSpan);
        });
    }

    showForms(forms) {

        const divToEmpty = document.getElementById('rowDocument');

        divToEmpty.innerHTML = "";

        forms.forEach((employeeFormModel) => {
            const rowDiv = document.getElementById('rowDocument');

            const mainDiv = document.createElement('div');
            mainDiv.setAttribute('class', 'col-12 col-xl-6 fb-doc-block js_loadMore');
            rowDiv.appendChild(mainDiv);

            const formItemDiv = document.createElement('div');
            formItemDiv.setAttribute('class', 'fb-doc-item mb-4');
            mainDiv.appendChild(formItemDiv);
            //
            const formHref = document.createElement('a');
            formHref.setAttribute('href', 'javascript:void(0)');
            formHref.setAttribute('class', 'div-link');
            formHref.setAttribute('data-toggle', 'modal');
            switch (employeeFormModel.talkType) {
                case 1:
                    formHref.setAttribute('data-target', '#document');
                    formHref.addEventListener("click", (e) => {
                        this.formId = employeeFormModel.formId;
                        api.getEopById(employeeFormModel.formId)
                            .then((eop) => {
                                var fillEopForm = new FillEOPFormComponent();
                                fillEopForm.fillForm(eop, this);
                            });
                    });
                    break;
                case 2:
                    formHref.setAttribute('data-target', '#documentPat');
                    formHref.addEventListener("click", (e) => {
                        this.formId = employeeFormModel.formId;
                        api.getPatById(employeeFormModel.formId)
                            .then((pat) => {
                                var fillPatForm = new FillPATFormComponent();
                                fillPatForm.fillForm(pat, this);
                            });
                    });
                    break;
                case 3:
                    formHref.setAttribute('data-target', '#documentAdHoc');
                    formHref.addEventListener('click', (e) => {
                        api.getAdHocById(employeeFormModel.formId)
                            .then((adHoc) => {
                                this._adHocForm = adHoc;
                                this.employeeId = adHoc.employeeId;
                                var fillAdHocForm = new FillAdHocForm();
                                fillAdHocForm.fillForm(adHoc, this);
                            });
                    });
                    break;
                case 4:
                    formHref.setAttribute('data-target', '#exitupdate-talk');
                    formHref.addEventListener('click', (e) => {
                        this.formId = employeeFormModel.formId;
                        api.getExitById(this.formId)
                            .then((exitForm) => {
                                var fillForm = new FillExitForm();
                                fillForm.fillForm(exitForm);
                                this.getEmployeeById(exitForm.employeeId).then((emp) => {
                                    document.getElementById('employee-name-exitupdate').textContent = `${emp.firstName} ${emp.lastName}`;
                                    var employmentDateExitSpan = document.getElementById('employment-date-exitupdate');
                                    employmentDateExitSpan.textContent = emp.employmentDate;
                                    var positionSpan = document.getElementById('position-exitupdate');
                                    var commonFunctions = new FbhrCommon();
                                    commonFunctions.getEmployeePosition(emp.hrPosId).then((position) => {
                                        positionSpan.textContent = position.name;
                                    });
                                    var fbProfile = document.getElementById('exitupdate-fb-profile');
                                    const elImg = document.createElement('img');
                                    elImg.setAttribute('alt', 'Name, Last Name');
                                    elImg.setAttribute('src', config.imageUrl + `${emp.imageUrl}`);
                                    fbProfile.appendChild(elImg);
                                })
                            });
                    });
                    break;
                default:
                    break;
            }

            //
            formItemDiv.appendChild(formHref);
            const formRowDiv = document.createElement('div');
            formRowDiv.setAttribute('class', 'row');
            formItemDiv.appendChild(formRowDiv);

            const rowContentDiv = document.createElement('div');
            rowContentDiv.setAttribute('class', 'col-12 d-flex justify-content-between');

            formRowDiv.appendChild(rowContentDiv);

            const rowEmployeeDiv = document.createElement('div');
            rowEmployeeDiv.setAttribute('class', 'd-flex justify-content-start align-items-center');

            rowContentDiv.appendChild(rowEmployeeDiv);

            const imgDiv = document.createElement('div');
            imgDiv.setAttribute('class', 'fb-profile');

            const elImg = document.createElement('img');
            elImg.setAttribute('src', config.imageUrl + `${employeeFormModel.imageUrl}`);
            elImg.setAttribute('alt', 'Name,Last Name');
            imgDiv.appendChild(elImg);
            rowEmployeeDiv.appendChild(imgDiv);

            const mainEmployeeDataDiv = document.createElement('div');
            mainEmployeeDataDiv.setAttribute('class', 'pl-2');

            const employeeNameDiv = document.createElement('div');
            employeeNameDiv.setAttribute('class', 'fb-employee-name');
            employeeNameDiv.textContent = `${employeeFormModel.employeeName}`;
            mainEmployeeDataDiv.appendChild(employeeNameDiv);

            const employeeDataDiv = document.createElement('div');
            employeeDataDiv.setAttribute('class', 'fb-employee-data');

            const unitSpan = document.createElement('span');
            unitSpan.setAttribute('class', 'fb-employee-unit d-inline-block text-nowrap text-truncate');
            unitSpan.textContent = employeeFormModel.unitId == 0 ? 'undefined unit' : `${employeeFormModel.unitName.split(`-`)[0]}`;

            const teamSpan = document.createElement('span');
            teamSpan.setAttribute('class', 'fb-employee-team d-inline-block text-nowrap text-truncate');
            teamSpan.textContent = employeeFormModel.teamId == 0 ? 'undefined team' : `${employeeFormModel.teamName}`;

            employeeDataDiv.appendChild(unitSpan);
            employeeDataDiv.appendChild(teamSpan);

            mainEmployeeDataDiv.appendChild(employeeDataDiv);

            rowEmployeeDiv.appendChild(mainEmployeeDataDiv);
            const followUpFlagDiv = document.createElement('div');
            followUpFlagDiv.setAttribute('class', 'd-flex justify-content-end align-items-start');

            rowContentDiv.appendChild(followUpFlagDiv);

            const badgeFollowUpDiv = document.createElement('div');

            if (employeeFormModel.isOpenForFollowUp == false) {
                badgeFollowUpDiv.setAttribute('class', 'badge badge-success w-100');
                badgeFollowUpDiv.textContent = 'Done';
            } else {
                badgeFollowUpDiv.setAttribute('class', 'badge badge-danger w-100');
                badgeFollowUpDiv.textContent = 'Follow up open';
            }
            followUpFlagDiv.appendChild(badgeFollowUpDiv);

            formRowDiv.appendChild(rowContentDiv);

            const divInfo = document.createElement('div');
            divInfo.setAttribute('class', 'row mt-2');
            formItemDiv.appendChild(divInfo);

            //////////////
            const divLoc = document.createElement('div');
            divLoc.setAttribute('class', 'col-3');
            divInfo.appendChild(divLoc);

            const divLocInfo = document.createElement('small');
            divLoc.appendChild(divLocInfo);
            divLocInfo.textContent = 'Location';

            const divLocBadge = document.createElement('div');
            divLoc.appendChild(divLocBadge);

            const spanLoc = document.createElement('span');
            spanLoc.setAttribute('class', 'badge badge-light w-100');
            divLocBadge.appendChild(spanLoc);
            spanLoc.textContent = `${employeeFormModel.location}`;


            //////////////
            const divYears = document.createElement('div');
            divYears.setAttribute('class', 'col-3');
            divInfo.appendChild(divYears);

            const divYearsInfo = document.createElement('small');
            divYears.appendChild(divYearsInfo);
            divYearsInfo.textContent = 'Years working';

            const divYearsBadge = document.createElement('div');
            divYears.appendChild(divYearsBadge);

            const spanYears = document.createElement('span');
            spanYears.setAttribute('class', 'badge badge-light w-100');
            divYearsBadge.appendChild(spanYears);
            spanYears.textContent = `${employeeFormModel.yearsWorking}`;

            //////////////
            const divTalkType = document.createElement('div');
            divTalkType.setAttribute('class', 'col-3');
            divInfo.appendChild(divTalkType);
            const divTalkTypeInfo = document.createElement('small');
            divTalkType.appendChild(divTalkTypeInfo);
            divTalkTypeInfo.textContent = 'Talk type';

            const divTalkTypeBadge = document.createElement('div');
            divTalkType.appendChild(divTalkTypeBadge);
            const spanTalkType = document.createElement('span');
            spanTalkType.setAttribute('class', 'badge badge-light w-100');
            divTalkTypeBadge.appendChild(spanTalkType);
            spanTalkType.textContent = this.getType(employeeFormModel.talkType);

            //////////////
            const divTalkDate = document.createElement('div');
            divTalkDate.setAttribute('class', 'col-3');
            divInfo.appendChild(divTalkDate);

            const divTalkDateInfo = document.createElement('small');
            divTalkDate.appendChild(divTalkDateInfo);
            divTalkDateInfo.textContent = 'Talk date';

            const divTalkDateBadge = document.createElement('div');
            divTalkDate.appendChild(divTalkDateBadge);

            const spanTalkDate = document.createElement('span');
            spanTalkDate.setAttribute('class', 'badge badge-light w-100');
            divTalkDateBadge.appendChild(spanTalkDate);
            spanTalkDate.textContent = this.stringHelper.getShortDateFromString(employeeFormModel.talkDate);

        });

        this.getUnresolvedForms().then((unr) => {
            const bell_notification = document.getElementById('unresolved-count-index');
            bell_notification.textContent = Object.keys(unr).length;

            const cta = document.getElementById('unresolved-index');
            cta.addEventListener("click", (e) => {
                this.showUnresolved(this.unresolved);
            })
        })

        // EOP buttons
        const ntSaveUpdate = document.getElementById('nt-save-update-eop');
        ntSaveUpdate.addEventListener("click", (e) => {
            ntSaveUpdate.setAttribute('data-toggle', 'modal');
            ntSaveUpdate.setAttribute('data-target', '#save-modal-index');
            ntSaveUpdate.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save-index');
            yesSaveUpdate.addEventListener('click', (e) => {
                var updateEOP = new UpdateEOP();
                updateEOP.onUpdateEvent(this.formId);
            });
        });

        const cancelEopUpdate = document.getElementById('cancel-eop-index');
        cancelEopUpdate.addEventListener("click", (e) => {
            cancelEopUpdate.setAttribute('data-toggle', 'modal');
            cancelEopUpdate.setAttribute('data-target', '#cancel-modal-index');
            cancelEopUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closeEopUpdate = document.getElementById('close-eop-index');
        closeEopUpdate.addEventListener("click", (e) => {
            closeEopUpdate.setAttribute('data-toggle', 'modal');
            closeEopUpdate.setAttribute('data-target', '#cancel-modal-index');
            closeEopUpdate.setAttribute('data-focus-on', 'input:first');
        })

        // PAT buttons
        const ntSaveUpdatePAT = document.getElementById('nt-save-update-pat');
        ntSaveUpdatePAT.addEventListener("click", (e) => {
            ntSaveUpdatePAT.setAttribute('data-toggle', 'modal');
            ntSaveUpdatePAT.setAttribute('data-target', '#save-modal-index');
            ntSaveUpdatePAT.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save-index');
            yesSaveUpdate.addEventListener("click", (e) => {
                var updatePAT = new UpdatePAT();
                updatePAT.onUpdateEvent(this.formId);
            });
        });

        const cancelPatUpdate = document.getElementById('cancel-pat-index');
        cancelPatUpdate.addEventListener("click", (e) => {
            cancelPatUpdate.setAttribute('data-toggle', 'modal');
            cancelPatUpdate.setAttribute('data-target', '#cancel-modal-index');
            cancelPatUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closePatUpdate = document.getElementById('close-pat-index');
        closePatUpdate.addEventListener("click", (e) => {
            closePatUpdate.setAttribute('data-toggle', 'modal');
            closePatUpdate.setAttribute('data-target', '#cancel-modal-index');
            closePatUpdate.setAttribute('data-focus-on', 'input:first');
        })

        // Ad-Hoc buttons
        const ntSaveUpdateAdHoc = document.getElementById('ad-hoc-update');
        ntSaveUpdateAdHoc.addEventListener("click", (e) => {
            ntSaveUpdateAdHoc.setAttribute('data-toggle', 'modal');
            ntSaveUpdateAdHoc.setAttribute('data-target', '#save-modal-index');
            ntSaveUpdateAdHoc.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save-index');
            yesSaveUpdate.addEventListener("click", (e) => {
                var updateAdHoc = new UpdateAdHocForm(this.employeeId);
                updateAdHoc.onUpdateAdHocFormEvent(this._adHocForm);
            });
        });

        const cancelAdHocUpdate = document.getElementById('cancel-ad-hoc-index');
        cancelAdHocUpdate.addEventListener("click", (e) => {
            cancelAdHocUpdate.setAttribute('data-toggle', 'modal');
            cancelAdHocUpdate.setAttribute('data-target', '#cancel-modal-index');
            cancelAdHocUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closeAdHocUpdate = document.getElementById('close-ad-hoc-index');
        closeAdHocUpdate.addEventListener("click", (e) => {
            console.log('ovde');
            closeAdHocUpdate.setAttribute('data-toggle', 'modal');
            closeAdHocUpdate.setAttribute('data-target', '#cancel-modal-index');
            closeAdHocUpdate.setAttribute('data-focus-on', 'input:first');
        })
        if (buttonApply = document.getElementById('applyFilter') != null) {
            var buttonApply = document.getElementById('applyFilter').addEventListener('click',

                () => {

                    this.getFilter();
                })
        }

        document.getElementById('applyFilter')
            .addEventListener('click',

                () => {

                    this.getFilter();
                }
            )
    }

    getType(formType) {

        switch (formType) {
            case 1:
                return 'EOP';
            case 2:
                return 'PAT';
            case 3:
                return 'AdHoc';
            case 4:
                return 'Exit';
            default:
                return 'Undefined';

        }
    }

    showUnits(units) {
        const select = document.getElementById('selectUnit');
        const opt = document.createElement('option');
        opt.value = "";
        opt.selected = true;
        var name = document.createTextNode("select unit");
        opt.appendChild(name);
        select.appendChild(opt);

        units.forEach((unit) => {
            const select = document.getElementById('selectUnit');

            const opt = document.createElement('option');
            opt.value = unit.id;
            var name = document.createTextNode(unit.name);
            opt.appendChild(name);
            select.appendChild(opt);
        });
    }

    showTeams(teams) {
        const select = document.getElementById('selectTeam');
        const opt = document.createElement('option');
        opt.value = "";
        opt.selected = true;
        var name = document.createTextNode("select team");
        opt.appendChild(name);
        select.appendChild(opt);
        teams.forEach((team) => {
            const select = document.getElementById('selectTeam');

            const opt = document.createElement('option');
            opt.value = team.id;
            var name = document.createTextNode(team.name);
            opt.appendChild(name);
            select.appendChild(opt);
        });
    }

    showLocations(locations) {
        const select = document.getElementById('selectLocation');
        const opt = document.createElement('option');
        opt.value = "";
        opt.selected = true;
        var name = document.createTextNode("select location");
        opt.appendChild(name);
        select.appendChild(opt);

        locations.forEach((location) => {
            const select = document.getElementById('selectLocation');

            const opt = document.createElement('option');
            opt.value = location.id;
            var name = document.createTextNode(location.name);
            opt.appendChild(name);
            select.appendChild(opt);
        });
    }

    getFilter() {

        var hasFollowUp;

        if (document.getElementById('isOpenFollowUp').value == "true")
            hasFollowUp = true;
        else if (document.getElementById('isOpenFollowUp').value == "false")
            hasFollowUp = false
        else hasFollowUp = null;

        var filter = {

            'LocationId': document.getElementById('selectLocation').value,
            'UnitId': document.getElementById('selectUnit').value,
            'TeamId': document.getElementById('selectTeam').value,
            'YearsFrom': parseInt(document.getElementById('yearFrom').value),
            'YearsTo': parseInt(document.getElementById('yearTo').value),
            'Type': parseInt(document.getElementById('formType').value),
            'YearOfForm': parseInt(document.getElementById('yearOfForm').value),
            'HasFollowUp': hasFollowUp

        };

        const URL = config.url + config.getFilteredForms;

        fetch(URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(filter),

        }).then(function (response) {
            if (response.ok) {
                return response.text();
            }
            return Promise.reject(response);
        }).then((data) => {

            this.showForms(JSON.parse(data));

        }).catch((error) => {

        });
    }

    showUnresolved() {
        this.getUnresolvedForms()
            .then((unresolved) => {
                const fb_info_item_fb_dropdown_cta = document.getElementById('unresolved-index');

                const dropdown_menu = document.createElement('div');
                dropdown_menu.setAttribute('class', 'fb-dropdown-menu');
                fb_info_item_fb_dropdown_cta.appendChild(dropdown_menu);

                const dropdown_menu_pointer = document.createElement('div');
                dropdown_menu_pointer.setAttribute('class', 'fb-dropdown-menu-pointer');
                dropdown_menu.appendChild(dropdown_menu_pointer);

                const title = document.createElement('h6');
                title.setAttribute('class', 'p-3 m-0');
                title.textContent = 'Opened talks reminder';
                dropdown_menu.appendChild(title);

                const wrapper = document.createElement('div');
                wrapper.setAttribute('class', 'fb-notification-wrapper');
                dropdown_menu.appendChild(wrapper);

                unresolved.forEach((form) => {

                    const employee = document.createElement('div');
                    employee.setAttribute('class', 'fb-employee');
                    wrapper.appendChild(employee);

                    const doc_item = document.createElement('div');
                    doc_item.setAttribute('class', 'fb-doc-item mb-3');
                    employee.appendChild(doc_item);

                    const link = document.createElement('a');
                    link.setAttribute('href', 'javascript:void(0)');
                    link.setAttribute('class', 'div-link');
                    link.setAttribute('data-toggle', 'modal');

                    switch (form.talkType) {
                        case 1:
                            link.setAttribute('data-target', '#document');
                            link.addEventListener("click", (e) => {
                                this.formId = form.formId;
                                api.getEopById(form.formId)
                                    .then((eop) => {
                                        var fillEopForm = new FillEOPFormComponent();
                                        fillEopForm.fillForm(eop, this);
                                    });
                            });
                            break;
                        case 2:
                            link.setAttribute('data-target', '#documentPat');
                            link.addEventListener("click", (e) => {
                                this.formId = form.formId;
                                api.getPatById(form.formId)
                                    .then((pat) => {
                                        var fillPatForm = new FillPATFormComponent();
                                        fillPatForm.fillForm(pat, this);
                                    });
                            });
                            break;
                        case 3:
                            link.setAttribute('data-target', '#documentAdHoc');
                            link.addEventListener('click', (e) => {
                                api.getAdHocById(form.formId)
                                    .then((adHoc) => {
                                        this._adHocForm = adHoc;

                                        var fillAdHocForm = new FillAdHocForm();
                                        fillAdHocForm.fillForm(adHoc, this);
                                    });
                            });
                            break;

                        default:
                            break;
                    }
                    doc_item.appendChild(link);

                    const row = document.createElement('div');
                    row.setAttribute('class', 'row');
                    doc_item.appendChild(row);

                    const div1_row = document.createElement('div');
                    div1_row.setAttribute('class', 'col-12 d-flex justify-content-start align-items-center');
                    row.appendChild(div1_row);

                    const profile = document.createElement('div');
                    profile.setAttribute('class', 'fb-profile');
                    div1_row.appendChild(profile);

                    //this.getEmployeeById(form.employeeId).then((emp) => {

                    const img = document.createElement('img');
                    img.setAttribute('src', config.imageUrl + `${form.imageUrl}`);
                    img.setAttribute('alt', 'Name LastName');
                    profile.appendChild(img);

                    const div_pl2 = document.createElement('div');
                    div_pl2.setAttribute('class', 'pl-2');
                    div1_row.appendChild(div_pl2);

                    const empName = document.createElement('div');
                    empName.setAttribute('class', 'fb-employee-name');
                    //empName.textContent = `${emp.firstName} ${emp.lastName}`;
                    empName.textContent = `${form.employeeName}`;
                    div_pl2.appendChild(empName);

                    //this.getEmployeeTeam(emp.teamId).then((team) => {
                    const empTeam = document.createElement('div');
                    empTeam.setAttribute('class', 'fb-employee-team');
                    //empTeam.textContent = emp.teamId == 0 ? 'undefined team' : team.name;
                    empTeam.textContent = form.teamId == 0 ? 'undefined team' : form.teamName;
                    div_pl2.appendChild(empTeam);
                    //})
                    //})

                    const div2_row = document.createElement('div');
                    div2_row.setAttribute('class', 'col-12 text-right mt-2');
                    row.appendChild(div2_row);

                    const spanDate = document.createElement('span');
                    spanDate.setAttribute('class', 'badge badge-info');
                    spanDate.textContent = this.stringHelper.getShortDateFromString(form.followUpDate);
                    div2_row.appendChild(spanDate);

                    const spanType = document.createElement('span');
                    spanType.setAttribute('class', 'badge badge-secondary');
                    spanType.textContent = this.getType(form.talkType);
                    div2_row.appendChild(spanType);

                    const spanOpen = document.createElement('span');
                    spanOpen.setAttribute('class', 'badge badge-danger');
                    spanOpen.textContent = 'Follow up open';
                    div2_row.appendChild(spanOpen);
                });
            })
    }
}

document.addEventListener('DOMContentLoaded', () => {
    window.app = new App();
});

