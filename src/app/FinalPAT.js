import { Component } from './core/Component';
import config from './config';
import getPerformanceFactorMark from './utility/GetPerformanceFactor';
import { UpdatePAT } from './UpdatePAT';
import { FinalEOP } from './FinalEOP';

export class FinalPAT extends Component {
    constructor(employeeDocIdPat) {
        super();
        this.employeeDocIdPat = employeeDocIdPat;

        this.showFinalPAT(this.employeeDocIdPat);
    }

    getFinalPATMatrix(PatId) {
        const URL = config.url + config.getFinalPatMatrix + `${PatId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const FinalPATMatrix = JSON.parse(data);
                return Promise.resolve(FinalPATMatrix);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getFinalPATComments(PatId) {
        const URL = config.url + config.getFinalPatComments + `${PatId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const FinalPATComments = JSON.parse(data);
                return Promise.resolve(FinalPATComments);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }
    
    showFinalPAT(employeeDocIdPat) {
        if (employeeDocIdPat != null) { 
            var updatePATfinal = new UpdatePAT();
            updatePATfinal.getPatById(employeeDocIdPat)
                .then((PAT) => {
                    var finaleop = new FinalEOP();
                    finaleop.getEmployeeById(PAT.employeeId)
                        .then((employee) => {
                            const newPatFinalProfileImgDiv = document.getElementById('fb-profile-pat-final');
                            const newPatFinalImg = document.createElement('img');
                            newPatFinalImg.setAttribute('src', config.imageUrl + `${employee.imageUrl}`);
                            newPatFinalProfileImgDiv.appendChild(newPatFinalImg);
                            const patFinalModalProfileNameDiv = document.getElementById('Modal-Fb-Profile-Name-Pat-final');
                            patFinalModalProfileNameDiv.textContent = `${employee.firstName} ${employee.lastName}`;
                        })
                })
            this.getFinalPATMatrix(employeeDocIdPat)
                .then((data) => {
                    document.getElementById('Pqu00').textContent = getPerformanceFactorMark(data[0][0].Mark);
                    document.getElementById('Pqu01').textContent = getPerformanceFactorMark(data[0][1].Mark);
                    document.getElementById('Pqu02').textContent = getPerformanceFactorMark(data[0][2].Mark);

                    document.getElementById('Ppr10').textContent = getPerformanceFactorMark(data[1][0].Mark);
                    document.getElementById('Ppr11').textContent = getPerformanceFactorMark(data[1][1].Mark);
                    document.getElementById('Ppr12').textContent = getPerformanceFactorMark(data[1][2].Mark);

                    document.getElementById('Ppk20').textContent = getPerformanceFactorMark(data[2][0].Mark);
                    document.getElementById('Ppk21').textContent = getPerformanceFactorMark(data[2][1].Mark);
                    document.getElementById('Ppk22').textContent = getPerformanceFactorMark(data[2][2].Mark);

                    document.getElementById('Pre30').textContent = getPerformanceFactorMark(data[3][0].Mark);
                    document.getElementById('Pre31').textContent = getPerformanceFactorMark(data[3][1].Mark);
                    document.getElementById('Pre32').textContent = getPerformanceFactorMark(data[3][2].Mark);

                    document.getElementById('Pco40').textContent = getPerformanceFactorMark(data[4][0].Mark);
                    document.getElementById('Pco41').textContent = getPerformanceFactorMark(data[4][1].Mark);
                    document.getElementById('Pco42').textContent = getPerformanceFactorMark(data[4][2].Mark);

                    document.getElementById('Ptw50').textContent = getPerformanceFactorMark(data[5][0].Mark);
                    document.getElementById('Ptw51').textContent = getPerformanceFactorMark(data[5][1].Mark);
                    document.getElementById('Ptw52').textContent = getPerformanceFactorMark(data[5][2].Mark);

                    document.getElementById('Pau60').textContent = getPerformanceFactorMark(data[6][0].Mark);
                    document.getElementById('Pau61').textContent = getPerformanceFactorMark(data[6][1].Mark);
                    document.getElementById('Pau62').textContent = getPerformanceFactorMark(data[6][2].Mark);

                    document.getElementById('ps70').textContent = getPerformanceFactorMark(data[7][0].Mark);
                    document.getElementById('ps71').textContent = getPerformanceFactorMark(data[7][1].Mark);
                    document.getElementById('ps72').textContent = getPerformanceFactorMark(data[7][2].Mark);
                });
            
            this.getFinalPATComments(employeeDocIdPat)
                .then((data) => {
                    document.getElementById('FinalQualityClPAT').value = data[0][0];
                    document.getElementById('FinalQualityLmPAT').value = data[0][1];
                    document.getElementById('FinalQualityTlPAT').value = data[0][2];

                    document.getElementById('FinalApprasierClPAT').value = data[1][0];
                    document.getElementById('FinalApprasierLmPAT').value = data[1][1];
                    document.getElementById('FinalApprasierTlPAT').value = data[1][2];

                    document.getElementById('FinalPerformanceClPAT').value = data[2][0];
                    document.getElementById('FinalPerformanceLmPAT').value = data[2][1];
                    document.getElementById('FinalPerformanceTlPAT').value = data[2][2];

                    document.getElementById('FinalCareerClPAT').value = data[3][0];
                    document.getElementById('FinalCareerLmPAT').value = data[3][1];
                    document.getElementById('FinalCareerTlPAT').value = data[3][2];

                    document.getElementById('FinalEducationClPAT').value = data[4][0];
                    document.getElementById('FinalEducationLmPAT').value = data[4][1];
                    document.getElementById('FinalEducationTlPAT').value = data[4][2];

                    document.getElementById('FinalOtherClPAT').value = data[5][0];
                    document.getElementById('FinalOtherLmPAT').value = data[5][1];
                    document.getElementById('FinalOtherTlPAT').value = data[5][2];
            })
        }
    };
}
        
document.addEventListener('DOMContentLoaded', () => {
    const urlParamsPAT = new URLSearchParams(window.location.search);
    const employeeDocIdPat = urlParamsPAT.get('patid');
    window.app = new FinalPAT(employeeDocIdPat);
});