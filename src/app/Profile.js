import { Component } from './core/Component';
import config from './config';
import { App } from './App';
import { EOPModel } from './models/EOPModel';
import { PATModel } from './models/PATModel';
import { UpdateEOP } from './UpdateEOP';
import { UpdatePAT } from './UpdatePAT';
import * as api from './apiJs/Index';
import { FillEOPFormComponent } from './common/FillEOPFormComponent';
import { FillPATFormComponent } from './common/FillPATFormComponent';
import { StringHelper } from './utility/StringHelper';
import { ValidationHelper } from './utility/ValidationHelper';
import { FillAdHocForm } from './common/AdHocForm/FillAdHocFormComponent';
import { CreateAdHocForm } from './services/adHoc/CreateAdHocFormService';
import { UpdateAdHocForm } from './services/adHoc/UpdateAdHocFormService';
import { FinalEOP } from './FinalEOP';
import { doc } from 'prettier';
import { FillExitForm } from './common/ExitForm/FillExitForm';
import { FinalPAT } from './FinalPAT';

export class Profile extends Component {
    constructor(id) {
        super();
        this.formId = null;
        const imgLogo = document.getElementById("imgLogo");
        if (imgLogo) imgLogo.setAttribute('href', config.urlApplication);
        this.employeeId = id;
        this.getEmployee(this.employeeId);
        this.stringHelper = new StringHelper();
        this.validationHelper = new ValidationHelper();
    }

    createPAT(PAT) {
        const URL = config.url + config.createPAT;
        const date = PAT.dateOfTalk;
        const line = PAT.lineManagerId;
        const team = PAT.teamLeadId;
        const data = {
            DateOfTalk: date,
            FormType: 2,
            EmployeeId: PAT.employeeId,
            HRReporter: document.getElementById('pat-reporter-createdby').value,
            LineManagerId: line,
            TeamLeadId: team
        };

        fetch(URL, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.text())
            .catch((error) => {
                console.log(error);
            })
        Notiflix.Notify.Success('sucessfully saved');
    };

    createEOP(EOP) {
        const URL = config.url + config.createEOP;
        const date = EOP.dateOfTalk;
        const line = EOP.lineManagerId;
        const team = EOP.teamLeadId;
        const startDate = EOP.startDate;
        const endDate = EOP.endDate;
        const data = {
            DateOfTalk: date,
            FormType: 1,
            EmployeeId: EOP.employeeId,
            HRReporter: document.getElementById('eop-reporter-createdby').value,
            LineManagerId: line,
            TeamLeadId: team,
            ReportEOP: {
                StartDate: startDate,
                EndDate: endDate
            }
        };

        fetch(URL, {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => response.text())
            .catch((error) => {
                console.log(error);
            })
        Notiflix.Notify.Success('sucessfully saved');
    };

    createExit() {
        const URL = config.url + config.createExit;
        const file = document.getElementById('file-upload-exit').files[0];

        var formData = new FormData();
        formData.append('FormType', 4);
        formData.append('DateOfTalk', document.getElementById('create-date-exit').value);
        formData.append('HRNotes', document.getElementById('hr-notes-exit').value);
        formData.append('EmployeeId', this.employeeId);
        formData.append('Questionary', file);

        const options = {
            method: 'POST',
            body: formData,
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        };

        delete options.headers['Content-Type'];

        fetch(URL, options)
            .then((response) => response.text())
            .catch((error) => {
                console.log(error);
            });
        Notiflix.Notify.Success('sucessfully saved');
    };

    getEmployee(id) {
        const URL = config.url + config.getEmployee + `${id}`;
        fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                this.employee = JSON.parse(data);
                this.showEmployee();
            })
            .catch((error) => {
            })
    }

    getTeamLead(id) {
        const URL = config.url + config.getTeamLead + `${id}`;

        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const employee = JSON.parse(data);
                return Promise.resolve(employee);
            })
            .catch((error) => {
            })
    }

    getLineManager(id) {
        const URL = config.url + config.getLineManager + `${id}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const employee = JSON.parse(data);
                return Promise.resolve(employee);

            })
            .catch((error) => {
            })
    }

    getYears(id) {
        const URL = config.url + config.getYears + `${id}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const years = JSON.parse(data);
                return Promise.resolve(years);

            })
            .catch((error) => {
                console.log(error);
            })
    }

    getEndDate(dateString) {
        const URL = config.url + config.getEndEOPDate + `${dateString}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                return Promise.resolve(data);
            })
            .catch((error) => {
            })
    }

    getStartDate(dateString) {
        const URL = config.url + config.getStartEOPDate + `${dateString}`;

        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                return Promise.resolve(data);
            })
            .catch((error) => {
                console.log(error)
            })
    }

    getEmployeePosition() {
        const URL = config.url + config.getPosition + this.employee.hrPosId;

        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const position = JSON.parse(data);
                return Promise.resolve(position);
            })
            .catch((error) => {
            })
    }

    getFormType(talkType) {
        var result = "";
        switch (talkType) {
            case 1:
                result = "End Of Probation";
                break;
            case 2:
                result = "Performance Appraisal Talk";
                break;
            case 3:
                result = "Ad Hoc Meeting";
                break;
            case 4:
                result = "Exit Interview";
                break;
            default:
                break;
        }
        return result;
    }

    getUnresolvedForms() {
        const URL = config.url + config.getUnresolvedForm;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const unresolved = JSON.parse(data);
                return Promise.resolve(unresolved);
            })
            .catch((error) => {
                console.log(error);
            })
    }

    showEmployeeDoc(employeeDoc) {
        const main_div = document.getElementById('recent-documents');

        //creating first div
        const div1 = document.createElement('div');
        div1.setAttribute('class', 'col-12 col-xl-6');
        main_div.appendChild(div1);

        //creating second div
        const div2_in_div1 = document.createElement('div');
        div2_in_div1.setAttribute('class', 'fb-doc-item mb-4');
        div1.appendChild(div2_in_div1);

        //creating href element
        const aHref_in_div2 = document.createElement('a');
        aHref_in_div2.setAttribute('href', 'javascript:void(0)');
        aHref_in_div2.setAttribute('class', 'div-link1');
        aHref_in_div2.setAttribute('data-toggle', 'modal');

        switch (employeeDoc.talkType) {
            case 1:
                aHref_in_div2.setAttribute('data-target', '#document');
                aHref_in_div2.addEventListener("click", (e) => {
                    this.formId = employeeDoc.formId;
                    api.getEopById(employeeDoc.formId)
                        .then((eop) => {
                            var fillEopForm = new FillEOPFormComponent();
                            fillEopForm.fillForm(eop, new App());
                        });
                });
                break;
            case 2:
                aHref_in_div2.setAttribute('data-target', '#documentPat');
                aHref_in_div2.addEventListener("click", (e) => {
                    this.formId = employeeDoc.formId;
                    api.getPatById(employeeDoc.formId)
                        .then((pat) => {
                            var fillPatForm = new FillPATFormComponent();
                            fillPatForm.fillForm(pat, new App());
                        });
                });
                break;
            case 3:
                aHref_in_div2.setAttribute('data-target', '#documentAdHoc');
                aHref_in_div2.addEventListener('click', (e) => {
                    api.getAdHocById(employeeDoc.formId)
                        .then((adHoc) => {
                            this._adHocForm = adHoc;
                            var fillAdHocForm = new FillAdHocForm();
                            fillAdHocForm.fillForm(adHoc, new App());
                        });
                });
                break;
            case 4:
                aHref_in_div2.setAttribute('data-target', '#exitupdate-talk');
                aHref_in_div2.addEventListener('click', (e) => {
                    this.formId = employeeDoc.formId;
                    api.getExitById(this.formId)
                        .then((exitForm) => {
                            var fillForm = new FillExitForm();
                            fillForm.fillForm(exitForm);
                            document.getElementById('employee-name-exitupdate').textContent = `${this.employee.firstName} ${this.employee.lastName}`;
                            var employmentDateExitSpan = document.getElementById('employment-date-exitupdate');
                            employmentDateExitSpan.textContent = this.employee.employmentDate;

                            var positionSpan = document.getElementById('position-exitupdate');
                            this.getEmployeePosition().then(
                                (position) => positionSpan.textContent = position.name
                            );

                            var fbProfile = document.getElementById('exitupdate-fb-profile');
                            const elImg = document.createElement('img');
                            elImg.setAttribute('alt', 'Name, Last Name');
                            elImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);
                            fbProfile.appendChild(elImg);
                        });
                });
                break;
            default:
                break;
        }
        div2_in_div1.appendChild(aHref_in_div2);

        //creating third div
        const div3_in_div2 = document.createElement('div');
        div3_in_div2.setAttribute('class', 'row');
        div2_in_div1.appendChild(div3_in_div2);

        const div4_in_div3 = document.createElement('div');
        div4_in_div3.setAttribute('class', 'col-12 d-flex justify-content-between');
        div3_in_div2.appendChild(div4_in_div3);

        const div5_in_div4 = document.createElement('div');
        div5_in_div4.setAttribute('class', 'fb-employee-name');
        const form_type = this.getFormType(employeeDoc.talkType);
        div5_in_div4.textContent = `${form_type}`;

        const div6_in_div4 = document.createElement('div');
        div6_in_div4.setAttribute('class', 'd-flex justify-content-end align-items-start');
        div4_in_div3.appendChild(div5_in_div4);
        div4_in_div3.appendChild(div6_in_div4);

        const div7_in_div6 = document.createElement('div');

        if (employeeDoc.isOpenForFollowUp == false) {
            div7_in_div6.setAttribute('class', 'badge badge-success w-100');
            div7_in_div6.textContent = `done`;
        }
        else {
            div7_in_div6.setAttribute('class', 'badge badge-danger w-100');
            div7_in_div6.textContent = `is open for follow up`;
        }
        div6_in_div4.appendChild(div7_in_div6);

        const div8_in_div2 = document.createElement('div');
        div8_in_div2.setAttribute('class', 'row mt-2');
        div2_in_div1.appendChild(div8_in_div2);

        const div9_in_div8 = document.createElement('div');
        div9_in_div8.setAttribute('class', 'col-4');

        const small_in_div9 = document.createElement('small');
        small_in_div9.textContent = `talk date`;
        const div13_in_div9 = document.createElement('div');
        const span_in_div13 = document.createElement('span');
        span_in_div13.textContent = `${this.stringHelper.getShortDateFromString(employeeDoc.dateOfTalk)}`;
        span_in_div13.setAttribute('class', 'badge badge-light w-100');
        div13_in_div9.appendChild(span_in_div13);
        div9_in_div8.appendChild(small_in_div9);
        div9_in_div8.appendChild(div13_in_div9);

        const div10_in_div8 = document.createElement('div');
        div10_in_div8.setAttribute('class', 'col-4');

        const small_in_div10 = document.createElement('small');
        small_in_div10.textContent = `for year`;
        const div14_in_div10 = document.createElement('div');
        const span_in_div14 = document.createElement('span');
        span_in_div14.textContent = `${employeeDoc.yearOfForm}`;
        span_in_div14.setAttribute('class', 'badge badge-light w-100');
        div14_in_div10.appendChild(span_in_div14);
        div10_in_div8.appendChild(small_in_div10);
        div10_in_div8.appendChild(div14_in_div10);

        const div11_in_div8 = document.createElement('div');
        div11_in_div8.setAttribute('class', 'col-4');

        const small_in_div11 = document.createElement('small');
        small_in_div11.textContent = `follow-up date`;
        const div15_in_div11 = document.createElement('div');
        const span_in_div15 = document.createElement('span');
        span_in_div15.setAttribute('class', 'badge badge-light w-100');

        var date = employeeDoc.followUpDate != null ? this.stringHelper.getShortDateFromString(employeeDoc.followUpDate) : null;
        date = date != null && parseInt(date.split('.')[2]) > 2000 ? date : null;
        if (employeeDoc.talkType == 3) {
            date = this.stringHelper.getShortDateFromString(employeeDoc.dateOfTalk);
        }
        span_in_div15.textContent = date != null ? date : `-`;
        div15_in_div11.appendChild(span_in_div15);
        div11_in_div8.appendChild(small_in_div11);
        div11_in_div8.appendChild(div15_in_div11);

        div8_in_div2.appendChild(div9_in_div8);
        div8_in_div2.appendChild(div10_in_div8);
        div8_in_div2.appendChild(div11_in_div8);

        // DELETE BUTTON
        const deleteRow = document.createElement('div');
        deleteRow.setAttribute('class', 'row mt-1 d-flex justify-content-center');

        const buttonBlock = document.createElement('button');
        buttonBlock.setAttribute('class', 'btn btn-sm');
        buttonBlock.setAttribute('type', 'button');
        buttonBlock.textContent = ' DELETE DOC';

        buttonBlock.addEventListener('click', () => {
            buttonBlock.setAttribute('data-toggle', 'modal');
            buttonBlock.setAttribute('data-target', '#delete-modal');
            buttonBlock.setAttribute('data-focus-on', 'input:first');
            var delUrl = "";

            const yesDelete = document.getElementById('yes-delete');
            yesDelete.addEventListener("click", (e) => {
                switch (employeeDoc.talkType) {
                    case 1:
                        delUrl = config.url + config.deleteEop + employeeDoc.formId;
                        break;
                    case 2:
                        delUrl = config.url + config.deletePat + employeeDoc.formId;
                        break;
                    case 3:
                        delUrl = config.url + config.deleteAdHoc + employeeDoc.formId;
                        break;
                    case 4:
                        delUrl = config.url + config.deleteExit + employeeDoc.formId;
                        break;
                }
                fetch(delUrl, {
                    method: 'DELETE'
                })
                    .then((response) => {
                        response.text();
                        console.log(response.status);
                        if (response.status >= 200 && response.status < 300)
                            Notiflix.Notify.Success('sucessfully deleted');
                        else
                            Notiflix.Notify.Failure('unsucessfully deleted');
                    })
                    .catch((err) => console.log(err));
            });
        });

        deleteRow.appendChild(buttonBlock);
        div2_in_div1.appendChild(deleteRow);

        switch (employeeDoc.talkType) {
            case 1:
                var updateEOP2 = new UpdateEOP();
                updateEOP2.getEopById(employeeDoc.formId)
                    .then((EOP) => {
                        document.getElementById('nt-create-final-eop').onclick = function () {
                            location.href = `finalEOP.html?docid=${EOP.id}`;
                        }
                    });
                break;
            case 2:
                var updatePAT2 = new UpdatePAT();
                updatePAT2.getPatById(employeeDoc.formId)
                    .then((PAT) => {
                        document.getElementById('nt-create-final-pat').onclick = function () {
                            location.href = `finalPAT.html?patid=${PAT.id}`;
                        }
                    });
                break;
        }
    }

    showEmployeeDocs() {
        const URL = config.url + config.getEmployeeDocuments;
        document.getElementById('recent-documents').innerHTML = "";

        fetch(URL, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': '*'
            },
            body: JSON.stringify(this.getEmployeeDocFilter())
        })
            .then((response) => response.text())
            .then((data) => {
                const employeeDocs = JSON.parse(data);
                employeeDocs.forEach(employeeDoc => {
                    this.showEmployeeDoc(employeeDoc);
                    this.newForm(employeeDoc);
                });
            })
            .catch((error) => {
                console.log(error);
            });
    }

    populateExitModal() {
        var employeeDateExitSpan = document.getElementById('employment-date-exit');
        employeeDateExitSpan.textContent = this.employee.employmentDate;

        var positionSpan = document.getElementById('position-exit');
        this.getEmployeePosition()
            .then((position) => {
                positionSpan.textContent = position.name;
            });

        var exitModalName = document.getElementById('employee-name-exit');
        exitModalName.textContent = `${this.employee.firstName} ${this.employee.lastName}`;

        var fbProfile = document.getElementById('exit-fb-profile');
        const elImg = document.createElement('img');
        elImg.setAttribute('alt', 'Name, Last Name');
        elImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);
        fbProfile.appendChild(elImg);

        var count = 0;
        const saveExit = document.getElementById('save-exit-create');
        saveExit.addEventListener("click", (e) => {
            if (this.validationHelper.validateExitCreateModal()) {
                saveExit.setAttribute('data-toggle', 'modal');
                saveExit.setAttribute('data-target', '#save-modal');
                saveExit.setAttribute('data-focus-on', 'input:first');

                const yesSaveCreate = document.getElementById('yes-save');
                yesSaveCreate.addEventListener("click", (e) => {
                    if (count < 1) {
                        this.createExit();
                        count++;
                    }
                });
            }
        });

        const cancelExit = document.getElementById('cancel-create-exit');
        cancelExit.addEventListener('click', (e) => {
            cancelExit.setAttribute('data-toggle', 'modal');
            cancelExit.setAttribute('data-target', '#cancel-modal');
            cancelExit.setAttribute('data-focus-on', 'input:first');
        })

        const closeExit = document.getElementById('close-create-exit');
        closeExit.addEventListener('click', (e) => {
            closeExit.setAttribute('data-toggle', 'modal');
            closeExit.setAttribute('data-target', '#cancel-modal');
            closeExit.setAttribute('data-focus-on', 'input:first');
        })

    };

    showEmployee() {
        const empployeeNameDiv = document.getElementById('employeeName');
        empployeeNameDiv.textContent = `${this.employee.firstName} ${this.employee.lastName}`;

        const DateOfBirthTd = document.getElementById('DateOfBirth');
        DateOfBirthTd.textContent = `${this.employee.birthDate}`;

        const profileFbDiv = document.getElementById('modal-fb-profile');
        const elImg = document.createElement('img');
        elImg.setAttribute('alt', 'Name, Last Name');
        elImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);
        profileFbDiv.appendChild(elImg);

        const eopModalProfileNameDiv = document.getElementById('modal-eop-profile-name');
        eopModalProfileNameDiv.textContent = `${this.employee.firstName} ${this.employee.lastName}`;

        const probationStart = document.getElementById('probation-start');
        probationStart.textContent = this.employee.employmentDate + '.';

        this.getEndDate(this.employee.employmentDate)
            .then((endDate) => {
                const probationEnd = document.getElementById('probation-end');
                probationEnd.textContent = `${this.stringHelper.getShortDateFromString(endDate)}`;
            });

        //AD HOC: Profile
        const adHocProfImgDiv = document.getElementById('ad-hoc-profile-image');
        const adHocImg = document.createElement('img');
        adHocImg.setAttribute('alt', 'Name, Last Name');
        adHocImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);
        adHocProfImgDiv.appendChild(adHocImg);

        const newAdHocProfileNameDiv = document.getElementById('ad-hoc-profile-name');
        newAdHocProfileNameDiv.textContent = `${this.employee.firstName} ${this.employee.lastName}`;

        const newEopImg = document.createElement('img');
        newEopImg.setAttribute('alt', 'Name, Last Name');
        newEopImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);
        const newEopProfileImgDiv = document.getElementById('Modal-Fb-Profile');
        newEopProfileImgDiv.appendChild(newEopImg);

        const newEopProfileNameDiv = document.getElementById('Modal-Fb-Profile-Name');
        newEopProfileNameDiv.textContent = `${this.employee.firstName} ${this.employee.lastName}`;

        //EMPLOYEE NAME AND IMG FOR NEW PAT
        const newPatImg = document.createElement('img');
        newPatImg.setAttribute('alt', 'Name, Last Name');
        newPatImg.setAttribute('src', config.imageUrl + `${this.employee.imageUrl}`);

        const newPatProfileImgDiv = document.getElementById('Modal-Fb-Profile-pat');
        newPatProfileImgDiv.appendChild(newPatImg);
        const newPatProfileNameDiv = document.getElementById('Modal-Fb-Profile-Name-pat');
        newPatProfileNameDiv.textContent = `${this.employee.firstName} ${this.employee.lastName}`;
        //
        const eopProbationStart = document.getElementById('probation-start-doc');
        eopProbationStart.textContent = this.employee.employmentDate + '.';

        this.getEndDate(this.employee.employmentDate)
            .then((endDate) => {
                const eopProbationEnd = document.getElementById('probation-end-doc');
                eopProbationEnd.textContent = `${this.stringHelper.getShortDateFromString(endDate)}`;
            });

        const app = new App();

        app.getEmployeeTeam(this.employee.teamId)
            .then((team) => {
                const teamDiv = document.getElementById('Team');
                teamDiv.textContent = this.employee.teamId == 0 ? 'undefined' : team.name;

            });

        app.getEmployeeUnit(this.employee.unitId)
            .then((unit) => {
                const unitDiv = document.getElementById('Unit');
                unitDiv.textContent = this.employee.unitId == 0 ? 'undefined' : unit.name;

            });

        this.getEmployeePosition()
            .then((position) => {
                const positionDiv = document.getElementById('Position');
                positionDiv.textContent = position.name;

            });

        this.getTeamLead(this.employee.id)
            .then((teamLead) => {
                const teamLeadDiv = document.getElementById('TeamLead');
                teamLeadDiv.textContent = teamLead.id == null ? 'undefined' : `${teamLead.firstName} ${teamLead.lastName}`;
            });

        this.getLineManager(this.employee.id)
            .then((lineManager) => {
                const lineManagerDiv = document.getElementById('LineManager');
                if (lineManager != null)
                    lineManagerDiv.textContent = lineManager.id == null ? 'undefined' : `${lineManager.firstName} ${lineManager.lastName}`;
                else
                    lineManagerDiv.textContent = 'undefined';
            });

        this.getYears(this.employee.id)
            .then((years) => {
                const yearsDiv = document.getElementById('YearsWithEnjoying');
                yearsDiv.textContent = years;
            });

        this.showEmployeeDocs();

        // EOP buttons
        var eop = new EOPModel();
        var counter = 0;
        const ntSaveCreate = document.getElementById('nt-save-create');
        ntSaveCreate.addEventListener("click", (e) => {
            if (this.validationHelper.validateEOPCreateModal()) {
                ntSaveCreate.setAttribute('data-toggle', 'modal');
                ntSaveCreate.setAttribute('data-target', '#save-modal');
                ntSaveCreate.setAttribute('data-focus-on', 'input:first');

                eop.dateOfTalk = document.getElementById('talk-date-input').value;
                eop.employeeId = this.employee.id;

                this.getLineManager(this.employee.id)
                    .then((lineManager) => {
                        eop.lineManagerId = lineManager != null ? lineManager.id : null;

                        this.getTeamLead(this.employee.id)
                            .then((teamLead) => {
                                eop.teamLeadId = teamLead.id;

                                this.getStartDate(this.employee.employmentDate)
                                    .then((startDate) => {
                                        eop.startDate = startDate.replace('"', '').replace('"', '');

                                        this.getEndDate(this.employee.employmentDate)
                                            .then((endDate) => {
                                                eop.endDate = endDate.replace('"', '').replace('"', '');
                                            });
                                    });
                            });
                    });

                const yesSaveCreate = document.getElementById('yes-save');
                yesSaveCreate.addEventListener("click", (e) => {
                    if (counter < 1) {
                        this.createEOP(eop);
                        counter++;
                    }
                });
            }
        });

        const cancelEopCreate = document.getElementById('cancel-create-eop');
        cancelEopCreate.addEventListener("click", (e) => {
            cancelEopCreate.setAttribute('data-toggle', 'modal');
            cancelEopCreate.setAttribute('data-target', '#cancel-modal');
            cancelEopCreate.setAttribute('data-focus-on', 'input:first');
        })

        const closeEopCreate = document.getElementById('close-create-eop');
        closeEopCreate.addEventListener("click", (e) => {
            closeEopCreate.setAttribute('data-toggle', 'modal');
            closeEopCreate.setAttribute('data-target', '#cancel-modal');
            closeEopCreate.setAttribute('data-focus-on', 'input:first');
        })

        const ntSaveUpdate = document.getElementById('nt-save-update-eop');
        ntSaveUpdate.addEventListener("click", (e) => {
            ntSaveUpdate.setAttribute('data-toggle', 'modal');
            ntSaveUpdate.setAttribute('data-target', '#save-modal');
            ntSaveUpdate.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save');
            yesSaveUpdate.addEventListener("click", (e) => {
                var updateEOP = new UpdateEOP();
                updateEOP.onUpdateEvent(this.formId);
            });
        });

        const cancelEopUpdate = document.getElementById('cancel-update-eop');
        cancelEopUpdate.addEventListener("click", (e) => {
            cancelEopUpdate.setAttribute('data-toggle', 'modal');
            cancelEopUpdate.setAttribute('data-target', '#cancel-modal');
            cancelEopUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closeEopUpdate = document.getElementById('close-update-eop');
        closeEopUpdate.addEventListener("click", (e) => {
            closeEopUpdate.setAttribute('data-toggle', 'modal');
            closeEopUpdate.setAttribute('data-target', '#cancel-modal');
            closeEopUpdate.setAttribute('data-focus-on', 'input:first');
        })

        //PAT buttons
        var pat = new PATModel();
        var counterPat = 0;
        const ntSaveCreatePAT = document.getElementById('nt-save-create-pat');
        ntSaveCreatePAT.addEventListener("click", (e) => {
            if (this.validationHelper.validatePATCreateModal()) {
                ntSaveCreatePAT.setAttribute('data-toggle', 'modal');
                ntSaveCreatePAT.setAttribute('data-target', '#save-modal');
                ntSaveCreatePAT.setAttribute('data-focus-on', 'input:first');

                pat.dateOfTalk = document.getElementById('talk-date-input-pat').value;
                pat.employeeId = this.employee.id;

                this.getLineManager(this.employee.id)
                    .then((lineManager) => {
                        pat.lineManagerId = lineManager.id;

                        this.getTeamLead(this.employee.id)
                            .then((teamLead) => {
                                pat.teamLeadId = teamLead.id;
                            });
                    });

                const yesSaveCreate = document.getElementById('yes-save');
                yesSaveCreate.addEventListener("click", (e) => {
                    if (counterPat < 1) {
                        this.createPAT(pat);
                        counterPat++;
                    }
                });
            }
        });

        const cancelPatCreate = document.getElementById('cancel-create-pat');
        cancelPatCreate.addEventListener("click", (e) => {
            cancelPatCreate.setAttribute('data-toggle', 'modal');
            cancelPatCreate.setAttribute('data-target', '#cancel-modal');
            cancelPatCreate.setAttribute('data-focus-on', 'input:first');
        })

        const closePatCreate = document.getElementById('close-create-pat');
        closePatCreate.addEventListener("click", (e) => {
            closePatCreate.setAttribute('data-toggle', 'modal');
            closePatCreate.setAttribute('data-target', '#cancel-modal');
            closePatCreate.setAttribute('data-focus-on', 'input:first');
        })

        const ntSaveUpdatePAT = document.getElementById('nt-save-update-pat');
        ntSaveUpdatePAT.addEventListener("click", (e) => {
            ntSaveUpdatePAT.setAttribute('data-toggle', 'modal');
            ntSaveUpdatePAT.setAttribute('data-target', '#save-modal');
            ntSaveUpdatePAT.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save');
            yesSaveUpdate.addEventListener("click", (e) => {
                var updatePAT = new UpdatePAT();
                updatePAT.onUpdateEvent(this.formId);
            });
        });

        const cancelPatUpdate = document.getElementById('cancel-update-pat');
        cancelPatUpdate.addEventListener("click", (e) => {
            cancelPatUpdate.setAttribute('data-toggle', 'modal');
            cancelPatUpdate.setAttribute('data-target', '#cancel-modal');
            cancelPatUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closePatUpdate = document.getElementById('close-update-pat');
        closePatUpdate.addEventListener("click", (e) => {
            closePatUpdate.setAttribute('data-toggle', 'modal');
            closePatUpdate.setAttribute('data-target', '#cancel-modal');
            closePatUpdate.setAttribute('data-focus-on', 'input:first');
        })

        this.getUnresolvedForms().then((unr) => {
            const bell_notification = document.getElementById('unresolved-count');
            bell_notification.textContent = Object.keys(unr).length;

            const cta = document.getElementById('unresolved-profile');
            cta.addEventListener("click", (e) => {
                this.showUnresolved();
            })
        })

        // Ad-Hoc buttons
        var counterAdHoc = 0;
        var btnAdHocCreate = document.getElementById('ad-hoc-create');
        btnAdHocCreate.addEventListener('click', (e) => {
            if (this.validationHelper.validateAdHocCreateModal()) {
                btnAdHocCreate.setAttribute('data-toggle', 'modal');
                btnAdHocCreate.setAttribute('data-target', '#save-modal');
                btnAdHocCreate.setAttribute('data-focus-on', 'input:first');

                const yesSaveCreate = document.getElementById('yes-save');
                yesSaveCreate.addEventListener("click", (e) => {
                    if (counterAdHoc < 1) {
                        var createAdHocService = new CreateAdHocForm(this.employeeId);
                        createAdHocService.onCreateAdHocFormEvent();
                        counterAdHoc++;
                    }
                });
            }
        });

        const cancelAdHocCreate = document.getElementById('ad-hoc-close');
        cancelAdHocCreate.addEventListener("click", (e) => {
            cancelAdHocCreate.setAttribute('data-toggle', 'modal');
            cancelAdHocCreate.setAttribute('data-target', '#cancel-modal');
            cancelAdHocCreate.setAttribute('data-focus-on', 'input:first');
        })

        const closeAdHocCreate = document.getElementById('close-create-ad-hoc');
        closeAdHocCreate.addEventListener("click", (e) => {
            closeAdHocCreate.setAttribute('data-toggle', 'modal');
            closeAdHocCreate.setAttribute('data-target', '#cancel-modal');
            closeAdHocCreate.setAttribute('data-focus-on', 'input:first');
        })

        var btnAdHocUpdate = document.getElementById('ad-hoc-update');
        btnAdHocUpdate.addEventListener('click', (e) => {
            btnAdHocUpdate.setAttribute('data-toggle', 'modal');
            btnAdHocUpdate.setAttribute('data-target', '#save-modal');
            btnAdHocUpdate.setAttribute('data-focus-on', 'input:first');

            const yesSaveUpdate = document.getElementById('yes-save');
            yesSaveUpdate.addEventListener("click", (e) => {
                var updateAdHocService = new UpdateAdHocForm(this.employeeId);
                updateAdHocService.onUpdateAdHocFormEvent(this._adHocForm);
            });
        });

        const cancelAdHocUpdate = document.getElementById('cancel-update-ad-hoc');
        cancelAdHocUpdate.addEventListener("click", (e) => {
            cancelAdHocUpdate.setAttribute('data-toggle', 'modal');
            cancelAdHocUpdate.setAttribute('data-target', '#cancel-modal');
            cancelAdHocUpdate.setAttribute('data-focus-on', 'input:first');
        })

        const closeAdHocUpdate = document.getElementById('close-update-ad-hoc');
        closeAdHocUpdate.addEventListener("click", (e) => {
            closeAdHocUpdate.setAttribute('data-toggle', 'modal');
            closeAdHocUpdate.setAttribute('data-target', '#cancel-modal');
            closeAdHocUpdate.setAttribute('data-focus-on', 'input:first');
        })

        if (buttonApply = document.getElementById('applyFilter') != null) {
            var buttonApply = document.getElementById('applyFilter').addEventListener('click', () => {
                this.showEmployeeDocs();
            })
        }
        this.populateExitModal();
    }

    getEmployeeDocFilter() {
        var hasFollowUp;

        if (document.getElementById('profile-hasFollowUp-select').value == "true")
            hasFollowUp = true;
        else if (document.getElementById('profile-hasFollowUp-select').value == "false")
            hasFollowUp = false;
        else
            hasFollowUp = null;

        var filter = {
            'EmployeeId': this.employeeId,
            'Year': parseInt(document.getElementById('profile-year-select').value),
            'TalkType': parseInt(document.getElementById('profile-talktype-select').value),
            'HasFollowUp': hasFollowUp
        }
        return filter;
    }

    showUnresolved() {
        this.getUnresolvedForms()
            .then((unresolved) => {
                const fb_info_item_fb_dropdown_cta = document.getElementById('unresolved-profile');

                const dropdown_menu = document.createElement('div');
                dropdown_menu.setAttribute('class', 'fb-dropdown-menu');
                fb_info_item_fb_dropdown_cta.appendChild(dropdown_menu);

                const dropdown_menu_pointer = document.createElement('div');
                dropdown_menu_pointer.setAttribute('class', 'fb-dropdown-menu-pointer');
                dropdown_menu.appendChild(dropdown_menu_pointer);

                const title = document.createElement('h6');
                title.setAttribute('class', 'p-3 m-0');
                title.textContent = 'Opened talks reminder';
                dropdown_menu.appendChild(title);

                const wrapper = document.createElement('div');
                wrapper.setAttribute('class', 'fb-notification-wrapper');
                dropdown_menu.appendChild(wrapper);

                const app = new App();
                unresolved.forEach((form) => {

                    const employee = document.createElement('div');
                    employee.setAttribute('class', 'fb-employee');
                    wrapper.appendChild(employee);

                    const doc_item = document.createElement('div');
                    doc_item.setAttribute('class', 'fb-doc-item mb-3');
                    employee.appendChild(doc_item);

                    const link = document.createElement('a');
                    link.setAttribute('href', 'javascript:void(0)');
                    link.setAttribute('class', 'div-link');
                    link.setAttribute('data-toggle', 'modal');

                    switch (form.talkType) {
                        case 1:
                            link.setAttribute('data-target', '#document');
                            link.addEventListener("click", (e) => {
                                this.formId = form.formId;
                                api.getEopById(form.formId)
                                    .then((eop) => {
                                        var fillEopForm = new FillEOPFormComponent();
                                        fillEopForm.fillForm(eop, new App());
                                    });
                            });
                            break;
                        case 2:
                            link.setAttribute('data-target', '#documentPat');
                            link.addEventListener("click", (e) => {
                                this.formId = form.formId;
                                api.getPatById(form.formId)
                                    .then((pat) => {
                                        var fillPatForm = new FillPATFormComponent();
                                        fillPatForm.fillForm(pat, new App());
                                    });
                            });
                            break;
                        case 3:
                            link.setAttribute('data-target', '#documentAdHoc');
                            link.addEventListener('click', (e) => {
                                api.getAdHocById(form.formId)
                                    .then((adHoc) => {
                                        this._adHocForm = adHoc;
                                        var fillAdHocForm = new FillAdHocForm();
                                        fillAdHocForm.fillForm(adHoc, new App());
                                    });
                            });
                            break;
                        default:
                            break;
                    }
                    doc_item.appendChild(link);

                    const row = document.createElement('div');
                    row.setAttribute('class', 'row');
                    doc_item.appendChild(row);

                    const div1_row = document.createElement('div');
                    div1_row.setAttribute('class', 'col-12 d-flex justify-content-start align-items-center');
                    row.appendChild(div1_row);

                    const profile = document.createElement('div');
                    profile.setAttribute('class', 'fb-profile');
                    div1_row.appendChild(profile);

                    const img = document.createElement('img');
                    img.setAttribute('src', config.imageUrl + `${form.imageUrl}`);
                    img.setAttribute('alt', 'Name LastName');
                    profile.appendChild(img);

                    const div_pl2 = document.createElement('div');
                    div_pl2.setAttribute('class', 'pl-2');
                    div1_row.appendChild(div_pl2);

                    const empName = document.createElement('div');
                    empName.setAttribute('class', 'fb-employee-name');
                    empName.textContent = `${form.employeeName}`;
                    div_pl2.appendChild(empName);

                    const empTeam = document.createElement('div');
                    empTeam.setAttribute('class', 'fb-employee-team');
                    empTeam.textContent = `${form.teamName}`;
                    div_pl2.appendChild(empTeam);

                    const div2_row = document.createElement('div');
                    div2_row.setAttribute('class', 'col-12 text-right mt-2');
                    row.appendChild(div2_row);

                    const spanDate = document.createElement('span');
                    spanDate.setAttribute('class', 'badge badge-info');
                    spanDate.textContent = this.stringHelper.getShortDateFromString(form.followUpDate);
                    div2_row.appendChild(spanDate);

                    const spanType = document.createElement('span');
                    spanType.setAttribute('class', 'badge badge-secondary');
                    spanType.textContent = app.getType(form.talkType);
                    div2_row.appendChild(spanType);

                    const spanOpen = document.createElement('span');
                    spanOpen.setAttribute('class', 'badge badge-danger');
                    spanOpen.textContent = 'Follow up open';
                    div2_row.appendChild(spanOpen);
                });
            })
    }

    newForm(employeeDoc) {
        var btnAddNewEOP = document.getElementById('add-new-eop');
        var btnAddNewExit = document.getElementById('add-new-exit');

        if (employeeDoc.talkType == 1) {
            btnAddNewEOP.setAttribute('data-target', '#eop-exists');
        }
        if (employeeDoc.talkType == 4) {
            btnAddNewExit.setAttribute('data-target', '#exit-exists');
        }
    }
};

document.addEventListener('DOMContentLoaded', () => {
    const urlParams = new URLSearchParams(window.location.search);
    const employeeId = urlParams.get('id');
    window.app = new Profile(employeeId);
});
