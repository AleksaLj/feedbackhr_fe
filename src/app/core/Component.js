import { util } from '../../lib/package';


export class Component {
    constructor() {
        this._uuid = util.getUUID();
    }

    get UUID() {
        return this._uuid;
    }
}
