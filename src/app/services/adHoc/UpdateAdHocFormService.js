import { Component } from '../../core/Component';
import * as api from '../../apiJs/Index';
import getFollowUpReason from '../../utility/GetFollowUpReasonComponent';

export class UpdateAdHocForm extends Component{ 
    constructor(employeeId) { 
        super();

        this._employeeId = employeeId;
    }

    updateAdHocForm(adHocForm) { 
        api.updateAdHoc(adHocForm);
    };

    buildAdHocForm(adHocForm) {
        adHocForm.dateOfTalk = document.getElementById('ad-hoc-talk-date-update').value;
        adHocForm.reporter = document.getElementById('ad-hoc-hr-reporter-update').value;

        var followUpOptions = [
            { FollowUpOption: 1, Checked: document.getElementById('pat-ad-hoc-update').checked },
            { FollowUpOption: 2, Checked: document.getElementById('wwo-ad-hoc-update').checked },
            { FollowUpOption: 3, Checked: document.getElementById('tacttp-ad-hoc-update').checked },
            { FollowUpOption: 4, Checked: document.getElementById('rwceu-ad-hoc-update').checked },
            { FollowUpOption: 5, Checked: document.getElementById('tc-ad-hoc-update').checked },
            { FollowUpOption: 6, Checked: document.getElementById('cwtl-ad-hoc-update').checked },
            { FollowUpOption: 7, Checked: document.getElementById('cwlm-ad-hoc-update').checked },
            { FollowUpOption: 8, Checked: document.getElementById('oswcam-ad-hoc-update').checked },
            { FollowUpOption: 9, Checked: document.getElementById('icacp-ad-hoc-update').checked },
            { FollowUpOption: 10, Checked: document.getElementById('pwe-ad-hoc-update').checked },
            { FollowUpOption: 11, Checked: document.getElementById('cjs-ad-hoc-update').checked },
            { FollowUpOption: 12, Checked: document.getElementById('tba-ad-hoc-update').checked },
            { FollowUpOption: 13, Checked: document.getElementById('ofpacd-ad-hoc-update').checked },
            { FollowUpOption: 14, Checked: document.getElementById('b-ad-hoc-update').checked },
            { FollowUpOption: 15, Checked: document.getElementById('s-ad-hoc-update').checked }
        ];

        //add hr notes, talk date, hr reporter
        var followUpSection = {
            FollowUpOptions: followUpOptions,
            HRNotes: document.getElementById('ad-hoc-fup-hr-notes-update').value,
            HRReporter: document.getElementById('ad-hoc-fup-hr-reporter-update').value
            //TalkDate: document.getElementById('ad-hoc-fup-talk-date-update').value
        };
        adHocForm.followUpSection = followUpSection;

        var elementsIdDict = {
            'SalaryElId': 's-ad-hoc-update',
            'SatisfiedAfterFUPId': 'ad-hoc-satisfied-after-follow-up-update',
            'ExpectedSalaryAfterFUPId': 'ad-hoc-expected-salary-after-follow-up-update',
            'ChangedAfterFUPId': 'ad-hoc-changed-after-follow-up-update',
            'AmountAfterFUPId': 'ad-hoc-amount-after-follow-up-update'
        };
        var patAdHoc = getFollowUpReason(document.getElementById('pat-ad-hoc-update'), document.getElementById('pat-ad-hoc-shortComment-update'), document.getElementById('pat-ad-hoc-resolved-update'), elementsIdDict);
        var wwoAdHoc = getFollowUpReason(document.getElementById('wwo-ad-hoc-update'), document.getElementById('wwo-ad-hoc-shortComment-update'), document.getElementById('wwo-ad-hoc-resolved-update'), elementsIdDict);
        var tacttpAdHoc = getFollowUpReason(document.getElementById('tacttp-ad-hoc-update'), document.getElementById('tacttp-ad-hoc-shortComment-update'), document.getElementById('tacttp-ad-hoc-resolved-update'), elementsIdDict);
        var rwceuAdHoc = getFollowUpReason(document.getElementById('rwceu-ad-hoc-update'), document.getElementById('rwceu-ad-hoc-shortComment-update'), document.getElementById('rwceu-ad-hoc-resolved-update'), elementsIdDict);
        var tcAdHoc = getFollowUpReason(document.getElementById('tc-ad-hoc-update'), document.getElementById('tc-ad-hoc-shortComment-update'), document.getElementById('tc-ad-hoc-resolved-update'), elementsIdDict);
        var cwlmAdhHoc = getFollowUpReason(document.getElementById('cwlm-ad-hoc-update'), document.getElementById('cwlm-ad-hoc-shortComment-update'), document.getElementById('cwlm-ad-hoc-resolved-update'), elementsIdDict);
        var cwtlAdHoc = getFollowUpReason(document.getElementById('cwtl-ad-hoc-update'), document.getElementById('cwtl-ad-hoc-shortComment-update'), document.getElementById('cwtl-ad-hoc-resolved-update'), elementsIdDict);
        var oswcamAdHoc = getFollowUpReason(document.getElementById('oswcam-ad-hoc-update'), document.getElementById('oswcam-ad-hoc-shortComment-update'), document.getElementById('oswcam-ad-hoc-resolved-update'), elementsIdDict);
        var icacpAdHoc = getFollowUpReason(document.getElementById('icacp-ad-hoc-update'), document.getElementById('icacp-ad-hoc-shortComment-update'), document.getElementById('icacp-ad-hoc-resolved-update'), elementsIdDict);
        var pweAdHoc = getFollowUpReason(document.getElementById('pwe-ad-hoc-update'), document.getElementById('pwe-ad-hoc-shortComment-update'), document.getElementById('pwe-ad-hoc-resolved-update'), elementsIdDict);
        var cjsAdHoc = getFollowUpReason(document.getElementById('cjs-ad-hoc-update'), document.getElementById('cjs-ad-hoc-shortComment-update'), document.getElementById('cjs-ad-hoc-resolved-update'), elementsIdDict);
        var tbaAdHoc = getFollowUpReason(document.getElementById('tba-ad-hoc-update'), document.getElementById('tba-ad-hoc-shortComment-update'), document.getElementById('tba-ad-hoc-resolved-update'), elementsIdDict);
        var ofpacdAdHoc = getFollowUpReason(document.getElementById('ofpacd-ad-hoc-update'), document.getElementById('ofpacd-ad-hoc-shortComment-update'), document.getElementById('ofpacd-ad-hoc-resolved-update'), elementsIdDict);
        var bAdHoc = getFollowUpReason(document.getElementById('b-ad-hoc-update'), document.getElementById('b-ad-hoc-shortComment-update'), document.getElementById('b-ad-hoc-resolved-update'), elementsIdDict);
        var sAdHoc = getFollowUpReason(document.getElementById('s-ad-hoc-update'), document.getElementById('s-ad-hoc-shortComment-update'), document.getElementById('s-ad-hoc-resolved-update'), elementsIdDict);
        var followUpsConcrete = [
            patAdHoc,
            wwoAdHoc,
            tacttpAdHoc,
            rwceuAdHoc,
            tcAdHoc,
            cwlmAdhHoc,
            cwtlAdHoc,
            oswcamAdHoc,
            icacpAdHoc,
            pweAdHoc,
            cjsAdHoc,
            tbaAdHoc,
            ofpacdAdHoc,
            bAdHoc,
            sAdHoc];
        adHocForm.followUpsConcrete = followUpsConcrete;

        //is opened for follow up
        adHocForm.employeeId = this._employeeId;

        //salary before follow up on form
        var salaryChangeAmountBeforeAdHoc = null;
        salaryChangeAmountBeforeAdHoc = parseInt(document.getElementById('ad-hoc-amount-before-follow-up-update').value);
        var expectedSalaryChange = null;
        expectedSalaryChange = parseInt(document.getElementById('ad-hoc-expected-salary-before-follow-up-update').value);         
        
        var salary = {
            SalaryChanged: document.getElementById('ad-hoc-changed-before-follow-up-update').checked,
            ChangedFor: salaryChangeAmountBeforeAdHoc,
            IsSatisfied: document.getElementById('ad-hoc-satisfied-before-follow-up-update').checked,
            DesiredAmount: expectedSalaryChange
        };
        adHocForm.salary = salary;

        this.updateAdHocForm(adHocForm);
    }

    onUpdateAdHocFormEvent(adHocForm) { 
        this.buildAdHocForm(adHocForm);
    }
}

document.addEventListener('DOMContentLoaded', () => {
    const urlParams = new URLSearchParams(window.location.search);
    const employeeId = urlParams.get('id');
    window.app = new UpdateAdHocForm(employeeId);
});