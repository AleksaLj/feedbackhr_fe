import { Component } from '../../core/Component';
import { AdHocFormModel } from '../../models/AdHocFormModel';
import * as api from '../../apiJs/Index';
import getFollowUpReason from '../../utility/GetFollowUpReasonComponent';
import { ValidationHelper } from '../../utility/ValidationHelper';

export class CreateAdHocForm extends Component { 
    constructor(employeeId) { 
        super();

        this._employeeId = employeeId;
    }

    createAdHocForm(adHocForm) {
        var validation = new ValidationHelper();
        var isValid = validation.validateAdHocCreateModal();
        if (isValid == true) {
            api.createAdHoc(adHocForm);
        }
    }

    buildAdHocForm() { 
        var adHocForm = new AdHocFormModel();


        adHocForm.formType = 3;
        adHocForm.dateOfTalk = document.getElementById('ad-hoc-talk-date').value;
        adHocForm.reporter = document.getElementById('ad-hoc-hr-reporter').value;
        adHocForm.hrIncluded = true;
        
        var followUpOptions = [
            { FollowUpOption: 1, Checked: document.getElementById('pat-ad-hoc').checked },
            { FollowUpOption: 2, Checked: document.getElementById('wwo-ad-hoc').checked },
            { FollowUpOption: 3, Checked: document.getElementById('tacttp-ad-hoc').checked },
            { FollowUpOption: 4, Checked: document.getElementById('rwceu-ad-hoc').checked },
            { FollowUpOption: 5, Checked: document.getElementById('tc-ad-hoc').checked },
            { FollowUpOption: 6, Checked: document.getElementById('cwtl-ad-hoc').checked },
            { FollowUpOption: 7, Checked: document.getElementById('cwlm-ad-hoc').checked },
            { FollowUpOption: 8, Checked: document.getElementById('oswcam-ad-hoc').checked },
            { FollowUpOption: 9, Checked: document.getElementById('icacp-ad-hoc').checked },
            { FollowUpOption: 10, Checked: document.getElementById('pwe-ad-hoc').checked },
            { FollowUpOption: 11, Checked: document.getElementById('cjs-ad-hoc').checked },
            { FollowUpOption: 12, Checked: document.getElementById('tba-ad-hoc').checked },
            { FollowUpOption: 13, Checked: document.getElementById('ofpacd-ad-hoc').checked },
            { FollowUpOption: 14, Checked: document.getElementById('b-ad-hoc').checked },
            { FollowUpOption: 15, Checked: document.getElementById('s-ad-hoc').checked }
        ];

        var followUpSection = {
            FollowUpOptions: followUpOptions,
            HRNotes: document.getElementById('ad-hoc-fup-hr-notes').value,
            HRReporter: document.getElementById('ad-hoc-fup-hr-reporter').value
            //TalkDate: document.getElementById('ad-hoc-fup-talk-date').value
        };
        adHocForm.followUpSection = followUpSection;

        var elementsIdDict = {
            'SalaryElId': 's-ad-hoc',
            'SatisfiedAfterFUPId': 'ad-hoc-satisfied-after-follow-up',
            'ExpectedSalaryAfterFUPId': 'ad-hoc-expected-salary-after-follow-up',
            'ChangedAfterFUPId': 'ad-hoc-changed-after-follow-up',
            'AmountAfterFUPId': 'ad-hoc-amount-after-follow-up'
        };
        var patAdHoc = getFollowUpReason(document.getElementById('pat-ad-hoc'), document.getElementById('pat-ad-hoc-shortComment'), document.getElementById('pat-ad-hoc-resolved'), elementsIdDict);
        var wwoAdHoc = getFollowUpReason(document.getElementById('wwo-ad-hoc'), document.getElementById('wwo-ad-hoc-shortComment'), document.getElementById('wwo-ad-hoc-resolved'), elementsIdDict);
        var tacttpAdHoc = getFollowUpReason(document.getElementById('tacttp-ad-hoc'), document.getElementById('tacttp-ad-hoc-shortComment'), document.getElementById('tacttp-ad-hoc-resolved'), elementsIdDict);
        var rwceuAdHoc = getFollowUpReason(document.getElementById('rwceu-ad-hoc'), document.getElementById('rwceu-ad-hoc-shortComment'), document.getElementById('rwceu-ad-hoc-resolved'), elementsIdDict);
        var tcAdHoc = getFollowUpReason(document.getElementById('tc-ad-hoc'), document.getElementById('tc-ad-hoc-shortComment'), document.getElementById('tc-ad-hoc-resolved'), elementsIdDict);
        var cwlmAdhHoc = getFollowUpReason(document.getElementById('cwlm-ad-hoc'), document.getElementById('cwlm-ad-hoc-shortComment'), document.getElementById('cwlm-ad-hoc-resolved'), elementsIdDict);
        var cwtlAdHoc = getFollowUpReason(document.getElementById('cwtl-ad-hoc'), document.getElementById('cwtl-ad-hoc-shortComment'), document.getElementById('cwtl-ad-hoc-resolved'), elementsIdDict);
        var oswcamAdHoc = getFollowUpReason(document.getElementById('oswcam-ad-hoc'), document.getElementById('oswcam-ad-hoc-shortComment'), document.getElementById('oswcam-ad-hoc-resolved'), elementsIdDict);
        var icacpAdHoc = getFollowUpReason(document.getElementById('icacp-ad-hoc'), document.getElementById('icacp-ad-hoc-shortComment'), document.getElementById('icacp-ad-hoc-resolved'), elementsIdDict);
        var pweAdHoc = getFollowUpReason(document.getElementById('pwe-ad-hoc'), document.getElementById('pwe-ad-hoc-shortComment'), document.getElementById('pwe-ad-hoc-resolved'), elementsIdDict);
        var cjsAdHoc = getFollowUpReason(document.getElementById('cjs-ad-hoc'), document.getElementById('cjs-ad-hoc-shortComment'), document.getElementById('cjs-ad-hoc-resolved'), elementsIdDict);
        var tbaAdHoc = getFollowUpReason(document.getElementById('tba-ad-hoc'), document.getElementById('tba-ad-hoc-shortComment'), document.getElementById('tba-ad-hoc-resolved'), elementsIdDict);
        var ofpacdAdHoc = getFollowUpReason(document.getElementById('ofpacd-ad-hoc'), document.getElementById('ofpacd-ad-hoc-shortComment'), document.getElementById('ofpacd-ad-hoc-resolved'), elementsIdDict);
        var bAdHoc = getFollowUpReason(document.getElementById('b-ad-hoc'), document.getElementById('b-ad-hoc-shortComment'), document.getElementById('b-ad-hoc-resolved'), elementsIdDict);
        var sAdHoc = getFollowUpReason(document.getElementById('s-ad-hoc'), document.getElementById('s-ad-hoc-shortComment'), document.getElementById('s-ad-hoc-resolved'), elementsIdDict);
        var followUpsConcrete = [
            patAdHoc,
            wwoAdHoc,
            tacttpAdHoc,
            rwceuAdHoc,
            tcAdHoc,
            cwlmAdhHoc,
            cwtlAdHoc,
            oswcamAdHoc,
            icacpAdHoc,
            pweAdHoc,
            cjsAdHoc,
            tbaAdHoc,
            ofpacdAdHoc,
            bAdHoc,
            sAdHoc];
        adHocForm.followUpsConcrete = followUpsConcrete;
        adHocForm.isOpenedForFollowUp = true;

        adHocForm.employeeId = this._employeeId;
        adHocForm.createdById = this._employeeId;

        //salary before follow up on form
        var salaryChangeAmountBeforeAdHoc = null;
        if (document.getElementById('ad-hoc-changed-before-follow-up').checked) { 
            salaryChangeAmountBeforeAdHoc = parseInt(document.getElementById('ad-hoc-amount-before-follow-up').value);
        }
        var expectedSalaryChange = null;
        if (!document.getElementById('ad-hoc-satisfied-before-follow-up').checked) { 
            expectedSalaryChange = parseInt(document.getElementById('ad-hoc-expected-salary-before-follow-up').value);         
        }
        
        var salary = {
            SalaryChanged: document.getElementById('ad-hoc-changed-before-follow-up').checked,
            ChangedFor: salaryChangeAmountBeforeAdHoc,
            IsSatisfied: document.getElementById('ad-hoc-satisfied-before-follow-up').checked,
            DesiredAmount: expectedSalaryChange
        };
        adHocForm.salary = salary;
        this.createAdHocForm(adHocForm);
    }

    onCreateAdHocFormEvent() { 
        this.buildAdHocForm();
    }
}

document.addEventListener('DOMContentLoaded', () => {
});