import { Component } from '../core/Component';

export class ValidationHelper extends Component {
    constructor() {
        super();

    }
    validateEOPCreateModal( ) {
        var dataInput = document.getElementById('talk-date-input').value;
        var createButton = document.getElementById('nt-save-create');

        if (dataInput == "") {
            Notiflix.Notify.Failure('unsucessfully saved');
            createButton.removeAttribute('data-dismiss', 'modal');
            createButton.removeAttribute('aria-label', 'Close');
            return false;
        } else {
            return true;
        }
    }

    validatePATCreateModal() {
        var dataInput = document.getElementById('talk-date-input-pat').value;
        var createButton = document.getElementById('nt-save-create-pat');

        if (dataInput == "") {
            Notiflix.Notify.Failure('unsucessfully saved');
            createButton.removeAttribute('data-dismiss', 'modal');
            createButton.removeAttribute('aria-label', 'Close');
            return false;
        } else {        
            return true;
        }
    }

    validateExitCreateModal() {
        var dataInput = document.getElementById('create-date-exit').value;
        var hrNotesInput = document.getElementById('hr-notes-exit').value;
        var fileUpload = document.getElementById('file-upload-exit').value;
        var createButton = document.getElementById('save-exit-create');

        if (dataInput == "" || hrNotesInput == "" || fileUpload=="") {
            Notiflix.Notify.Failure('unsucessfully saved');
            createButton.removeAttribute('data-dismiss', 'modal');
            createButton.removeAttribute('aria-label', 'Close');
            return false;
        } else {
            return true;
        }
    }

    validateAdHocCreateModal() {
        var options = this.getAdHocFormOptions();
        var isChecked = false;
        for (var i = 0; i < options.length; i++){
            if (options[i].Checked == true) {
                isChecked = true;
            }
        }
        var dataInput = document.getElementById('ad-hoc-talk-date').value;
        var createButton = document.getElementById('ad-hoc-create');

        if (dataInput == "" ) {
            Notiflix.Notify.Failure('unsucessfully saved');
            createButton.removeAttribute('data-dismiss', 'modal');
            createButton.removeAttribute('aria-label', 'Close');
            return false;
        }
        else if (isChecked == false) {
            Notiflix.Notify.Failure('Unsucessfully saved. Please, check at least one reason!')
            return false;
        }
        else {
            return true;
        }
    }
    
    getAdHocFormOptions() {
        var followUpOptions = [
            { FollowUpOption: 1, Checked: document.getElementById('pat-ad-hoc').checked },
            { FollowUpOption: 2, Checked: document.getElementById('wwo-ad-hoc').checked },
            { FollowUpOption: 3, Checked: document.getElementById('tacttp-ad-hoc').checked },
            { FollowUpOption: 4, Checked: document.getElementById('rwceu-ad-hoc').checked },
            { FollowUpOption: 5, Checked: document.getElementById('tc-ad-hoc').checked },
            { FollowUpOption: 6, Checked: document.getElementById('cwtl-ad-hoc').checked },
            { FollowUpOption: 7, Checked: document.getElementById('cwlm-ad-hoc').checked },
            { FollowUpOption: 8, Checked: document.getElementById('oswcam-ad-hoc').checked },
            { FollowUpOption: 9, Checked: document.getElementById('icacp-ad-hoc').checked },
            { FollowUpOption: 10, Checked: document.getElementById('pwe-ad-hoc').checked },
            { FollowUpOption: 11, Checked: document.getElementById('cjs-ad-hoc').checked },
            { FollowUpOption: 12, Checked: document.getElementById('tba-ad-hoc').checked },
            { FollowUpOption: 13, Checked: document.getElementById('ofpacd-ad-hoc').checked },
            { FollowUpOption: 14, Checked: document.getElementById('b-ad-hoc').checked },
            { FollowUpOption: 15, Checked: document.getElementById('s-ad-hoc').checked }
        ];
        return followUpOptions;
    }
}