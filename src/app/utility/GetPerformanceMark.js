 const  getPerformanceFactorEnum = (Enum)=> {
    var result = ' ';
    switch (Enum) {
        case 1:
            result = 'NE';
            break;
        case 2:
            result = 'U';
            break;
        case 3:
            result = 'IN';
            break;
        case 4:
            result = 'MR';
            break;
        case 5:
            result = 'AR';    
            break;
    }
    return result;
}

export default getPerformanceFactorEnum;