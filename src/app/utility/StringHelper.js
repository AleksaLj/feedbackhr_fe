import { Component } from '../core/Component';

export class StringHelper extends Component {
    constructor() { 
        super();
    }

    getShortDateFromString(stringDate)
    {
        var date = stringDate.split("T")[0];

        var day = date.split("-")[2];
        var month = date.split("-")[1];
        var year = date.split("-")[0];

        return `${day}.${month}.${year}.`.replace('"', '');
    }
}
