//uskladiti za sve follow up sekcije

const getFollowUpReason = (followUpElement, followUpComment, followUpResolved, elementsIdDict) => {
    var obj = {
        Resolved: followUpResolved.checked,
        ShortComment: null,
        SalaryAfter: null
    };
    
    if (followUpElement.checked) {
        var salary = null;
        if (followUpElement.id == elementsIdDict.SalaryElId) {
            var desiredAmount = null;
            //satisfied after follow up          
            desiredAmount = parseInt(document.getElementById(elementsIdDict.ExpectedSalaryAfterFUPId).value);

            var changedFor = null;
            //changed-after-follow-up
            changedFor = parseInt(document.getElementById(elementsIdDict.AmountAfterFUPId).value);
                
            salary = {
                //satisfied-after-follow-up
                IsSatisfied: document.getElementById(elementsIdDict.SatisfiedAfterFUPId).checked,
                //changed-after-follow-up
                SalaryChanged : document.getElementById(elementsIdDict.ChangedAfterFUPId).checked,
                ChangedFor : changedFor,
                DesiredAmount : desiredAmount                
            }
        }
        
        obj.Resolved = followUpResolved.checked;
        obj.ShortComment = followUpComment.value;
        obj.SalaryAfter = salary;
        
        return obj;   
    }
    return obj;
};

export default getFollowUpReason;