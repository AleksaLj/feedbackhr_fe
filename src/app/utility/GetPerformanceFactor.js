const  getPerformanceFactorMark = (Enum)=> {
    var result = ' ';
    switch (Enum) {
        case 1:
            result = 'not evaluated';
            break;
        case 2:
            result = 'unsatisfactory results';
            break;
        case 3:
            result = 'improvement needed';
            break;
        case 4:
            result = 'meets requirements';
            break;
        case 5:
            result = 'above requirements';    
            break;
    }
    return result;
}

export default getPerformanceFactorMark;