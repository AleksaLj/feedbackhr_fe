import { Component } from './core/Component';
import config from './config';
import { PATModel } from './models/PATModel';
// import  getPerformanceFactorEnum  from './utility/GetPerformanceMark';

export class UpdatePAT extends Component {
    constructor(employeeId) {
        super();
        this.employeeId = employeeId;
    }

    getPerformanceFactorMark(mark) {
        var result = 0;
        switch (mark) {
            case 'NE':
                result = 1;
                break;
            case 'U':
                result = 2;
                break;
            case 'IN':
                result = 3;
                break;
            case 'MR':
                result = 4;
                break;
            case 'AR':
                result = 5;    
                break;
        }
        return result;
    }

    getCheckedMark(elementName) {
        var elements = document.getElementsByName(elementName);
        var mark = 0;
        for (var j = 0; j < elements.length; j++){
            if (elements[j].checked) {
                var elementValue = elements[j].value;
                mark = this.getPerformanceFactorMark(elementValue);
            } 
        }
        return mark;
    }
    
    getCheckedFollowUpReason(followUpElement, followUpComment, followUpResolved) {
      var  obj = {
            Resolved: followUpResolved.checked,
            ShortComment: null,
            SalaryAfter: null
        }
        if (followUpElement.checked) {
            var salary = null;
            if (followUpElement.id == 's-pat') {
                salary = {
                    IsSatisfied : document.getElementById('satisfied-after-follow-up-pat').checked,
                    SalaryChanged : document.getElementById('changed-after-follow-up-pat').checked,
                    ChangedFor : document.getElementById('amount-after-follow-up-pat').value,
                    DesiredAmount : document.getElementById('satisfied-amount-after-follow-up-pat').value                   
                }
            }
            obj = {
                Resolved: followUpResolved.checked,
                ShortComment: followUpComment.value,
                SalaryAfter: salary
            }
            return obj;   
        }
        return obj;
    }

    getPatById(PatId) {
        const URL = config.url + config.getPatById + `${PatId}`;
        return fetch(URL, {
                headers: {
                    'Access-Control-Allow-Origin': '*'
                }
            })
                .then((response) => response.text())
                .then((data) => {
                    const PAT = JSON.parse(data);
                    return Promise.resolve(PAT);
                })
            .catch((error) => {
                console.log(error);
                })
    }
    
    updatePAT(PAT) {
        const URL = config.url + config.updatePAT;
        fetch(URL, {
            method: 'POST',
            body: JSON.stringify(PAT),
            headers: {
                'Access-Control-Allow-Origin': '*',
                'Accept': 'application/json, application/xml, text/plain, text/html, *.*',
                'Content-Type': 'application/json'
            }
        })
            .then((response) => {
                response.text();
                if (response.status == 200)
                    Notiflix.Notify.Success('sucessfully saved');
                else
                    Notiflix.Notify.Failure('unsucessfully saved');
             })
            .catch((error) => {
                console.log(error);
            })        
    }

    onUpdateEvent(formId) {
        this.getPatById(formId)            
            .then((PAT) => {
                var pat = PAT;
                pat.dateOfTalk = document.getElementById('talk-date-input-update-pat').value;
                pat.hrReporter = document.getElementById('hr-reporter-name-pat').value;
                
                //Line Manager PAT Marks
                var qLmPATMark = this.getCheckedMark('qualityLmPAT');
                var pLmPATMark = this.getCheckedMark('productivityLmPAT');
                var pkLmPATMark = this.getCheckedMark('professional-knowledgeLmPAT');
                var rLmPATMark = this.getCheckedMark('reliabilityLmPAT');
                var cLmPATMark = this.getCheckedMark('communicationLmPAT');
                var tcLmPATMark = this.getCheckedMark('teamworkcooperationLmPAT');
                var aLmPATMark = this.getCheckedMark('autonomyLmPAT');
                var psLmPATMark = this.getCheckedMark('problemsolvingLmPAT');

                //Line Manager PATConcrete
                pat.lineManagerPAT = {
                    Quality: { Mark: qLmPATMark, Remark: document.getElementById('RemarksQualityLmPAT').value },
                    Productivity: { Mark: pLmPATMark, Remark: document.getElementById('RemarksProductivityLmPAT').value },
                    ProfessionalKnowledge: { Mark: pkLmPATMark, Remark: document.getElementById('RemarksProfessionalKnowledgeLmPAT').value },
                    Reliability: { Mark: rLmPATMark, Remark: document.getElementById('RemarksReliabilityLmPAT').value },
                    Communication: { Mark: cLmPATMark, Remark: document.getElementById('RemarksCommunicationLmPAT').value },
                    TeamWorkAndCooperation: { Mark: tcLmPATMark, Remark: document.getElementById('RemarksTeamWorkAndCooperationLmPAT').value },
                    Autonomy: { Mark: aLmPATMark, Remark: document.getElementById('RemarksAutonomyLmPAT').value },
                    ProblemSolving: { Mark: psLmPATMark, Remark: document.getElementById('RemarksProblemSolvingLmPAT').value },
                    OverallImpression: document.getElementById('OverallImpresionLmPAT').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsLmPAT').value,
                    PerformanceGoals: document.getElementById('PerformanceGoalsLmPAT').value,
                    CareerGoals: document.getElementById('CareerGoalsLmPAT').value,
                    Education: document.getElementById('EducationLmPAT').value,
                    Other: document.getElementById('OtherLmPAT').value
                }
                //Team Lead PAT Marks
                var qTlPATMark = this.getCheckedMark('qualityTlPAT');
                var pTlPATMark = this.getCheckedMark('productivityTlPAT');
                var pkTlPATMark = this.getCheckedMark('professional-knowledgeTlPAT');
                var rTlPATMark = this.getCheckedMark('reliabilityTlPAT');
                var cTlPATMark = this.getCheckedMark('communicationTlPAT');
                var tcTlPATMark = this.getCheckedMark('teamworkcooperationTlPAT');
                var aTlPATMark = this.getCheckedMark('autonomyTlPAT');
                var psTlPATMark = this.getCheckedMark('problemsolvingTlPAT');
 
                //Team Lead PATConcrete
                pat.teamLeadPAT = {
                    Quality: { Mark: qTlPATMark, Remark: document.getElementById('RemarksQualityTlPAT').value },
                    Productivity: { Mark: pTlPATMark, Remark: document.getElementById('RemarksProductivityTlPAT').value },
                    ProfessionalKnowledge: { Mark: pkTlPATMark, Remark: document.getElementById('RemarksProfessionalKnowledgeTlPAT').value },
                    Reliability: { Mark: rTlPATMark, Remark: document.getElementById('RemarksReliabilityTlPAT').value },
                    Communication: { Mark: cTlPATMark, Remark: document.getElementById('RemarksCommunicationTlPAT').value },
                    TeamWorkAndCooperation: { Mark: tcTlPATMark, Remark: document.getElementById('RemarksTeamWorkAndCooperationTlPAT').value },
                    Autonomy: { Mark: aTlPATMark, Remark: document.getElementById('RemarksAutonomyTlPAT').value },
                    ProblemSolving: { Mark: psTlPATMark, Remark: document.getElementById('RemarksProblemSolvingTlPAT').value },
                    OverallImpression: document.getElementById('OverallImpresionTlPAT').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsTlPAT').value,
                    PerformanceGoals: document.getElementById('PerformanceGoalsTlPAT').value,
                    CareerGoals: document.getElementById('CareerGoalsTlPAT').value,
                    Education: document.getElementById('EducationTlPAT').value,
                    Other: document.getElementById('OtherTlPAT').value
                }
                //Client PAT Marks
                var qClPATMark = this.getCheckedMark('qualityClPAT');
                var pClPATMark = this.getCheckedMark('productivityClPAT');
                var pkClPATMark = this.getCheckedMark('professional-knowledgeClPAT');
                var rClPATMark = this.getCheckedMark('reliabilityClPAT');
                var cClPATMark = this.getCheckedMark('communicationClPAT');
                var tcClPATMark = this.getCheckedMark('teamworkcooperationClPAT');
                var aClPATMark = this.getCheckedMark('autonomyClPAT');
                var psClPATMark = this.getCheckedMark('problemsolvingClPAT');
             
                //Client PATConcrete
                pat.clientPAT = {
                    Quality: { Mark: qClPATMark, Remark: document.getElementById('RemarksQualityClPAT').value },
                    Productivity: { Mark: pClPATMark, Remark: document.getElementById('RemarksProductivityClPAT').value },
                    ProfessionalKnowledge: { Mark: pkClPATMark, Remark: document.getElementById('RemarksProfessionalKnowledgeClPAT').value },
                    Reliability: { Mark: rClPATMark, Remark: document.getElementById('RemarksReliabilityClPAT').value },
                    Communication: { Mark: cClPATMark, Remark: document.getElementById('RemarksCommunicationClPAT').value },
                    TeamWorkAndCooperation: { Mark: tcClPATMark, Remark: document.getElementById('RemarksTeamWorkAndCooperationClPAT').value },
                    Autonomy: { Mark: aClPATMark, Remark: document.getElementById('RemarksAutonomyClPAT').value },
                    ProblemSolving: { Mark: psClPATMark, Remark: document.getElementById('RemarksProblemSolvingClPAT').value },
                    OverallImpression: document.getElementById('OverallImpresionClPAT').value,
                    ApprasierComment: document.getElementById('AppraisersCommentsClPAT').value,
                    PerformanceGoals: document.getElementById('PerformanceGoalsClPAT').value,
                    CareerGoals: document.getElementById('CareerGoalsClPAT').value,
                    Education: document.getElementById('EducationClPAT').value,
                    Other: document.getElementById('OtherClPAT').value
                } 
                pat.client = document.getElementById('client-appraiser-pat').value;
                /////////////////
                var desiredAmount = null;
                if (document.getElementById('satisfied-pat').checked == false) {
                    desiredAmount = parseInt(document.getElementById('expected-pat').value);
                }

                var changedForValue = null;
                if (document.getElementById('salary-change-pat').checked) {
                    changedForValue = parseInt(document.getElementById('salary-change-amount-pat').value);
                }

                pat.salary = {
                    IsSatisfied: document.getElementById('satisfied-pat').checked,
                    SalaryChanged: document.getElementById('salary-change-pat').checked,                    
                    ChangedFor: changedForValue,
                    DesiredAmount: desiredAmount
                };
                pat.isOpenedForFollowUp = document.getElementById('follow-up-pat').checked;

                var patPat = this.getCheckedFollowUpReason(document.getElementById('pat-pat'), document.getElementById('pat-pat-shortComment'), document.getElementById('pat-pat-resolved'));
                var wwoPat = this.getCheckedFollowUpReason(document.getElementById('wwo-pat'), document.getElementById('wwo-pat-shortComment'), document.getElementById('wwo-pat-resolved'));
                var tacttpPat = this.getCheckedFollowUpReason(document.getElementById('tacttp-pat'), document.getElementById('tacttp-pat-shortComment'), document.getElementById('tacttp-pat-resolved'));
                var rwceuPat = this.getCheckedFollowUpReason(document.getElementById('rwceu-pat'), document.getElementById('rwceu-pat-shortComment'), document.getElementById('rwceu-pat-resolved'));
                var tcPat = this.getCheckedFollowUpReason(document.getElementById('tc-pat'), document.getElementById('tc-pat-shortComment'), document.getElementById('tc-pat-resolved'));
                var cwlmPat = this.getCheckedFollowUpReason(document.getElementById('cwlm-pat'), document.getElementById('cwlm-pat-shortComment'), document.getElementById('cwlm-pat-resolved'));
                var cwtlPat = this.getCheckedFollowUpReason(document.getElementById('cwtl-pat'), document.getElementById('cwtl-pat-shortComment'), document.getElementById('cwtl-pat-resolved'));
                var oswcamPat = this.getCheckedFollowUpReason(document.getElementById('oswcam-pat'), document.getElementById('oswcam-pat-shortComment'), document.getElementById('oswcam-pat-resolved'));
                var icacpPat = this.getCheckedFollowUpReason(document.getElementById('icacp-pat'), document.getElementById('icacp-pat-shortComment'), document.getElementById('icacp-pat-resolved'));
                var pwePat = this.getCheckedFollowUpReason(document.getElementById('pwe-pat'), document.getElementById('pwe-pat-shortComment'), document.getElementById('pwe-pat-resolved'));
                var cjsPat = this.getCheckedFollowUpReason(document.getElementById('cjs-pat'), document.getElementById('cjs-pat-shortComment'), document.getElementById('cjs-pat-resolved'));
                var tbaPat = this.getCheckedFollowUpReason(document.getElementById('tba-pat'), document.getElementById('tba-pat-shortComment'), document.getElementById('tba-pat-resolved'));
                var ofpacdPat = this.getCheckedFollowUpReason(document.getElementById('ofpacd-pat'), document.getElementById('ofpacd-pat-shortComment'), document.getElementById('ofpacd-pat-resolved'));
                var bPat = this.getCheckedFollowUpReason(document.getElementById('b-pat'), document.getElementById('b-pat-shortComment'), document.getElementById('b-pat-resolved'));
                var sPat = this.getCheckedFollowUpReason(document.getElementById('s-pat'), document.getElementById('s-pat-shortComment'), document.getElementById('s-pat-resolved'));
                var followUpsToDo = [
                    patPat,
                    wwoPat,
                    tacttpPat,
                    rwceuPat,
                    tcPat,
                    cwlmPat,
                    cwtlPat,
                    oswcamPat,
                    icacpPat,
                    pwePat,
                    cjsPat,
                    tbaPat,
                    ofpacdPat,
                    bPat,
                    sPat];
                pat.followUpsToDo = followUpsToDo;
                
                var followUpOptions = [
                    { FollowUpOption: 1, Checked: document.getElementById('pat-pat').checked },
                    { FollowUpOption: 2, Checked: document.getElementById('wwo-pat').checked },
                    { FollowUpOption: 3, Checked: document.getElementById('tacttp-pat').checked },
                    { FollowUpOption: 4, Checked: document.getElementById('rwceu-pat').checked },
                    { FollowUpOption: 5, Checked: document.getElementById('tc-pat').checked },
                    { FollowUpOption: 6, Checked: document.getElementById('cwtl-pat').checked },
                    { FollowUpOption: 7, Checked: document.getElementById('cwlm-pat').checked },
                    { FollowUpOption: 8, Checked: document.getElementById('oswcam-pat').checked },
                    { FollowUpOption: 9, Checked: document.getElementById('icacp-pat').checked },
                    { FollowUpOption: 10, Checked: document.getElementById('pwe-pat').checked },
                    { FollowUpOption: 11, Checked: document.getElementById('cjs-pat').checked },
                    { FollowUpOption: 12, Checked: document.getElementById('tba-pat').checked },
                    { FollowUpOption: 13, Checked: document.getElementById('ofpacd-pat').checked },
                    { FollowUpOption: 14, Checked: document.getElementById('b-pat').checked },
                    { FollowUpOption: 15, Checked: document.getElementById('s-pat').checked }
                ];
                
                pat.followUpSection.followUpOptions = followUpOptions; 
                pat.followUpSection.hRNotes = document.getElementById('pat-hr-notes').value;

                if (document.getElementById('follow-up-pat').checked) {
                    pat.followUpSection.talkDate = document.getElementById('pat-talk-date').value;    
                    pat.followUpSection.hrReporter = document.getElementById('follow-up-hr-reporter-pat').value;
                } 
                
                var desiredAmountAfter = null;
                if (document.getElementById('satisfied-after-follow-up-pat').checked == false) {
                    desiredAmountAfter = parseInt(document.getElementById('satisfied-amount-after-follow-up-pat').value);
                }

                var changedForValueAfter = null;
                if (document.getElementById('changed-after-follow-up-pat').checked) {
                    changedForValueAfter = parseInt(document.getElementById('amount-after-follow-up-pat').value);
                }
                pat.followUpsToDo[14].SalaryAfter = {                 
                    IsSatisfied: document.getElementById('satisfied-after-follow-up-pat').checked,
                    SalaryChanged: document.getElementById('changed-after-follow-up-pat').checked,
                    ChangedFor: changedForValueAfter,
                    DesiredAmount: desiredAmountAfter                    
                };
                this.updatePAT(pat);
            });
    };
}

document.addEventListener('DOMContentLoaded', () => {
    const urlParams = new URLSearchParams(window.location.search);
    const employeeId = urlParams.get('id');
    window.app = new UpdatePAT(employeeId);
});