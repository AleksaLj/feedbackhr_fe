import config from '../config';

export const getExitById = (ExitId) => {
    const URL = config.url + config.getExitById + `${ExitId}`;
    
    return fetch(URL, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then((response) => response.text())
        .then((data) => {
            const exit = JSON.parse(data);
            return Promise.resolve(exit);
            
        })
        .catch((error) => {
            console.log(error);
        })
}