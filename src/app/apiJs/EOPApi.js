import config from '../config';

export const getEopById = (EopId) => {
        const URL = config.url + config.getEopById + `${EopId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const EOP = JSON.parse(data);
                return Promise.resolve(EOP);
                
            })
            .catch((error) => {
                console.log(error);
            })
}