import config from '../config';

export const getAdHocById = (adHocId) => {
    const URL = config.url + config.getAdHocById + `${adHocId}`;

    return fetch(URL, {
        headers: {
            'Access-Control-Allow-Origin': '*'
        }
    })
        .then(response => response.text())
        .then(data => { 
            const adHoc = JSON.parse(data);
            return Promise.resolve(adHoc);
        })
        .catch(error => { 
            console.log(error);
        });
};

export const createAdHoc = (adHocForm) => { 
    //prepare data
        //...add isOpenedForFollowUp
        var data = {
            FormType: adHocForm.formType,
            CreatedDate: adHocForm.createdDate,
            DateOfTalk: adHocForm.dateOfTalk,
            Reporter: adHocForm.reporter,
            HRIncluded: adHocForm.hrIncluded,
            HRNotes: adHocForm.hrNotes,
            FollowUpSection: adHocForm.followUpSection,
            FollowUpsConcrete: adHocForm.followUpsConcrete,
            IsOpenedForFollowUp: adHocForm.isOpenedForFollowUp,
            EmployeeId: adHocForm.employeeId,
            CreatedById: adHocForm.createdById,
            Salary: adHocForm.salary
        };
    
        //ajax call to api create ad hoc
        const URL = config.url + config.createAdHoc;

        //create XMLHttpRequest
        var xhttp = new XMLHttpRequest();

        //configure http method and header
        xhttp.open("POST", URL, true);
        xhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
        xhttp.setRequestHeader('Accept', 'application/json, application/xml, text/plain, text/html, *.*');
        xhttp.setRequestHeader('Content-Type', 'application/json');
        
        //send request
        xhttp.send(JSON.stringify(data));

        //after receiving response
        // xhttp.onload = () => { 
        //     console.log(xhttp.responseText);
        //     //window.location.reload();
        // }
        Notiflix.Notify.Success('sucessfully saved');
        xhttp.onerror = () => { 
            console.log(xhttp.statusText());
        }
};

export const updateAdHoc = (adHocForm) => {
    var data = {
        Id: adHocForm.id,
        FormType: adHocForm.formType,
        CreatedDate: adHocForm.createdDate,
        DateOfTalk: adHocForm.dateOfTalk,
        Reporter: adHocForm.reporter,
        HRIncluded: adHocForm.hrIncluded,
        HRNotes: adHocForm.hrNotes,
        FollowUpSection: adHocForm.followUpSection,
        FollowUpsConcrete: adHocForm.followUpsConcrete,
        EmployeeId: adHocForm.employeeId,
        CreatedById: adHocForm.createdById,
        Salary: adHocForm.salary
    };

    const URL = config.url + config.updateAdHoc;

    var xhttp = new XMLHttpRequest();

    xhttp.open('POST', URL, true);
    xhttp.setRequestHeader('Access-Control-Allow-Origin', '*');
    xhttp.setRequestHeader('Accept', 'application/json, application/xml, text/plain, text/html, *.*');
    xhttp.setRequestHeader('Content-Type', 'application/json');

    xhttp.send(JSON.stringify(data));
    
    Notiflix.Notify.Success('sucessfully saved');
    xhttp.onload = () => { 
        console.log(xhttp.responseText);
    //     window.location.reload();
    };

    xhttp.onerror = () => { 
        console.log(xhttp.statusText);
    };
};