import config from '../config';

export const getPatById = (PatId) => {
        const URL = config.url + config.getPatById + `${PatId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const PAT = JSON.parse(data);
                return Promise.resolve(PAT);
                
            })
            .catch((error) => {
                console.log(error);
            })
}