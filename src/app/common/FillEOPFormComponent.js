import { Component } from '../core/Component';
import getPerformanceFactorEnum from '../utility/GetPerformanceMark';
import { Profile } from '../Profile';
import { App } from '../App';
import config from '../config';
import { doc } from 'prettier';

export class FillEOPFormComponent extends Component { 
    constructor() { 
        super();
    }

    checkElement(elementName, mark) {
        var markEnum=getPerformanceFactorEnum(mark);
        var elements = document.getElementsByName(elementName);
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].value == markEnum) {               
                elements[i].checked= true;
            }
        }
    }

    fillSalaryPart(eop) { 
        document.getElementById('satisfied').checked = eop.salary.isSatisfied;
        document.getElementById('salary-change').checked = eop.salary.salaryChanged;        
        document.getElementById('salary-change-amount').value = eop.salary.changedFor;
        document.getElementById('expected').value = eop.salary.desiredAmount;

        document.getElementById('satisfied-after-follow-up').checked = eop.followUpsToDo[14].salaryAfter.isSatisfied;
        document.getElementById('changed-after-follow-up').checked = eop.followUpsToDo[14].salaryAfter.salaryChanged;
        document.getElementById('amount-after-follow-up').value = eop.followUpsToDo[14].salaryAfter.changedFor;
        document.getElementById('satisfied-amount-after-follow-up').value = eop.followUpsToDo[14].salaryAfter.desiredAmount;
        
        if (eop.salary.salaryChanged) {
            document.getElementById('salary-change-amount').removeAttribute('disabled');
        }
        if (eop.salary.isSatisfied == false) {
            document.getElementById('expected').removeAttribute('disabled');
            document.getElementById('s-eop').setAttribute('disabled', true);
            document.getElementById('s-eop').setAttribute('checked', true);
            document.getElementById('follow-up').setAttribute('checked', true);
            document.getElementById('follow-up-reasons').setAttribute('style', 'display: block');
            document.getElementById('follow-up-form').setAttribute('style', 'display: block');
            document.getElementById('s-topic').setAttribute('style', 'display: block');
            document.getElementById('salary-after-eop').setAttribute('style', 'display: block');
            if (document.getElementById('satisfied-after-follow-up').checked) {
                document.getElementById('satisfied-amount-after-follow-up').setAttribute('disabled', true);
            }
            else {
                document.getElementById('satisfied-amount-after-follow-up').removeAttribute('disabled');
            }
            if (document.getElementById('changed-after-follow-up').checked) {
                document.getElementById('amount-after-follow-up').removeAttribute('disabled');
            }
        }
        else { 
            document.getElementById('expected').setAttribute('disabled', true);
            document.getElementById('s-eop').setAttribute('disabled', true);
            document.getElementById('s-eop').setAttribute('checked', false);
            document.getElementById('s-topic').setAttribute('style', 'display: none');
            document.getElementById('salary-after-eop').setAttribute('style', 'display: none');
            document.getElementById('changed-after-follow-up').setAttribute('checked', false);
            document.getElementById('amount-after-follow-up').setAttribute('disabled', true);
            document.getElementById('expected').value = '';
            document.getElementById('amount-after-follow-up').value = '';
            document.getElementById('satisfied-amount-after-follow-up').value = '';
            document.getElementById('s-eop-shortComment').value = '';
        }
    }

    fillEOPFollowUP(eop) {
        document.getElementById('follow-up').checked = eop.isOpenedForFollowUp;
        document.getElementById('follow-up-hr-reporter').value = eop.followUpSection.hrReporter;
        document.getElementById('eop-hr-notes').value = eop.followUpSection.hrNotes;
        const date = Date.parse(eop.followUpSection.talkDate);  
        var dateObj = new Date(date);     
        dateObj.setMinutes(dateObj.getMinutes() - dateObj.getTimezoneOffset());
        document.getElementById('eop-talk-date').value = dateObj.toISOString().slice(0, 16);

        if (eop.isOpenedForFollowUp) {
            document.getElementById('follow-up').setAttribute('checked', 'true');
            document.getElementById('follow-up-form').setAttribute('style', 'display: block');
            document.getElementById('follow-up-reasons').setAttribute('style', 'display: block');        
        }
    }

    fillEOPReasons(eop, resolvedId, commentId, topicId, arrayNumber) {
        document.getElementById(resolvedId).checked = eop.followUpsToDo[arrayNumber].resolved;
        document.getElementById(commentId).value = eop.followUpsToDo[arrayNumber].shortComment;

        if (eop.followUpSection.followUpOptions[arrayNumber].checked)
            document.getElementById(topicId).setAttribute('style', 'display: block');
        
        if (eop.followUpSection.followUpOptions[14].checked) {
            document.getElementById('salary-after-eop').setAttribute('style', 'display: block');
        }
        else {
            document.getElementById('salary-after-eop').setAttribute('style', 'display: none');
        }
        document.getElementById('s-eop').setAttribute('disabled', true);
    }
    
    fillEopSection(eop) {
        document.getElementById('pat-eop').checked = eop.followUpSection.followUpOptions[0].checked;
        document.getElementById('wwo-eop').checked = eop.followUpSection.followUpOptions[1].checked;
        document.getElementById('tacttp-eop').checked = eop.followUpSection.followUpOptions[2].checked;
        document.getElementById('rwceu-eop').checked = eop.followUpSection.followUpOptions[3].checked;
        document.getElementById('tc-eop').checked = eop.followUpSection.followUpOptions[4].checked;
        document.getElementById('cwtl-eop').checked = eop.followUpSection.followUpOptions[5].checked;
        document.getElementById('cwlm-eop').checked = eop.followUpSection.followUpOptions[6].checked;
        document.getElementById('oswcam-eop').checked = eop.followUpSection.followUpOptions[7].checked;
        document.getElementById('icacp-eop').checked = eop.followUpSection.followUpOptions[8].checked;
        document.getElementById('pwe-eop').checked = eop.followUpSection.followUpOptions[9].checked;
        document.getElementById('cjs-eop').checked = eop.followUpSection.followUpOptions[10].checked;
        document.getElementById('tba-eop').checked = eop.followUpSection.followUpOptions[11].checked;
        document.getElementById('ofpacd-eop').checked = eop.followUpSection.followUpOptions[12].checked;
        document.getElementById('b-eop').checked = eop.followUpSection.followUpOptions[13].checked;
        document.getElementById('s-eop').checked = eop.followUpSection.followUpOptions[14].checked;
    }
  
    fillClPartOfEOP(eop) { 
        document.getElementById('client-appraiser').value = eop.client;

        document.getElementById('RemarksQualityClEOP').value = eop.clientEOP.quality.remark;
        document.getElementById('RemarksProductivityClEOP').value = eop.clientEOP.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeClEOP').value = eop.clientEOP.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityClEOP').value = eop.clientEOP.reliability.remark;
        document.getElementById('RemarksCommunicationClEOP').value = eop.clientEOP.communication.remark;
        document.getElementById('RemarksTeamworkCooperationClEOP').value = eop.clientEOP.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyClEOP').value = eop.clientEOP.autonomy.remark;
        document.getElementById('OverallImpressionClEOP').value = eop.clientEOP.overallImpression;
        document.getElementById('AppraisersCommentsClEOP').value = eop.clientEOP.apprasierComment; 
        
        this.checkElement('qualityClEop', eop.clientEOP.quality.mark);
        this.checkElement('productivityClEop', eop.clientEOP.productivity.mark);
        this.checkElement('professional-knowledgeClEop', eop.clientEOP.professionalKnowledge.mark);
        this.checkElement('reliabilityClEop', eop.clientEOP.reliability.mark);
        this.checkElement('communicationClEop', eop.clientEOP.communication.mark);
        this.checkElement('teamwork-cooperationClEop', eop.clientEOP.teamWorkAndCooperation.mark);
        this.checkElement('autonomyClEop', eop.clientEOP.autonomy.mark);
    }

    fillTlPartOfEOP(eop) { 
        var profile = new Profile();
        profile.getTeamLead(eop.employeeId)
        .then((teamLead) => {
            document.getElementById('tl-reporter-name-eop').value = `${teamLead.firstName} ${teamLead.lastName}`;
        });
        document.getElementById('RemarksQualityTlEOP').value = eop.teamLeadEOP.quality.remark;
        document.getElementById('RemarksProductivityTlEOP').value = eop.teamLeadEOP.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeTlEOP').value = eop.teamLeadEOP.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityTlEOP').value = eop.teamLeadEOP.reliability.remark;
        document.getElementById('RemarksCommunicationTlEOP').value = eop.teamLeadEOP.communication.remark;
        document.getElementById('RemarksTeamworkCooperationTlEOP').value = eop.teamLeadEOP.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyTlEOP').value = eop.teamLeadEOP.autonomy.remark;
        document.getElementById('OverallImpressionTlEOP').value = eop.teamLeadEOP.overallImpression;
        document.getElementById('AppraisersCommentsTlEOP').value = eop.teamLeadEOP.apprasierComment;

        this.checkElement('qualityTlEop', eop.teamLeadEOP.quality.mark);
        this.checkElement('productivityTlEop', eop.teamLeadEOP.productivity.mark);
        this.checkElement('professional-knowledgeTlEop', eop.teamLeadEOP.professionalKnowledge.mark);
        this.checkElement('reliabilityTlEop', eop.teamLeadEOP.reliability.mark);
        this.checkElement('communicationTlEop', eop.teamLeadEOP.communication.mark);
        this.checkElement('teamwork-cooperationTlEop', eop.teamLeadEOP.teamWorkAndCooperation.mark);
        this.checkElement('autonomyTlEop', eop.teamLeadEOP.autonomy.mark);
    }

    fillLmPartOfEOP(eop) {
        var profile = new Profile();
        profile.getLineManager(eop.employeeId)
            .then((lineManager) => {
                if (lineManager != null)
                    document.getElementById('lm-reporter-name-LmEOP').value = `${lineManager.firstName} ${lineManager.lastName}`;
                else
                    document.getElementById('lm-reporter-name-LmEOP').value = 'undefined';
        });
        document.getElementById('RemarksQualityLmEOP').value = eop.lineManagerEOP.quality.remark;
        document.getElementById('RemarksProductivityLmEOP').value = eop.lineManagerEOP.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeLmEOP').value = eop.lineManagerEOP.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityLmEOP').value = eop.lineManagerEOP.reliability.remark;
        document.getElementById('RemarksCommunicationLmEOP').value = eop.lineManagerEOP.communication.remark;
        document.getElementById('RemarksTeamworkCooperationLmEOP').value = eop.lineManagerEOP.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyLmEOP').value = eop.lineManagerEOP.autonomy.remark;
        document.getElementById('OverallImpressionLmEOP').value = eop.lineManagerEOP.overallImpression;
        document.getElementById('AppraisersCommentsLmEOP').value = eop.lineManagerEOP.apprasierComment;

        this.checkElement('qualityLmEOP', eop.lineManagerEOP.quality.mark);
        this.checkElement('productivityLmEOP', eop.lineManagerEOP.productivity.mark);
        this.checkElement('professional-knowledgeLmEOP', eop.lineManagerEOP.professionalKnowledge.mark);
        this.checkElement('reliabilityLmEOP', eop.lineManagerEOP.reliability.mark);
        this.checkElement('communicationLmEOP', eop.lineManagerEOP.communication.mark);
        this.checkElement('teamworkcooperationLmEOP', eop.lineManagerEOP.teamWorkAndCooperation.mark);
        this.checkElement('autonomyLmEOP', eop.lineManagerEOP.autonomy.mark);
    }

    fillForm(eop, app) { 
        const date = Date.parse(eop.dateOfTalk);  
        var dateObj = new Date(date);   
        dateObj.setMinutes(dateObj.getMinutes() - dateObj.getTimezoneOffset());        

        document.getElementById('talk-date-input-update-eop').value=dateObj.toISOString().slice(0,16);
        document.getElementById('hr-reporter-name-eop').value = eop.hrReporter;
        document.getElementById('employee-impressions-eop').value = eop.reportEOP.employeesImpression;
        document.getElementById('next-steps-eop').value = eop.reportEOP.nextSteps;
        document.getElementById('performance-feedback-eop').value = eop.reportEOP.performanceFeedback;
        document.getElementById('final-recommendation-eop').value = eop.reportEOP.finalRecommendation;

        this.fillClPartOfEOP(eop);
        this.fillTlPartOfEOP(eop);
        this.fillLmPartOfEOP(eop);
        this.fillEopSection(eop);
        this.fillEOPFollowUP(eop);
        this.fillEOPReasons(eop, 'pat-eop-resolved', 'pat-eop-shortComment', 'pat-topic', 0);
        this.fillEOPReasons(eop, 'wwo-eop-resolved', 'wwo-eop-shortComment', 'wwo-topic', 1);
        this.fillEOPReasons(eop, 'tacttp-eop-resolved', 'tacttp-eop-shortComment', 'tacttp-topic', 2);
        this.fillEOPReasons(eop, 'rwceu-eop-resolved', 'rwceu-eop-shortComment', 'rwceu-topic', 3);
        this.fillEOPReasons(eop, 'tc-eop-resolved', 'tc-eop-shortComment', 'tc-topic', 4);
        this.fillEOPReasons(eop, 'cwlm-eop-resolved', 'cwlm-eop-shortComment', 'cwtl-topic', 5);
        this.fillEOPReasons(eop, 'cwtl-eop-resolved', 'cwtl-eop-shortComment', 'cwlm-topic', 6);
        this.fillEOPReasons(eop, 'oswcam-eop-resolved', 'oswcam-eop-shortComment', 'oswcam-topic', 7);
        this.fillEOPReasons(eop, 'icacp-eop-resolved', 'icacp-eop-shortComment', 'icacp-topic', 8);
        this.fillEOPReasons(eop, 'pwe-eop-resolved', 'pwe-eop-shortComment', 'pwe-topic', 9);
        this.fillEOPReasons(eop, 'cjs-eop-resolved', 'cjs-eop-shortComment', 'cjs-topic', 10);
        this.fillEOPReasons(eop, 'tba-eop-resolved', 'tba-eop-shortComment', 'tba-topic', 11);
        this.fillEOPReasons(eop, 'ofpacd-eop-resolved', 'ofpacd-eop-shortComment', 'ofpacd-topic', 12);
        this.fillEOPReasons(eop, 'b-eop-resolved', 'b-eop-shortComment', 'b-topic', 13);
        this.fillEOPReasons(eop, 's-eop-resolved', 's-eop-shortComment', 's-topic', 14);
        this.fillSalaryPart(eop);

        app.getEmployeeById(eop.employeeId).then((emp) => {             
            document.getElementById('modal-eop-profile-name').textContent = `${emp.firstName} ${emp.lastName}`;
            document.getElementById('show-eop-img').src = config.imageUrl + `${emp.imageUrl}`;
            document.getElementById('probation-start').textContent = emp.employmentDate + '.';
            var profile = new Profile();
            profile.getEndDate(emp.employmentDate)
                .then((endDate) => {    
                    document.getElementById('probation-end').textContent = `${app.stringHelper.getShortDateFromString(endDate)}`;                
            });
        })
    }
}