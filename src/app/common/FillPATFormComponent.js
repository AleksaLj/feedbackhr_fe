import { doc } from 'prettier';
import { Component } from '../core/Component';
import getPerformanceFactorEnum from '../utility/GetPerformanceMark';
import { Profile } from '../Profile';
import { App } from '../App';
import config from '../config';

export class FillPATFormComponent extends Component { 
    constructor() { 
        super();
    }

    checkElement(elementName, mark) {
        var markEnum = getPerformanceFactorEnum(mark);
        var elements = document.getElementsByName(elementName);
        for (var i = 0; i < elements.length; i++) {
            if (elements[i].value == markEnum) {               
                elements[i].checked = true;
            } 
        }
    }
  
    fillClPartOfPAT(pat) { 
        document.getElementById('client-appraiser-pat').value = pat.client;
        
        document.getElementById('RemarksQualityClPAT').value = pat.clientPAT.quality.remark;
        document.getElementById('RemarksProductivityClPAT').value = pat.clientPAT.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeClPAT').value = pat.clientPAT.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityClPAT').value = pat.clientPAT.reliability.remark;
        document.getElementById('RemarksCommunicationClPAT').value = pat.clientPAT.communication.remark;
        document.getElementById('RemarksTeamWorkAndCooperationClPAT').value = pat.clientPAT.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyClPAT').value = pat.clientPAT.autonomy.remark;
        document.getElementById('RemarksProblemSolvingClPAT').value = pat.clientPAT.problemSolving.remark;
        document.getElementById('OverallImpresionClPAT').value = pat.clientPAT.overallImpression;
        document.getElementById('AppraisersCommentsClPAT').value = pat.clientPAT.apprasierComment; 
        document.getElementById('PerformanceGoalsClPAT').value = pat.clientPAT.performanceGoals;
        document.getElementById('CareerGoalsClPAT').value = pat.clientPAT.careerGoals;
        document.getElementById('EducationClPAT').value = pat.clientPAT.education;
        document.getElementById('OtherClPAT').value = pat.clientPAT.other;

        this.checkElement('qualityClPAT', pat.clientPAT.quality.mark);
        this.checkElement('productivityClPAT', pat.clientPAT.productivity.mark);
        this.checkElement('professional-knowledgeClPAT', pat.clientPAT.professionalKnowledge.mark);
        this.checkElement('reliabilityClPAT', pat.clientPAT.reliability.mark);
        this.checkElement('communicationClPAT', pat.clientPAT.communication.mark);
        this.checkElement('teamworkcooperationClPAT', pat.clientPAT.teamWorkAndCooperation.mark);
        this.checkElement('autonomyClPAT', pat.clientPAT.autonomy.mark);
        this.checkElement('problemsolvingClPAT', pat.clientPAT.problemSolving.mark);
    }

    fillTlPartOfPAT(pat) { 
        var profile = new Profile();
        profile.getTeamLead(pat.employeeId)
        .then((teamLead) => {
            document.getElementById('tl-reporter-name-pat').value = `${teamLead.firstName} ${teamLead.lastName}`;
        });
        document.getElementById('RemarksQualityTlPAT').value = pat.teamLeadPAT.quality.remark;
        document.getElementById('RemarksProductivityTlPAT').value = pat.teamLeadPAT.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeTlPAT').value = pat.teamLeadPAT.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityTlPAT').value = pat.teamLeadPAT.reliability.remark;
        document.getElementById('RemarksCommunicationTlPAT').value = pat.teamLeadPAT.communication.remark;
        document.getElementById('RemarksTeamWorkAndCooperationTlPAT').value = pat.teamLeadPAT.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyTlPAT').value = pat.teamLeadPAT.autonomy.remark;
        document.getElementById('RemarksProblemSolvingTlPAT').value = pat.teamLeadPAT.problemSolving.remark;
        document.getElementById('OverallImpresionTlPAT').value = pat.teamLeadPAT.overallImpression;
        document.getElementById('AppraisersCommentsTlPAT').value = pat.teamLeadPAT.apprasierComment;
        document.getElementById('PerformanceGoalsTlPAT').value = pat.teamLeadPAT.performanceGoals;
        document.getElementById('CareerGoalsTlPAT').value = pat.teamLeadPAT.careerGoals;
        document.getElementById('EducationTlPAT').value = pat.teamLeadPAT.education;
        document.getElementById('OtherTlPAT').value = pat.teamLeadPAT.other;

        this.checkElement('qualityTlPAT', pat.teamLeadPAT.quality.mark);
        this.checkElement('productivityTlPAT', pat.teamLeadPAT.productivity.mark);
        this.checkElement('professional-knowledgeTlPAT', pat.teamLeadPAT.professionalKnowledge.mark);
        this.checkElement('reliabilityTlPAT', pat.teamLeadPAT.reliability.mark);
        this.checkElement('communicationTlPAT', pat.teamLeadPAT.communication.mark);
        this.checkElement('teamworkcooperationTlPAT', pat.teamLeadPAT.teamWorkAndCooperation.mark);
        this.checkElement('autonomyTlPAT', pat.teamLeadPAT.autonomy.mark);
        this.checkElement('problemsolvingTlPAT', pat.teamLeadPAT.problemSolving.mark);
    }

    fillLmPartOfPAT(pat) { 
        var profile = new Profile();
        profile.getLineManager(pat.employeeId)
            .then((lineManager) => {
                document.getElementById('lm-reporter-name-pat').value=`${lineManager.firstName} ${lineManager.lastName}`;
            });
        document.getElementById('RemarksQualityLmPAT').value = pat.lineManagerPAT.quality.remark;
        document.getElementById('RemarksProductivityLmPAT').value = pat.lineManagerPAT.productivity.remark;
        document.getElementById('RemarksProfessionalKnowledgeLmPAT').value = pat.lineManagerPAT.professionalKnowledge.remark;
        document.getElementById('RemarksReliabilityLmPAT').value = pat.lineManagerPAT.reliability.remark;
        document.getElementById('RemarksCommunicationLmPAT').value = pat.lineManagerPAT.communication.remark;
        document.getElementById('RemarksTeamWorkAndCooperationLmPAT').value = pat.lineManagerPAT.teamWorkAndCooperation.remark;
        document.getElementById('RemarksAutonomyLmPAT').value = pat.lineManagerPAT.autonomy.remark;
        document.getElementById('RemarksProblemSolvingLmPAT').value = pat.lineManagerPAT.problemSolving.remark;
        document.getElementById('OverallImpresionLmPAT').value = pat.lineManagerPAT.overallImpression;
        document.getElementById('AppraisersCommentsLmPAT').value = pat.lineManagerPAT.apprasierComment;
        document.getElementById('PerformanceGoalsLmPAT').value = pat.lineManagerPAT.performanceGoals;
        document.getElementById('CareerGoalsLmPAT').value = pat.lineManagerPAT.careerGoals;
        document.getElementById('EducationLmPAT').value = pat.lineManagerPAT.education;
        document.getElementById('OtherLmPAT').value = pat.lineManagerPAT.other;

        this.checkElement('qualityLmPAT', pat.lineManagerPAT.quality.mark);
        this.checkElement('productivityLmPAT', pat.lineManagerPAT.productivity.mark);
        this.checkElement('professional-knowledgeLmPAT', pat.lineManagerPAT.professionalKnowledge.mark);
        this.checkElement('reliabilityLmPAT', pat.lineManagerPAT.reliability.mark);
        this.checkElement('communicationLmPAT', pat.lineManagerPAT.communication.mark);
        this.checkElement('teamworkcooperationLmPAT', pat.lineManagerPAT.teamWorkAndCooperation.mark);
        this.checkElement('autonomyLmPAT', pat.lineManagerPAT.autonomy.mark);
        this.checkElement('problemsolvingLmPAT', pat.lineManagerPAT.problemSolving.mark);
    }

    fillPATFollowUP(pat) {
        document.getElementById('follow-up-pat').checked = pat.isOpenedForFollowUp;
        document.getElementById('follow-up-hr-reporter-pat').value = pat.followUpSection.hrReporter;
        document.getElementById('pat-hr-notes').value = pat.followUpSection.hrNotes;
        const date = Date.parse(pat.followUpSection.talkDate);  
        var dateObj = new Date(date);     
        dateObj.setMinutes(dateObj.getMinutes() - dateObj.getTimezoneOffset());
        document.getElementById('pat-talk-date').value = dateObj.toISOString().slice(0, 16);
        
        if (pat.isOpenedForFollowUp) {
            document.getElementById('follow-up-pat').setAttribute('checked', 'true');
            document.getElementById('follow-up-form-pat').setAttribute('style', 'display: block');
            document.getElementById('follow-up-reasons-pat').setAttribute('style', 'display: block');        
        }
    }

    fillPATReasons(pat, resolvedId, commentId, topicId, arrayNumber) {
        document.getElementById(resolvedId).checked = pat.followUpsToDo[arrayNumber].resolved;
        document.getElementById(commentId).value = pat.followUpsToDo[arrayNumber].shortComment;

        if (pat.followUpSection.followUpOptions[arrayNumber].checked)
            document.getElementById(topicId).setAttribute('style', 'display: block');
        
        if (pat.followUpSection.followUpOptions[14].checked) {
            document.getElementById('salary-after-pat').setAttribute('style', 'display: block');
        }
        else {
            document.getElementById('salary-after-pat').setAttribute('style', 'display: none');
        }
        document.getElementById('s-pat').setAttribute('disabled', true);
    }

    fillPatSection(pat) {
        document.getElementById('pat-pat').checked = pat.followUpSection.followUpOptions[0].checked;
        document.getElementById('wwo-pat').checked = pat.followUpSection.followUpOptions[1].checked;
        document.getElementById('tacttp-pat').checked = pat.followUpSection.followUpOptions[2].checked;
        document.getElementById('rwceu-pat').checked = pat.followUpSection.followUpOptions[3].checked;
        document.getElementById('tc-pat').checked = pat.followUpSection.followUpOptions[4].checked;
        document.getElementById('cwtl-pat').checked = pat.followUpSection.followUpOptions[5].checked;
        document.getElementById('cwlm-pat').checked = pat.followUpSection.followUpOptions[6].checked;
        document.getElementById('oswcam-pat').checked = pat.followUpSection.followUpOptions[7].checked;
        document.getElementById('icacp-pat').checked = pat.followUpSection.followUpOptions[8].checked;
        document.getElementById('pwe-pat').checked = pat.followUpSection.followUpOptions[9].checked;
        document.getElementById('cjs-pat').checked = pat.followUpSection.followUpOptions[10].checked;
        document.getElementById('tba-pat').checked = pat.followUpSection.followUpOptions[11].checked;
        document.getElementById('ofpacd-pat').checked = pat.followUpSection.followUpOptions[12].checked;
        document.getElementById('b-pat').checked = pat.followUpSection.followUpOptions[13].checked;
        document.getElementById('s-pat').checked = pat.followUpSection.followUpOptions[14].checked;
    }

    fillSalaryPart(pat) { 
        document.getElementById('satisfied-pat').checked = pat.salary.isSatisfied;
        document.getElementById('salary-change-pat').checked = pat.salary.salaryChanged;
        document.getElementById('salary-change-amount-pat').value = pat.salary.changedFor;
        document.getElementById('expected-pat').value = pat.salary.desiredAmount;
        
        document.getElementById('satisfied-after-follow-up-pat').checked = pat.followUpsToDo[14].salaryAfter.isSatisfied;
        document.getElementById('changed-after-follow-up-pat').checked = pat.followUpsToDo[14].salaryAfter.salaryChanged;
        document.getElementById('amount-after-follow-up-pat').value = pat.followUpsToDo[14].salaryAfter.changedFor;
        document.getElementById('satisfied-amount-after-follow-up-pat').value = pat.followUpsToDo[14].salaryAfter.desiredAmount;

        if (pat.salary.salaryChanged) {
            document.getElementById('salary-change-amount-pat').removeAttribute('disabled');
        }
        if (pat.salary.isSatisfied == false) {
            document.getElementById('expected-pat').removeAttribute('disabled');
            document.getElementById('s-pat').setAttribute('disabled', true);
            document.getElementById('s-pat').setAttribute('checked', true);
            document.getElementById('follow-up-pat').setAttribute('checked', true);
            document.getElementById('follow-up-reasons-pat').setAttribute('style', 'display: block');
            document.getElementById('follow-up-form-pat').setAttribute('style', 'display: block');
            document.getElementById('s-topic-pat').setAttribute('style', 'display: block');
            document.getElementById('salary-after-pat').setAttribute('style', 'display: block');
            if (document.getElementById('satisfied-after-follow-up-pat').checked) {
                document.getElementById('satisfied-amount-after-follow-up-pat').setAttribute('disabled', true);
            }
            else {
                document.getElementById('satisfied-amount-after-follow-up-pat').removeAttribute('disabled');
            }
            if (document.getElementById('changed-after-follow-up-pat').checked) {
                document.getElementById('amount-after-follow-up-pat').removeAttribute('disabled');
            }
        }
        else { 
            document.getElementById('expected-pat').setAttribute('disabled', true);
            document.getElementById('s-pat').setAttribute('disabled', true);
            document.getElementById('s-pat').setAttribute('checked', false);
            document.getElementById('s-topic-pat').setAttribute('style', 'display: none');
            document.getElementById('salary-after-pat').setAttribute('style', 'display: none');
            document.getElementById('changed-after-follow-up-pat').setAttribute('checked', false);
            document.getElementById('amount-after-follow-up-pat').setAttribute('disabled', true);
            document.getElementById('expected-pat').value = '';
            document.getElementById('amount-after-follow-up-pat').value = '';
            document.getElementById('satisfied-amount-after-follow-up-pat').value = '';
            document.getElementById('s-pat-shortComment').value = '';
        }
    }

    fillForm(pat, app) { 
        const date = Date.parse(pat.dateOfTalk);  
        var dateObj = new Date(date); 
        dateObj.setMinutes(dateObj.getMinutes() - dateObj.getTimezoneOffset());
        document.getElementById('talk-date-input-update-pat').value = dateObj.toISOString().slice(0,16);
        document.getElementById('hr-reporter-name-pat').value = pat.hrReporter;

        this.fillClPartOfPAT(pat);
        this.fillTlPartOfPAT(pat);
        this.fillLmPartOfPAT(pat);
        this.fillPatSection(pat);
        this.fillPATFollowUP(pat);
        this.fillPATReasons(pat, 'pat-pat-resolved', 'pat-pat-shortComment', 'pat-topic-pat', 0);
        this.fillPATReasons(pat, 'wwo-pat-resolved', 'wwo-pat-shortComment', 'wwo-topic-pat', 1);
        this.fillPATReasons(pat, 'tacttp-pat-resolved', 'tacttp-pat-shortComment', 'tacttp-topic-pat', 2);
        this.fillPATReasons(pat, 'rwceu-pat-resolved', 'rwceu-pat-shortComment', 'rwceu-topic-pat', 3);
        this.fillPATReasons(pat, 'tc-pat-resolved', 'tc-pat-shortComment', 'tc-topic-pat', 4);
        this.fillPATReasons(pat, 'cwlm-pat-resolved', 'cwlm-pat-shortComment', 'cwlm-topic-pat', 5);
        this.fillPATReasons(pat, 'cwtl-pat-resolved', 'cwtl-pat-shortComment', 'cwtl-topic-pat', 6);
        this.fillPATReasons(pat, 'oswcam-pat-resolved', 'oswcam-pat-shortComment', 'oswcam-topic-pat', 7);
        this.fillPATReasons(pat, 'icacp-pat-resolved', 'icacp-pat-shortComment', 'icacp-topic-pat', 8);
        this.fillPATReasons(pat, 'pwe-pat-resolved', 'pwe-pat-shortComment', 'pwe-topic-pat', 9);
        this.fillPATReasons(pat, 'cjs-pat-resolved', 'cjs-pat-shortComment', 'cjs-topic-pat', 10);
        this.fillPATReasons(pat, 'tba-pat-resolved', 'tba-pat-shortComment', 'tba-topic-pat', 11);
        this.fillPATReasons(pat, 'ofpacd-pat-resolved', 'ofpacd-pat-shortComment', 'ofpacd-topic-pat', 12);
        this.fillPATReasons(pat, 'b-pat-resolved', 'b-pat-shortComment', 'b-topic-pat', 13);
        this.fillPATReasons(pat, 's-pat-resolved', 's-pat-shortComment', 's-topic-pat', 14);
        this.fillSalaryPart(pat);

        app.getEmployeeById(pat.employeeId).then((emp) => {             
            document.getElementById('modal-pat-profile-name').textContent = `${emp.firstName} ${emp.lastName}`;
            document.getElementById('show-pat-img').src = config.imageUrl + `${emp.imageUrl}`;
        })
    }
}