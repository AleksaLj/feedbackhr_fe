import { Component } from '../../core/Component';
import { AdHocFormModel } from '../../models/AdHocFormModel';
import config from './../../config';
import { App } from './../../App';
export class FillAdHocForm extends Component {
    constructor() {
        super();
    }

    fillSalaryPart(adHoc) {
        document.getElementById('ad-hoc-changed-before-follow-up-update').checked = adHoc.salary.salaryChanged;
        document.getElementById('ad-hoc-amount-before-follow-up-update').value = adHoc.salary.changedFor;
        document.getElementById('ad-hoc-satisfied-before-follow-up-update').checked = adHoc.salary.isSatisfied;
        document.getElementById('ad-hoc-expected-salary-before-follow-up-update').value = adHoc.salary.desiredAmount;

        if (adHoc.followUpsConcrete[14].salaryAfter != null) {
            document.getElementById('ad-hoc-changed-after-follow-up-update').checked = adHoc.followUpsConcrete[14].salaryAfter.salaryChanged;
            document.getElementById('ad-hoc-amount-after-follow-up-update').value = adHoc.followUpsConcrete[14].salaryAfter.changedFor;
            document.getElementById('ad-hoc-satisfied-after-follow-up-update').checked = adHoc.followUpsConcrete[14].salaryAfter.isSatisfied;
            document.getElementById('ad-hoc-expected-salary-after-follow-up-update').value = adHoc.followUpsConcrete[14].salaryAfter.desiredAmount;
        }

        if (adHoc.salary.salaryChanged == true) { 
            document.getElementById('ad-hoc-amount-before-follow-up-update').removeAttribute('disabled');
        }
        if (adHoc.salary.isSatisfied == false) {
            document.getElementById('ad-hoc-expected-salary-before-follow-up-update').removeAttribute('disabled');
            document.getElementById('ad-hoc-changed-before-follow-up-update').setAttribute('checked', true);
            document.getElementById('s-ad-hoc-update').setAttribute('disabled', true);
            document.getElementById('s-ad-hoc-update').setAttribute('checked', true);
            document.getElementById('ad-hoc-follow-up-update').setAttribute('checked', true);
            document.getElementById('ad-hoc-follow-up-reasons-update').setAttribute('style', 'display: block');
            document.getElementById('ad-hoc-follow-up-form-update').setAttribute('style', 'display: block');
            document.getElementById('ad-hoc-s-topic-update').setAttribute('style', 'display: block');
            document.getElementById('ad-hoc-salary-after-update').setAttribute('style', 'display: block');
            if (document.getElementById('ad-hoc-satisfied-after-follow-up-update').checked) {
                document.getElementById('ad-hoc-expected-salary-after-follow-up-update').setAttribute('disabled', true);
            }
            if (document.getElementById('ad-hoc-changed-after-follow-up-update').checked) {
                document.getElementById('ad-hoc-amount-after-follow-up-update').removeAttribute('disabled');
            }
        }
        else { 
            document.getElementById('ad-hoc-expected-salary-before-follow-up-update').setAttribute('disabled', true);
            document.getElementById('ad-hoc-changed-before-follow-up-update').setAttribute('checked', false);
            document.getElementById('ad-hoc-amount-before-follow-up-update').setAttribute('disabled', true);
            document.getElementById('s-ad-hoc-update').setAttribute('disabled', true);
            document.getElementById('s-ad-hoc-update').setAttribute('checked', false);
            document.getElementById('ad-hoc-s-topic-update').setAttribute('style', 'display: none');
            document.getElementById('ad-hoc-salary-after-update').setAttribute('style', 'display: none');
            document.getElementById('ad-hoc-changed-after-follow-up-update').setAttribute('checked', false);
            document.getElementById('ad-hoc-amount-after-follow-up-update').setAttribute('disabled', true);
            document.getElementById('ad-hoc-amount-before-follow-up-update').value = '';
            document.getElementById('ad-hoc-expected-salary-before-follow-up-update').value = '';
            document.getElementById('ad-hoc-amount-after-follow-up-update').value = '';
            document.getElementById('ad-hoc-expected-salary-after-follow-up-update').value = '';
            document.getElementById('s-ad-hoc-shortComment-update').value = '';
        }
    }

    fillAdHocSection(adHoc) { 
        document.getElementById('pat-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[0].checked;
        document.getElementById('wwo-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[1].checked;
        document.getElementById('tacttp-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[2].checked;
        document.getElementById('rwceu-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[3].checked;
        document.getElementById('tc-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[4].checked;
        document.getElementById('cwtl-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[5].checked;
        document.getElementById('cwlm-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[6].checked;
        document.getElementById('oswcam-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[7].checked;
        document.getElementById('icacp-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[8].checked;
        document.getElementById('pwe-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[9].checked;
        document.getElementById('cjs-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[10].checked;
        document.getElementById('tba-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[11].checked;
        document.getElementById('ofpacd-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[12].checked;
        document.getElementById('b-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[13].checked;
        document.getElementById('s-ad-hoc-update').checked = adHoc.followUpSection.followUpOptions[14].checked;
    }

    fillAdHocFollowUp(adHoc) { 
        document.getElementById('ad-hoc-fup-hr-reporter-update').value = adHoc.followUpSection.hrReporter;
        document.getElementById('ad-hoc-fup-hr-notes-update').value = adHoc.followUpSection.hrNotes;
        const date = Date.parse(adHoc.followUpSection.talkDate);  
        var dateOfTalk = new Date(date);     
        dateOfTalk.setMinutes(dateOfTalk.getMinutes() - dateOfTalk.getTimezoneOffset());
        document.getElementById('ad-hoc-follow-up-update').setAttribute('checked', 'true');
        document.getElementById('ad-hoc-follow-up-form-update').setAttribute('style', 'display: block');
        document.getElementById('ad-hoc-follow-up-reasons-update').setAttribute('style', 'display: block');        
    }

    fillAdHocReason(adHoc, elIndex, ...elIds) {
        //setting resolved
        document.getElementById(elIds[0]).checked = adHoc.followUpsConcrete[elIndex].resolved;
        //setting comment
        document.getElementById(elIds[1]).value = adHoc.followUpsConcrete[elIndex].shortComment;

        //show checked topic 
        if (adHoc.followUpSection.followUpOptions[elIndex].checked)
            document.getElementById(elIds[2]).setAttribute('style', 'display: block');
    }

    fillForm(adHoc, app) { 
        const date = Date.parse(adHoc.dateOfTalk);
        var dateOfTalk = new Date(date);
        dateOfTalk.setMinutes(dateOfTalk.getMinutes() - dateOfTalk.getTimezoneOffset()); 
        document.getElementById('ad-hoc-talk-date-update').value = dateOfTalk.toISOString().slice(0, 16);
        document.getElementById('ad-hoc-hr-reporter-update').value = adHoc.reporter;

        this.fillSalaryPart(adHoc);
        this.fillAdHocSection(adHoc);
        this.fillAdHocFollowUp(adHoc);
        this.fillAdHocReason(adHoc, 0, 'pat-ad-hoc-resolved-update', 'pat-ad-hoc-shortComment-update', 'ad-hoc-pat-topic-update');
        this.fillAdHocReason(adHoc, 1, 'wwo-ad-hoc-resolved-update', 'wwo-ad-hoc-shortComment-update', 'ad-hoc-wwo-topic-update');
        this.fillAdHocReason(adHoc, 2, 'tacttp-ad-hoc-resolved-update', 'tacttp-ad-hoc-shortComment-update', 'ad-hoc-tacttp-topic-update');
        this.fillAdHocReason(adHoc, 3, 'rwceu-ad-hoc-resolved-update', 'rwceu-ad-hoc-shortComment-update', 'ad-hoc-rwceu-topic-update');
        this.fillAdHocReason(adHoc, 4, 'tc-ad-hoc-resolved-update', 'tc-ad-hoc-shortComment-update', 'ad-hoc-tc-topic-update');
        this.fillAdHocReason(adHoc, 5, 'cwlm-ad-hoc-resolved-update', 'cwlm-ad-hoc-shortComment-update', 'ad-hoc-cwlm-topic-update');
        this.fillAdHocReason(adHoc, 6, 'cwtl-ad-hoc-resolved-update', 'cwtl-ad-hoc-shortComment-update', 'ad-hoc-cwtl-topic-update');
        this.fillAdHocReason(adHoc, 7, 'oswcam-ad-hoc-resolved-update', 'oswcam-ad-hoc-shortComment-update', 'ad-hoc-oswcam-topic-update');
        this.fillAdHocReason(adHoc, 8, 'icacp-ad-hoc-resolved-update', 'icacp-ad-hoc-shortComment-update', 'ad-hoc-icacp-topic-update');
        this.fillAdHocReason(adHoc, 9, 'pwe-ad-hoc-resolved-update', 'pwe-ad-hoc-shortComment-update', 'ad-hoc-pwe-topic-update');
        this.fillAdHocReason(adHoc, 10, 'cjs-ad-hoc-resolved-update', 'cjs-ad-hoc-shortComment-update', 'ad-hoc-cjs-topic-update');
        this.fillAdHocReason(adHoc, 11, 'tba-ad-hoc-resolved-update', 'tba-ad-hoc-shortComment-update', 'ad-hoc-tba-topic-update');
        this.fillAdHocReason(adHoc, 12, 'ofpacd-ad-hoc-resolved-update', 'ofpacd-ad-hoc-shortComment-update', 'ad-hoc-ofpacd-topic-update');
        this.fillAdHocReason(adHoc, 13, 'b-ad-hoc-resolved-update', 'b-ad-hoc-shortComment-update', 'ad-hoc-b-topic-update');
        this.fillAdHocReason(adHoc, 14, 's-ad-hoc-resolved-update', 's-ad-hoc-shortComment-update', 'ad-hoc-s-topic-update');

        app.getEmployeeById(adHoc.employeeId).then((emp) => {             
            document.getElementById('ad-hoc-profile-name-upd').textContent = `${emp.firstName} ${emp.lastName}`;
            document.getElementById('show-ad-hoc-img').src = config.imageUrl + `${emp.imageUrl}`;
        })
    }
}