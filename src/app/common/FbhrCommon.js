import { Component } from '../core/Component';
import config from '../config';

export class FbhrCommon extends Component {

    constructor() {
        super();
    };

    getEmployeePosition(positionId) {
        const URL = config.url + config.getPosition +`${positionId}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                
                const position = JSON.parse(data);
                return Promise.resolve(position);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getEmployeeById(employeeId) {
        const URL = config.url + config.getEmployee + `${employeeId}`;

        fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const employee = JSON.parse(data);
                console.log(employee);
                return Promise.resolve(employee);
            })
            .catch((error) => {
                console.log(error);
            })
    }
}