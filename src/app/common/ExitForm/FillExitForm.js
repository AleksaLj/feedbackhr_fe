import { doc } from "prettier";
import config from "../../config";
import { Component } from "../../core/Component";
import { StringHelper } from "../../utility/StringHelper";

export class FillExitForm extends Component{

    constructor() {
        super();
        this.stringHelper = new StringHelper();
    };

    fillForm(exitFrom) {
        document.getElementById('create-date-exitupdate').value = this.stringHelper.getShortDateFromString(exitFrom.dateOfTalk);
        document.getElementById('hr-notes-exitupdate').value = exitFrom.hrNotes;
        document.getElementById('exitupdate-hr-representative').textContent = 'Jane Doe';
        document.getElementById('exit-download-questionary').addEventListener('click', () => {
            var fileName = exitFrom.employeeId + "_Exit";
            this.getExitQuestionary(fileName);
        });

    }

    getExitQuestionary(fileName) {

        const url = config.url + config.downloadQuestionary + `${fileName}`;

        fetch(
            url, {
            method: 'GET'
            })
            .then((response) => response.blob())
            .then(blob => URL.createObjectURL(blob))
            .then(newUrl => {
                window.open(newUrl, '_blank');
                URL.revokeObjectURL(newUrl);
            })
            .catch((error) => {
                console.log(error);
            });

    }
}