$(document).ready(function () {


    //Sacuvan Follow up reason kada se otvori ponovo, da mu se otvori njegov mali follow up za komentar,
//    radi samo kada se okine, ne i kada je stiklirano vec


    //NOTIF MESSAGES
    // var error = {
    //     dataLoad: 'Error Loading Data! Please try Again.',
    //     incomplete: 'Please select required data.',
    //     other: 'Bla, bla, bla...',
    // }
    // var success = {
    //     save: 'Sucessfully Saved.',
    //     add: 'Successfully added',
    //     appActive: 'Parking App successfuly activated',
    //     appInactive: 'Parking App sucessfulu Paused',
    //     spotInUse: 'Great! Now you have a parking spot for the next week'
    // }
    // var info = {
    //     general: 'Lep je dan napolju!',
    //     reccuringModeOff: 'Reccuring Mode has been turned Off. Campaigns will not repeat weekly.',
    //     reccuringModeOn: 'Reccuring Mode has been turned On. Campaigns will repeat on selected days.',
    //     general: 'some info',
    // };

    //NOTIFICATION INIT
    Notiflix.Notify.Init({
        width: '300px',
        fontSize: '13px',
        fontFamily: 'Roboto',
        timeout: 4000,
        messageMaxLength: 200,
        success: {
            background: '#28a745',
            childClassName: 'success',
            notiflixIconColor: 'rgba(255,255,255,0.6)',
        },
        failure: {
            background: '#d8483e',
            childClassName: 'failure',
            notiflixIconColor: 'rgba(255,255,255,0.6)',
        },
        info: {
            background: '#0000b4',
            childClassName: 'info',
            notiflixIconColor: 'rgba(255,255,255,0.6)',
        },
    });

    $('.nt-save').on('click', function(){
        //Notiflix.Notify.Success('sucessfully saved');
    });


    $('.fb-tab').click(function () {
        var tab_id = $(this).attr('data-tab');
        $('.fb-tab').removeClass('selected');
        if (tab_id != 'tab-1' && tab_id != 'tab-2') {
            $('.tab-content').hide();
            $('#tab-1').show();
            $('#tab-2').show();
        }
        else {
            $('.tab-content').hide();
        }
        $(this).addClass('selected');
        $("#" + tab_id).show();
    });

    // CLOSE BUTTON MODAL STYLE
    $('.close').prop('style', 'border-radius: 0px');
    $('.close').prop('style', 'border: 0px !important');
    
    //CANCEL BUTTON STYLE ON YES(confirm)
    $('#yes').on('click', function () {
        setTimeout(function () { window.location.reload() }, 500);
    });

    $('#yes-save').on('click', function () {
        setTimeout(function () { window.location.reload() }, 500);
    });

    $('#yes-delete').on('click', function () {
        setTimeout(function () { window.location.reload() }, 500);
    });
    
    $('#yes-index').on('click', function () {
        setTimeout(function () { window.location.reload() }, 500);
    });

    $('#yes-save-index').on('click', function () {
        setTimeout(function () { window.location.reload() }, 500);
    });
    // FIRST MODAL AFTER CLOSING SECOND
    $(document).find('.child-modal').on('hidden.bs.modal', function() {
        $('body').addClass('modal-open');
    });

    //***SEARCH FILTER START***//
    //toggle search reset visibility
    $('#reset-icon').hide();

    $('#find-employee').keyup(function () {
        var s = $(this).val();
        if (s.length > 0) {
            $('#reset-icon').show();
        } else {
            $('#reset-icon').hide();
        }
    });

    //clear search val & focus
    $('#reset-icon').on('click', function () {
        $('#find-employee').val('').focus();
        $(this).hide();
        $('.fb-employee').show();
        $('.no-results-message').hide();
    });

    //Filter list
    $('#find-employee').keyup(function () {
        var filter = $(this).val(),
            count = 0;
        $('.fb-employee').each(function () {
            if ($(this).text().search(new RegExp(filter, "i")) < 0) {
                $(this).hide();
            } else {
                $(this).show();
                count++;
            }
        });
        if (count > 0) {
            $('.no-results-message').hide();
            $('.vct-actions').hide();
        } else {
            $('.no-results-message').show();
            $('.vct-actions').hide();
        }
    });
    //***SEARCH FILTER END***//

    //DROPDOWN MENU
    $('.fb-dropdown-cta').on('click', function () {
        $('.fb-dropdown-menu').hide();
        $(this).find('.fb-dropdown-menu').show();
    });

    $(document).mouseup(function (e) {
        var container = $('.fb-dropdown-menu');
        if (!container.is(e.target) && container.has(e.target).length === 0) {
            container.hide();
        };
    });

    $('.fb-menu-item').on('mouseup', function(){
        $('.fb-dropdown-menu').hide();
    });

    //RESET FILTERS
    $('#reset-selection').on('click', function(){
        var select = $('#fb-filters').find('select');
        select.prop('selectedIndex',0);
    });

    //LOAD MORE
    $('.js_loadMore').hide();
    if ($(window).height() < 800) {
        $(function () {
            $('.js_loadMore').slice(0, 4).show();
            $('#load-more').on('click', function (e) {
                e.preventDefault();
                $('.js_loadMore:hidden').slice(0, 4).show(200);
                if ($('.js_loadMore:hidden').length == 0) {
                    $('#load-more').hide();
                }
            });
        });
    }
    else {
        $(function () {
            $('.js_loadMore').slice(0, 12).show();
            $('#load-more').on('click', function (e) {
                e.preventDefault();
                $('.js_loadMore:hidden').slice(0, 4).show(200);
                if ($('.js_loadMore:hidden').length == 0) {
                    $('#load-more').hide();
                }
            });
        });
    };

    // FOLLOW-UP REASONS STYLE
    $('.ml-1').prop('style', 'width: 200px; height: 27px; word-wrap: break-word; white-space: initial !important;');
    $(window).resize(function () {
        if ($(window).width() > 767 && $(window).width() <= 991) {
            $('.ml-1').prop('style', ' width: 185px; height: 27px; word-wrap: break-word; white-space: initial !important;');
        }
        if ($(window).width() <= 767) {
            $('.ml-1').prop('style', '');
        }
    });

    //UI HELP TOOLS
    $('.helper-switch').click(function () {
        var tab_id = $(this).attr('data-tab');
        $('.js_uiHelperPanel').hide();
        $("#" + tab_id).show();
    });
    function jqUpdateSize(){
        var width = $(window).width();
        var height = $(window).height();
        $('#wWidth').html(width);
        $('#wHeight').html(height);
    };

    $(document).ready(jqUpdateSize);

    $(window).resize(jqUpdateSize);

    //TEMP
    // $(window).on('load',function(){
    //     $('#document').modal('show');
    // });

    //AD-HOC
    $('#ad-hoc-follow-up').on('change', function () {
        if ($('#ad-hoc-satisfied-before-follow-up').prop('checked')) {
            $('#s-ad-hoc').prop('disabled', true);
            $('#s-ad-hoc').prop('checked', false);
        }
        if ($(this).prop('checked')) {
            $('#ad-hoc-follow-up-reasons').show();
            $('#ad-hoc-follow-up-form').show();
        }
        else {
            $('#ad-hoc-follow-up-reasons').hide();
            $('#ad-hoc-follow-up-form').hide();
        }
    })

    $('#ad-hoc-follow-up-update').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-follow-up-reasons-update').show();
            $('#ad-hoc-follow-up-form-update').show();

        }
        else {
            $('#ad-hoc-follow-up-reasons-update').hide();
            $('#ad-hoc-follow-up-form-update').hide();
        }
    })

    //SALARY BEFORE FOLLOW UP CHANGE
    $('#ad-hoc-changed-before-follow-up').on('change', function(){
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-before-follow-up').prop('disabled', false);
            $('#ad-hoc-amount-before-follow-up').focus();
        }
        else {
            $('#ad-hoc-amount-before-follow-up').prop('disabled', true);
            $('#ad-hoc-amount-before-follow-up').val('');
        }
    });

    $('#ad-hoc-changed-before-follow-up-update').on('change', function(){
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-before-follow-up-update').prop('disabled', false);
            $('#ad-hoc-amount-before-follow-up-update').focus();
        }
        else {
            $('#ad-hoc-amount-before-follow-up-update').prop('disabled', true);
            $('#ad-hoc-amount-before-follow-up-update').val('');
        }
    });

    //SALARY AFTER FOLLOW UP CHANGE
    $('#ad-hoc-changed-after-follow-up').on('change', function(){
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-after-follow-up').prop('disabled', false);
            $('#ad-hoc-amount-after-follow-up').focus();
        }
        else {
            $('#ad-hoc-amount-after-follow-up').prop('disabled', true);
            $('#ad-hoc-amount-after-follow-up').val('');
        }
    });

    $('#ad-hoc-changed-after-follow-up-update').on('change', function(){
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', false);
            $('#ad-hoc-amount-after-follow-up-update').focus();
        }
        else {
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', true);
            $('#ad-hoc-amount-after-follow-up-update').val('');
        }
    });

    //SALARY CHANGE EOP
    $('#salary-change').on('change', function(){
        if($(this).prop('checked')){
            $('#salary-change-amount').prop('disabled', false);
            $('#salary-change-amount').focus();
        }
        else{
            $('#salary-change-amount').prop('disabled', true);
            $('#salary-change-amount').val('');
        }
    });

    //SALARY CHANGE PAT
    $('#salary-change-pat').on('change', function(){
        if($(this).prop('checked')){
            $('#salary-change-amount-pat').prop('disabled', false);
            $('#salary-change-amount-pat').focus();
        }
        else{
            $('#salary-change-amount-pat').prop('disabled', true);
            $('#salary-change-amount-pat').val('');
        }
    });

    //FOLLOW UP EOP CHECK
    $('#follow-up').on('change', function () {
        if ($('#satisfied').prop('checked')) {
            $('#s-eop').prop('disabled', true);
            $('#s-eop').prop('checked', false);
        }
        if($(this).prop('checked')){
            $('#follow-up-reasons').show();
            $('#follow-up-form').show();
        }
        else {
            $('#follow-up-reasons').hide();
            $('#follow-up-form').hide();
        }
    });

    $('#ad-hoc-satisfied-before-follow-up').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-expected-salary-before-follow-up').prop('disabled', true);
            $('#s-ad-hoc').prop('disabled', true);
            $('#s-ad-hoc').prop('checked', false);
            $('#ad-hoc-s-topic').hide();
            $('#ad-hoc-salary-after').hide();
            $('#ad-hoc-changed-after-follow-up').prop('checked', false);
            $('#ad-hoc-amount-after-follow-up').prop('disabled', true);
            $('#ad-hoc-expected-salary-before-follow-up').val('');
            $('#ad-hoc-amount-after-follow-up').val('');
            $('#ad-hoc-expected-salary-after-follow-up').val('');
            $('#s-ad-hoc-shortComment').val('');
        }
        else {
            $('#ad-hoc-expected-salary-before-follow-up').prop('disabled', false);
            $('#ad-hoc-expected-salary-before-follow-up').focus();
            $('#s-ad-hoc').prop('disabled', true);
            $('#s-ad-hoc').prop('checked', true);
            $('#ad-hoc-follow-up').prop('checked', true);
            $('#ad-hoc-follow-up-reasons').show();
            $('#ad-hoc-follow-up-form').show();
            $('#ad-hoc-s-topic').show();
            $('#ad-hoc-salary-after').show();
            $('#ad-hoc-changed-after-follow-up').prop('checked', false);
            $('#ad-hoc-amount-after-follow-up').prop('disabled', true);
            $('#ad-hoc-expected-salary-after-follow-up').prop('disabled', true);
            $('#ad-hoc-satisfied-after-follow-up').prop('checked', true);
        }
    });

    $('#ad-hoc-satisfied-before-follow-up-update').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-expected-salary-before-follow-up-update').prop('disabled', true);
            $('#s-ad-hoc-update').prop('disabled', true);
            $('#s-ad-hoc-update').prop('checked', false);
            $('#ad-hoc-s-topic-update').hide();
            $('#ad-hoc-salary-after-update').hide();
            $('#ad-hoc-changed-after-follow-up-update').prop('checked', false);
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', true);
            $('#ad-hoc-expected-salary-before-follow-up-update').val('');
            $('#ad-hoc-amount-after-follow-up-update').val('');
            $('#ad-hoc-expected-salary-after-follow-up-update').val('');
            $('#s-ad-hoc-shortComment-update').val('');
        }
        else {
            $('#ad-hoc-expected-salary-before-follow-up-update').prop('disabled', false);
            $('#ad-hoc-expected-salary-before-follow-up-update').focus();
            $('#s-ad-hoc-update').prop('disabled', true);
            $('#s-ad-hoc-update').prop('checked', true);
            $('#ad-hoc-follow-up-update').prop('checked', true);
            $('#ad-hoc-follow-up-reasons-update').show();
            $('#ad-hoc-follow-up-form-update').show();
            $('#ad-hoc-s-topic-update').show();
            $('#ad-hoc-salary-after-update').show();
            $('#ad-hoc-satisfied-after-follow-up-update').prop('checked', true);
            $('#ad-hoc-changed-after-follow-up-update').prop('checked', false);
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', true);
            $('#ad-hoc-expected-salary-after-follow-up-update').prop('disabled', true);

        }
    });

    $('#ad-hoc-satisfied-after-follow-up').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-expected-salary-after-follow-up').prop('disabled', true);
            $('#ad-hoc-expected-salary-after-follow-up').val('');
        } else {
            $('#ad-hoc-expected-salary-after-follow-up').prop('disabled', false);
            $('#ad-hoc-expected-salary-after-follow-up').focus();
        }
    });

    $('#ad-hoc-satisfied-after-follow-up-update').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-expected-salary-after-follow-up-update').prop('disabled', true);
            $('#ad-hoc-expected-salary-after-follow-up-update').val('');
        } else {
            $('#ad-hoc-expected-salary-after-follow-up-update').prop('disabled', false);
            $('#ad-hoc-expected-salary-after-follow-up-update').focus();
        }
    });

    $('#ad-hoc-changed-follow-up').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-after-follow-up').prop('disabled', false);
            $('#ad-hoc-amount-after-follow-up').focus();
        } else {
            $('#ad-hoc-amount-after-follow-up').prop('disabled', true);
            $('#ad-hoc-amount-after-follow-up').val('');
        }
    });

    $('#ad-hoc-changed-follow-up-update').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', false);
            $('#ad-hoc-amount-after-follow-up-update').focus();
        } else {
            $('#ad-hoc-amount-after-follow-up-update').prop('disabled', true);
            $('#ad-hoc-amount-after-follow-up-update').val('');
        }
    });

    //FOLLOW UP PAT CHECK
    $('#follow-up-pat').on('change', function () {
        if ($('#satisfied-pat').prop('checked')) {
            $('#s-pat').prop('disabled', true);
            $('#s-pat').prop('checked', false);
        }
        if($(this).prop('checked')){
            $('#follow-up-reasons-pat').show();
            $('#follow-up-form-pat').show();
        }
        else{
            $('#follow-up-reasons-pat').hide();
            $('#follow-up-form-pat').hide();
        }
    });

    //SATISFIED-EOP
    $('#satisfied').on('change',function(){
        if ($(this).prop('checked')) {
            $('#expected').prop('disabled', true);
            $('#salary-change').prop('disabled', true);
            //$('#salary').prop('checked', false);
            $('#expected').val('');
            if($('#follow-up').prop('checked')){
                $('#follow-up').prop('checked', true);
                $('#follow-up-reasons').show();
                $('#follow-up-form').show();
                $('#salary-after-eop').hide();
                $('#s-eop').prop('disabled', true);
                $('#s-eop').prop('checked', false);
            }
            else {
                $('#follow-up').prop('checked', false);
                $('#follow-up-reasons').hide();
                $('#follow-up-form').hide();
                $('#salary-after-eop').show();
                $('#s-eop').prop('disabled', true);
                $('#s-eop').prop('checked', false);
            }
            $('#s-topic').hide();
        }
        else{
            $('#expected').prop('disabled', false);
            $('#salary-change').prop('disabled', false);
            $('#follow-up').prop('checked', true);
            //$('#salary').prop('checked', true);
            $('#s-eop').prop('checked', true);
            $('#follow-up-reasons').show();
            $('#follow-up-form').show();
            $('#s-topic').show();
            $('#expected').focus();
            $('#s-eop').prop('disabled', true);
            $('#s-eop').prop('checked', true);
            //$('#salary').prop('disabled', true);
            $('#salary-after-eop').show();
        }
    });

    //SATISFIED-PAT
    $('#satisfied-pat').on('change',function(){
        if ($(this).prop('checked')) {
            $('#expected-pat').prop('disabled', true);
            $('#salary-change-pat').prop('disabled', true);
            //$('#salary').prop('checked', false);
            $('#expected-pat').val('');
            if ($('#follow-up-pat').prop('checked')) {
                $('#follow-up-pat').prop('checked', true);
                $('#follow-up-reasons-pat').show();
                $('#follow-up-form-pat').show();
                $('#salary-after-pat').hide();
                $('#s-pat').prop('disabled', true);
                $('#s-pat').prop('checked', false);
            }
            else {
                $('#follow-up-pat').prop('checked', false);
                $('#follow-up-reasons-pat').hide();
                $('#follow-up-form-pat').hide();
                $('#salary-after-pat').show();
                $('#s-pat').prop('disabled', true);
                $('#s-pat').prop('checked', false);
            }
            $('#s-topic-pat').hide();
            //$('#salary').prop('disabled', false);
        }
        else{
            $('#expected-pat').prop('disabled', false);
            $('#salary-change-pat').prop('disabled', false);
            $('#follow-up-pat').prop('checked', true);
            //$('#salary').prop('checked', true);
            $('#follow-up-reasons-pat').show();
            $('#follow-up-form-pat').show();
            $('#s-topic-pat').show();
            $('#expected-pat').focus();
            $('#s-pat').prop('disabled', true);
            $('#s-pat').prop('checked', true);
            //$('#salary').prop('disabled', true);
            $('#salary-after-pat').show();
        }
    });

    // CHANGED AMOUNT AFTER FUP-EOP
    $('#changed-after-follow-up').on('change', function () {
       if($(this).prop('checked')){
        $('#amount-after-follow-up').prop('disabled', false);
        $('#amount-after-follow-up').focus();
       }
       else if($(this).prop('checked', false)){
           $('#amount-after-follow-up').prop('disabled', true);
           $('#amount-after-follow-up').val('');
       }
    });

    // CHANGED AMOUNT AFTER FUP-PAT
    $('#changed-after-follow-up-pat').on('change', function () {
        if ($(this).prop('checked')) {
            $('#amount-after-follow-up-pat').prop('disabled', false);
            $('#amount-after-follow-up-pat').focus();
        }
        else if ($(this).prop('checked', false)) {
            $('#amount-after-follow-up-pat').prop('disabled', true);
            $('#amount-after-follow-up-pat').val('');
        }
    });

    // SATISFIED AFTER FUP-EOP
    $('#satisfied-after-follow-up').on('change', function () {
        if($(this).prop('checked')){
            $('#satisfied-amount-after-follow-up').prop('disabled', true);
            $('#satisfied-amount-after-follow-up').val('');
        }
        else if($(this).prop('checked', false)){
            $('#satisfied-amount-after-follow-up').prop('disabled',false);
            $('#satisfied-amount-after-follow-up').focus();
        }
    });

    // SATISFIED AFTER FUP-PAT
    $('#satisfied-after-follow-up-pat').on('change', function () {
        if($(this).prop('checked')){
            $('#satisfied-amount-after-follow-up-pat').prop('disabled', true);
            $('#satisfied-amount-after-follow-up-pat').val('');
        }
        else if($(this).prop('checked', false)){
            $('#satisfied-amount-after-follow-up-pat').prop('disabled',false);
            $('#satisfied-amount-after-follow-up-pat').focus();
        }
    });

    // TOPIC-EOP
    $('.topic').on('change',function () {
        var tab_id = $(this).attr('data-tab');
        if($(this).prop('checked')){
        $("#" + tab_id).show();
        }
        else if($(this).prop('checked', false)){
            $("#" + tab_id).hide();
        }
    });

    // TOPIC-PAT
    $('.topic-pat').on('change',function () {
        var tab_id = $(this).attr('data-tab');
        if($(this).prop('checked')){
        $("#" + tab_id).show();
        }
        else if($(this).prop('checked', false)){
            $("#" + tab_id).hide();
        }
    });

    $('#s-ad-hoc').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-salary-after').show();
        }
        else {
            $('#ad-hoc-salary-after').hide();
        }
    });

    $('#s-ad-hoc-update').on('change', function () {
        if ($(this).prop('checked')) {
            $('#ad-hoc-salary-after-update').show();
        }
        else {
            $('#ad-hoc-salary-after-update').hide();
        }
    });
});