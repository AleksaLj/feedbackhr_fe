import { Component } from './core/Component';
import config from './config';
import getPerformanceFactorMark from './utility/GetPerformanceFactor';
import { UpdateEOP } from './UpdateEOP';


export class FinalEOP extends Component {
    constructor(employeeDocIdEop) {
        super();
        this.employeeDocIdEop = employeeDocIdEop;

        this.showFinalEOP(this.employeeDocIdEop);
    }

    getFinalEOPMatrix(EopId) {
        const URL = config.url + config.getFinalEopMatrix + `${EopId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const FinalEOPMatrix = JSON.parse(data);
                return Promise.resolve(FinalEOPMatrix);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }

    getFinalEOPComments(EopId) {
        const URL = config.url + config.getFinalEopComments + `${EopId}`;
        
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const FinalEOPComments = JSON.parse(data);
                return Promise.resolve(FinalEOPComments);
                
            })
            .catch((error) => {
                console.log(error);
            })
    }
    getEmployeeById(id) {

        const URL = config.url + config.getEmployee + `${id}`;
        return fetch(URL, {
            headers: {
                'Access-Control-Allow-Origin': '*'
            }
        })
            .then((response) => response.text())
            .then((data) => {
                const employee = JSON.parse(data);
                return Promise.resolve(employee);
            })
            .catch((error) => {
                console.log(error);
            })
    }
    
    showFinalEOP(employeeDocIdEop) {
        if (employeeDocIdEop != null) { 
            var updateEOPfinal = new UpdateEOP();
            updateEOPfinal.getEopById(employeeDocIdEop)
                .then((EOP) => {
                    this.getEmployeeById(EOP.employeeId)
                        .then((employee) => {
                            const newEopFinalProfileImgDiv = document.getElementById('fb-profile-eop-final');
                            const newEopFinalImg = document.createElement('img');
                            newEopFinalImg.setAttribute('src', config.imageUrl + `${employee.imageUrl}`);
                            newEopFinalProfileImgDiv.appendChild(newEopFinalImg);
                            const eopFinalModalProfileNameDiv = document.getElementById('Modal-Fb-Profile-Name-Eop-final');
                            eopFinalModalProfileNameDiv.textContent = `${employee.firstName} ${employee.lastName}`;
                        })
                })
                    
                   
            this.getFinalEOPMatrix(employeeDocIdEop)
                .then((data) => {
                    document.getElementById('qu00').textContent = getPerformanceFactorMark(data[0][0].Mark);
                    document.getElementById('qu01').textContent = getPerformanceFactorMark(data[0][1].Mark);
                    document.getElementById('qu02').textContent = getPerformanceFactorMark(data[0][2].Mark);

                    document.getElementById('pr10').textContent = getPerformanceFactorMark(data[1][0].Mark);
                    document.getElementById('pr11').textContent = getPerformanceFactorMark(data[1][1].Mark);
                    document.getElementById('pr12').textContent = getPerformanceFactorMark(data[1][2].Mark);

                    document.getElementById('pk20').textContent = getPerformanceFactorMark(data[2][0].Mark);
                    document.getElementById('pk21').textContent = getPerformanceFactorMark(data[2][1].Mark);
                    document.getElementById('pk22').textContent = getPerformanceFactorMark(data[2][2].Mark);

                    document.getElementById('re30').textContent = getPerformanceFactorMark(data[3][0].Mark);
                    document.getElementById('re31').textContent = getPerformanceFactorMark(data[3][1].Mark);
                    document.getElementById('re32').textContent = getPerformanceFactorMark(data[3][2].Mark);

                    document.getElementById('co40').textContent = getPerformanceFactorMark(data[4][0].Mark);
                    document.getElementById('co41').textContent = getPerformanceFactorMark(data[4][1].Mark);
                    document.getElementById('co42').textContent = getPerformanceFactorMark(data[4][2].Mark);

                    document.getElementById('tw50').textContent = getPerformanceFactorMark(data[5][0].Mark);
                    document.getElementById('tw51').textContent = getPerformanceFactorMark(data[5][1].Mark);
                    document.getElementById('tw52').textContent = getPerformanceFactorMark(data[5][2].Mark);

                    document.getElementById('au60').textContent = getPerformanceFactorMark(data[6][0].Mark);
                    document.getElementById('au61').textContent = getPerformanceFactorMark(data[6][1].Mark);
                    document.getElementById('au62').textContent = getPerformanceFactorMark(data[6][2].Mark);
                });
            
            this.getFinalEOPComments(employeeDocIdEop)
                .then((data) => {
                    document.getElementById('FinalQualityClEOP').value = data[0][0];
                    document.getElementById('FinalQualityLmEOP').value = data[0][1];
                    document.getElementById('FinalQualityTlEOP').value = data[0][2];
                    document.getElementById('FinalApprasierClEOP').value = data[1][0];
                    document.getElementById('FinalApprasierLmEOP').value = data[1][1];
                    document.getElementById('FinalApprasierTlEOP').value = data[1][2];
                })
        }
    };
    
}
        
document.addEventListener('DOMContentLoaded', () => {
    const urlParamsEOP = new URLSearchParams(window.location.search);
    const employeeDocId = urlParamsEOP.get('docid');
    window.app = new FinalEOP(employeeDocId);
});