import { Component } from '../core/Component';

export class UnitModel extends Component{ 
    constructor() { 
        super();
        this._id = null;
        this._name = null;
    }

    set id(value) { 
        this._id = value;
    }

    get id() { 
        return this._id;
    }

    set name(value) { 
        this._name = value;
    }

    get name() { 
        return this._name;
    }
}