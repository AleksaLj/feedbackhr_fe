import { Component } from '../core/Component';

export class PATModel extends Component {

    constructor() {
        super();
    }

    get id() { 
        return this._id;
    }
    set id(value) { 
        this._id = value;
    }

    get dateOfTalk() {
        return this._dateOfTalk;  
    }
    set dateOfTalk(value) {
        this._dateOfTalk = value;
    }

    get formType() {
        return this._formType;  
    }
    set formType(value) {
        this._formType = value;
    }

    get employeeId() {
        return this._employeeId;  
    }
    set employeeId(value) {
        this._employeeId = value;
    }

    get HRReporter() {
        return this._HRReporter;  
    }
    set HRReporter(value) {
        this._HRReporter = value;
    }

    get employeesImpression() {
        return this._employeesImpression;
    }

    set employeesImpression(value) {
        this._employeesImpression = value;
    }
    get performanceFeedback() {
        return this._performanceFeedback;
    }

    set performanceFeedback(value) {
        this._performanceFeedback = value;
    }

    get nextSteps() {
        return this._nextSteps;
    }

    set nextSteps(value) {
        this._nextSteps = value;
    }

    get finalRecommendation() {
        return this._finalRecommendation;
    }
    set finalRecommendation(value) {
        this._finalRecommendation = value;
    }
}