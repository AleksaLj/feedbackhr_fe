import { Component } from '../core/Component';

export class AdHocFormModel extends Component { 
    constructor() { 
        super();
    }

    get id() { 
        return this._id;
    }
    set id(value) { 
        this._id = value;
    }

    get formType() { 
        return this._formType;
    }
    set formType(value) { 
        this._formType = value;
    }

    get createdDate() { 
        return this._createdDate;
    }
    set createdDate(value) { 
        this._createdDate = value;
    }

    get dateOfTalk() { 
        return this._dateOfTalk;
    }
    set dateOfTalk(value) { 
        this._dateOfTalk = value;
    }

    get reporter() { 
        return this._reporter;
    }
    set reporter(value) { 
        this._reporter = value;
    }

    get hrIncluded() { 
        return this._hrIncluded;
    }
    set hrIncluded(value) { 
        this._hrIncluded = value;
    }

    get hrNotes() { 
        return this._hrNotes;
    }
    set hrNotes(value) { 
        this._hrNotes = value;
    }

    //followUpSection - object
    get followUpSection() { 
        return this._followUpSection;
    }
    set followUpSection(value) { 
        this._followUpSection = value;
    }
    
    get isOpenedForFollowUp() { 
        return this._isOpenedForFollowUp;
    }
    set isOpenedForFollowUp(value) { 
        this._isOpenedForFollowUp = value;
    }

    get employeeId() { 
        return this._employeeId;
    }
    set employeeId(value) { 
        this._employeeId = value;
    }
    
    get createdById() { 
        return this._createdById;
    }
    set createdById(value) { 
        this._createdById = value;
    }

    get salary() { 
        return this._salary;
    }
    set salary(value) { 
        this._salary = value;
    }
}