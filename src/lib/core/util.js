const webkitRegExp = /(webkit)[ /]([\w.]+)/;
const ieRegExp = /(msie) (\d{1,2}\.\d)/;
const ie11RegExp = /(trident).*rv:(\d{1,2}\.\d)/;
const msEdge = /(edge)\/((\d+)?[\w.]+)/;
const mozillaRegExp = /(mozilla)(?:.*? rv:([\w.]+))/;
const types = {
    '[object Array]': 'array',
    '[object Date]': 'date',
    '[object Object]': 'object',
    '[object String]': 'string',
    '[object Null]': 'null',
};

export const util =
{

    browserFromUA(ua) {
        ua = ua || navigator.userAgent;
        ua = ua.toLowerCase();

        const result = {};
        const matches =
            ieRegExp.exec(ua) ||
            ie11RegExp.exec(ua) ||
            msEdge.exec(ua) ||
            (ua.indexOf('compatible') < 0 && mozillaRegExp.exec(ua)) ||
            webkitRegExp.exec(ua) ||
            [];
        let browserName = matches[1];
        let browserVersion = matches[2];

        if (browserName === 'webkit') {
            result['webkit'] = true;

            if (ua.indexOf('chrome') >= 0 || ua.indexOf('crios') >= 0) {
                browserName = 'chrome';
                browserVersion = /(?:chrome|crios)\/(\d+\.\d+)/.exec(ua);
                browserVersion = browserVersion && browserVersion[1];
            } else if (ua.indexOf('fxios') >= 0) {
                browserName = 'mozilla';
                browserVersion = /fxios\/(\d+\.\d+)/.exec(ua);
                browserVersion = browserVersion && browserVersion[1];
            } else if (ua.indexOf('safari') >= 0 && /version|phantomjs/.test(ua)) {
                browserName = 'safari';
                browserVersion = /(?:version|phantomjs)\/([0-9.]+)/.exec(ua);
                browserVersion = browserVersion && browserVersion[1];
            } else {
                browserName = 'unknown';
                browserVersion = /applewebkit\/([0-9.]+)/.exec(ua);
                browserVersion = browserVersion && browserVersion[1];
            }
        }

        if (browserName === 'trident' || browserName === 'edge') {
            browserName = 'msie';
        }

        if (browserName) {
            result[browserName] = true;
            result.version = browserVersion;
        }

        return result;
    },
    getUUID() {
        const segment = function () {
            return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
        };
        return `${segment() + segment()}-${segment()}-${segment()}-${segment()}-${segment()}${segment()}${segment()}`;
    },
    isBoolean(object) {
        return typeof object === 'boolean';
    },
    isDate(object) {
        return type(object) === 'date';
    },
    isDefined(object) {
        return object !== null && object !== undefined;
    },
    isFunction(object) {
        return typeof object === 'function';
    },
    isString(object) {
        return typeof object === 'string';
    },
    isNumeric(object) {
        return (typeof object === 'number' && isFinite(object)) || !isNaN(object - parseFloat(object));
    },
    isObject(object) {
        return type(object) === 'object';
    },
    type(object) {
        const typeOfObject = Object.prototype.toString.call(object);

        return typeof object === 'object' ? types[typeOfObject] || 'object' : typeof object;
    },
    isEmptyObject(object) {
        let property;

        for (property in object) {
            return false;
        }

        return true;
    },
    isPrimitive(value) {
        return ['object', 'array', 'function'].indexOf(this.type(value)) === -1;
    },
    isPromise(object) {
        return object && util.isFunction(object.then);
    },
    isDeferred(object) {
        return object && util.isFunction(object.done) && util.isFunction(object.fail);
    },
    isDate: (object) => util.type(object) === 'date',
    isNotEmpty: (value) => value !== undefined && value !== null && value !== '',
    isPlainObject: (object) => {
        if (!object || Object.prototype.toString.call(object) !== '[object Object]') {
            return false;
        }
        const proto = Object.getPrototypeOf(object);
        const ctor = Object.hasOwnProperty.call(proto, 'constructor') && proto.constructor;

        return typeof ctor === 'function' && Object.toString.call(ctor) === Object.toString.call(Object);
    },
    /**
     *  @callback onDelayExecuteCallback
     *
     */
    /**
     *  @callback onDelayFailCallback
     *  @param {Error} err
     */
    /**
     *  @typedef DelayOptions
     *  @property {onDelayExecuteCallback} onExecute - The function that will be executed
     *  @property {onDelayFailCallback} [onFail] - Execute if error
     *  @property {Object} [scope] - Function scope
     *  @property {Number} [ms=0] - The number of milliseconds to wait before executing the code. If omitted, the value 0 is used
     */
    /**
     * @param {DelayOptions} param0
     */
    delay({ onExecute, onFail = (err) => {}, scope = null, ms = 0 }) {
        return setTimeout(() => {
            try {
                onExecute.call(scope);
            } catch (err) {
                onFail.call(scope, err);
            }
        }, ms);
    },

}
