# FeedbackHR

###### Install
> run npm **'dev:install'** scripts or execute **'npm install'** in terminal (ctrl+shift+`)


###### NPM Task scripts
> - **build:app** start compile app javascript and copy to dist and www/assets/vendor/enjoying  folder
> - **compile:sass** compile app.scss file and copy to www/assets/vendor/enjoying/css  folder
> - **build:all** start *build:app* and *compile:sass* tasks
> - **run:lint** Runs eslit and generates a result in the log in the folder **log**
> - **dev:watch** watch *.js or *.scss changes and run  build:app or compile:sass tasks
> - **dev:install** install all component see: https://docs.npmjs.com/cli/install

###### Remarks
> - Live Server is listening on localhost:5001
> - Tasks defined in **gulpfile.js**
> - The configuration for Live Server is located in the folder **.vscode**
  
---
ende :)
