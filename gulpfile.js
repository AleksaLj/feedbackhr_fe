'use strict';
const { src, dest, watch, task, series, parallel } = require('gulp');
const rollup = require('rollup');
const babel = require('rollup-plugin-babel');
const pkg = require('./package.json');
const sass = require('gulp-sass');
const merge = require('merge-stream');
const less = require('gulp-less');

const DEFAULT_BUNDLE_FORMAT = 'umd';
const MODULE_NAME = 'FeedbackHR';
const APP_NAME = pkg.name;
const LIB_NAME = `${pkg.name}.lib`;
const MAIN_VENDOR_OUTPUT = `./www/assets/vendor/${pkg.author}/`;
/* JS CONFIG*/
const JS_LIB_CONFIG = {
    inputFile: './src/lib/package.js',
    outputFile: `./dist/${DEFAULT_BUNDLE_FORMAT}/${LIB_NAME}.lib.js`,
    format: DEFAULT_BUNDLE_FORMAT,
    module: MODULE_NAME,
    banner: '',
    outro: '',
};
const JS_APP_CONFIG = {
    inputFile: './src/app/package.js',

    outputFile: `./dist/${DEFAULT_BUNDLE_FORMAT}/${APP_NAME}.js`,
    format: DEFAULT_BUNDLE_FORMAT,
    module: MODULE_NAME,
    banner: '',
    outro: '',
};
/**-----------------------------------------------------------------------------------------------
 *  Rollup bundle helper function
 *------------------------------------------------------------------------------------------------
 * @param {BundleConfig} param0
 */
async function rollupBundle({ inputFile, outputFile, format, module, banner, outro }) {
    console.info({ inputFile, outputFile, format, module });
    var bundle = await rollup.rollup({
        input: inputFile,
        plugins: [
            babel({
                babelrc: true,
            }),
        ],
    });

    return bundle.write({
        file: outputFile,
        format,
        name: module,
        sourcemap: true,
        banner,
        outro,
    });
}

/**
 *
 * @param {RollupConfig} cfg
 */
const buildJs = (cfg) => {
    return async (cb) => {
        return await rollupBundle(cfg);
    };
};

const watchChanges = () => {
    return (cb) => {
        watch('./src/app/**/*.js', task('build:app'));
        watch('./src/design/**/*.scss', task('compile:sass'));
    };
};
const buildSASS = () => {
    return async (cb) => {
        return src('./src/design/app.scss')
            .pipe(sass.sync().on('error', sass.logError))
            .pipe(dest(`${MAIN_VENDOR_OUTPUT}css`));
    };
};

const buildLESS = () => {
    return async (cb) => {
        return src('./src/design/app.less')
            .pipe(less('error', less.logError))
            .pipe(dest(`${MAIN_VENDOR_OUTPUT}css`));
    };
};

const copyFiles = (items) => {
    return async (cb) => {
        items = items || [];
        const streams = [];
        items.forEach((f) => {
            console.log(f);
            streams.push(src(f.src).pipe(dest(f.dest)));
        });
        switch (streams.length) {
            case 0:
                return cb();
            case 1:
                return streams[0];
            default:
                return merge(...streams);
        }
    };
};

task(
    "copy:dependencies",
    copyFiles([
        {
            src: `./src/app/dependencies/**/*`,
            dest: MAIN_VENDOR_OUTPUT
        },
        {
            src: `./src/design/css/**/*`,
            dest: `${MAIN_VENDOR_OUTPUT}/css/`
        },
        {
            src: `./src/design/fonts/**/*`,
            dest: `${MAIN_VENDOR_OUTPUT}/fonts/`
        },
        
        {
            src: `./src/design/images/**/*`,
            dest: `${MAIN_VENDOR_OUTPUT}/images/`
        },
        {
            src: `node_modules/jquery-validation/dist/jquery.validate.min.js`,
            dest: `${MAIN_VENDOR_OUTPUT}/jquery/`
        }
    ])
);
task('bundle:app', buildJs(JS_APP_CONFIG));
task(
    'copy:app',
    copyFiles([
        {
            src: `./dist/${DEFAULT_BUNDLE_FORMAT}/${APP_NAME}.js`,
            dest: MAIN_VENDOR_OUTPUT,
        },
        {
            src: `./dist/${DEFAULT_BUNDLE_FORMAT}/${APP_NAME}.js.map`,
            dest: MAIN_VENDOR_OUTPUT,
        },
    ])
);
task('build:lib', buildJs(JS_LIB_CONFIG));
task('compile:sass', buildSASS());
task('compile:less', buildLESS());
task('build:app', series(task('bundle:app'), task('copy:app')));
task('start:watch', watchChanges());
task('build:all', series(task('bundle:app'), task('copy:app'), task('copy:dependencies'), task('compile:less')));

/**
 * - Rollap bundle configuration
 * @typedef RollupConfig
 * @property {String} inputFile
 * @property {String} outputFile
 * @property {( 'amd' | 'cjs' | 'es' | 'iife' | 'system' | 'umd')} format
 * @property {String} module
 * @property {String} baner
 * @property {String} outro
 */
/**
 *  -- file options struct from package
 * @typedef FileOptions
 * @property {String} dest
 * @property {String} src
 */

/**
 *  -- file options struct from package
 * @typedef FileArgs
 * @property {String} dest
 * @property {String | String[]} src
 */
